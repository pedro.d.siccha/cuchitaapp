package com.inforad.cuchitaapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.necesito.categotia;
import com.inforad.cuchitaapp.ofrezco.perfilNegocio;
import com.inforad.cuchitaapp.ofrezco.seleccionarPlan;
import com.inforad.cuchitaapp.provider.UserProvider;

public class tipoUsuario extends AppCompatActivity {

    private LinearLayout btnNecesito, btnOfrezco;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private UserProvider mUserProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_tipo_usuario);

        Iniciar();

        btnNecesito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(tipoUsuario.this, categotia.class);
                startActivity(intent);
            }
        });

        btnOfrezco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FirebaseUser user = mAuth.getCurrentUser();
                if (user != null){
                    String id_registro = user.getUid();

                    mUserProvider.obtenerUsuario(id_registro).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            if (snapshot.exists()){
                                if (snapshot.child("actividad").getValue().toString().equals("TUTORIAL")){
                                    Intent intent = new Intent(tipoUsuario.this, seleccionarPlan.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                }else {
                                    Intent intent = new Intent(tipoUsuario.this, perfilNegocio.class);
                                    startActivity(intent);
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });
                }
            }
        });
    }

    private void Iniciar(){
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        btnNecesito = findViewById(R.id.btnNecesito);
        btnOfrezco = findViewById(R.id.btnOfrezco);
        mUserProvider = new UserProvider();
    }

}