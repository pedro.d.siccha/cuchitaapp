package com.inforad.cuchitaapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.inforad.cuchitaapp.models.Direccion;
import com.inforad.cuchitaapp.provider.direccionProvider;

import de.hdodenhof.circleimageview.CircleImageView;

public class confirDireccion extends AppCompatActivity {

    private String dir, ciudad, pais, tipoDireccion, barrio, idUsuario;
    private double latitud, longitud;
    private TextView txtDireccion, txtCiudad, txtPais, txtBarrio;
    private CircleImageView btnCambiar;
    private Button btnGuardar;
    private direccionProvider mDireccion;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_confir_direccion);
        Iniciar();

        btnCambiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(confirDireccion.this, mapa.class);
                startActivity(intent);
            }
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                guardarDireccion();
            }
        });
    }

    private void guardarDireccion() {
        Direccion direccion = new Direccion(tipoDireccion, dir, ciudad, pais, "Activo", "1", latitud, longitud);
        mDireccion.guardarDireccion(direccion, idUsuario, tipoDireccion).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Intent intent = new Intent(confirDireccion.this, tipoUsuario.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
    }

    private void Iniciar() {
        txtDireccion = findViewById(R.id.tvDireccion);
        txtCiudad = findViewById(R.id.tvCiudad);
        txtPais = findViewById(R.id.tvPais);
        txtBarrio = findViewById(R.id.barrio);
        btnCambiar = findViewById(R.id.civCambiarDireccion);
        btnGuardar = findViewById(R.id.bGuardar);

        mDireccion = new direccionProvider();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();

        obtenerDatos();
    }

    private void obtenerDatos() {
        dir = getIntent().getStringExtra("direccion");
        ciudad = getIntent().getStringExtra("ciudad");
        pais = getIntent().getStringExtra("pais");
        tipoDireccion = getIntent().getStringExtra("tipoDireccion");
        longitud = getIntent().getDoubleExtra("longitud", 0);
        latitud = getIntent().getDoubleExtra("latitud", 0);
        barrio = getIntent().getStringExtra("barrio");

        txtPais.setText(pais);
        txtCiudad.setText(ciudad);
        txtDireccion.setText(dir);
        txtBarrio.setText(barrio);
    }
}