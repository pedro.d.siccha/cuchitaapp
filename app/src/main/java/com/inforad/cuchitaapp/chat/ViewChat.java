package com.inforad.cuchitaapp.chat;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.adapters.chatAdaptador;
import com.inforad.cuchitaapp.models.Chat;
import com.inforad.cuchitaapp.provider.UserProvider;
import com.inforad.cuchitaapp.provider.separadorProvider;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class ViewChat extends AppCompatActivity {

    private CircleImageView imgPerfil;
    private TextView txtNombre;
    private UserProvider mUserProvider;
    private ImageButton btnEnviar;
    private separadorProvider mChatProvider;
    private String idEnvia, idRecibe;
    private AppCompatEditText txtMensaje;
    private RecyclerView recyclerChats;
    private DatabaseReference mDatabase;
    private chatAdaptador mChatAdaptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_chat);
        Iniciar();

        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Chat chat = new Chat(idEnvia, idRecibe, txtMensaje.getText().toString(), "", "ENVIADO");
                mChatProvider.enviarMensaje(chat, idEnvia, idRecibe).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        mChatProvider.enviarMensaje(chat, idRecibe, idEnvia).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                txtMensaje.setText("");
                            }
                        });
                    }
                });
            }
        });
    }

    private void Iniciar() {
        imgPerfil = findViewById(R.id.civFoto);
        txtNombre = findViewById(R.id.tvNombre);
        btnEnviar = findViewById(R.id.btnEnviarMensaje);
        txtMensaje = findViewById(R.id.etMensaje);
        recyclerChats = findViewById(R.id.rvChat);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerChats.setLayoutManager(linearLayoutManager);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mUserProvider = new UserProvider();
        mChatProvider = new separadorProvider();

        cargarDatos();
    }

    private void cargarDatos() {

        idEnvia = getIntent().getStringExtra("idEnvia");
        idRecibe = getIntent().getStringExtra("idRecibir");

        mUserProvider.obtenerUsuario(idRecibe).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String img = snapshot.child("imagen").getValue().toString();
                    txtNombre.setText(snapshot.child("alias").getValue().toString());
                    Picasso.with(ViewChat.this).load(img).into(imgPerfil);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        Query queryChats = mDatabase.child("chats").child(idEnvia + "__&__" + idRecibe).child("mensaje");
        FirebaseRecyclerOptions<Chat> options = new FirebaseRecyclerOptions.Builder<Chat>().setQuery(queryChats, Chat.class).build();
        mChatAdaptador = new chatAdaptador(options, ViewChat.this);
        recyclerChats.setAdapter(mChatAdaptador);
        mChatAdaptador.startListening();
    }
}