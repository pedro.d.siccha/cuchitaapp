package com.inforad.cuchitaapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.PedidoProducto;
import com.inforad.cuchitaapp.provider.negocioInventarioProvider;
import com.squareup.picasso.Picasso;

public class productoPedidoAdaptador extends FirebaseRecyclerAdapter<PedidoProducto, productoPedidoAdaptador.ViewHolder> {

    private Context mContext;
    private String mIdProducto;

    public productoPedidoAdaptador(@NonNull FirebaseRecyclerOptions<PedidoProducto> options, Context context, String idProducto) {
        super(options);
        mContext = context;
        mIdProducto = idProducto;

    }

    @Override
    protected void onBindViewHolder(@NonNull productoPedidoAdaptador.ViewHolder holder, int position, @NonNull PedidoProducto pedidoModel) {
        holder.mInvetarioProvider.obtenerProducto(pedidoModel.getIdTienda(), mIdProducto).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String img = snapshot.child("imgProducto").getValue().toString();
                    Picasso.with(mContext).load(img).into(holder.imgProducto);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        holder.producto.setText(pedidoModel.getNombreProducto());
        holder.cantidad.setText(pedidoModel.getCantidad());
        holder.precio.setText("S/." + pedidoModel.getPrecio());

    }

    @NonNull
    @Override
    public productoPedidoAdaptador.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_producto_pedido, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView producto, cantidad, precio;
        private ImageView imgProducto;
        private View mView;
        private negocioInventarioProvider mInvetarioProvider;
        public ViewHolder(View view){
            super(view);
            mView = view;
            producto = view.findViewById(R.id.tvProducto);
            cantidad = view.findViewById(R.id.tvCantidad);
            precio = view.findViewById(R.id.tvPrecio);
            imgProducto = view.findViewById(R.id.civProducto);
            mInvetarioProvider = new negocioInventarioProvider();
        }
    }

}
