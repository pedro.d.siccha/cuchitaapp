package com.inforad.cuchitaapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.imgDescNegocio;
import com.squareup.picasso.Picasso;

public class imagenDescripcionAdaptador extends FirebaseRecyclerAdapter<imgDescNegocio, imagenDescripcionAdaptador.ViewHolder> {
    private Context mContext;

    public imagenDescripcionAdaptador(@NonNull FirebaseRecyclerOptions<imgDescNegocio> options, Context context) {
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull imgDescNegocio model) {
        String imagen = model.getUrl();
        Picasso.with(mContext).load(imagen).into(holder.img);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_imagen_descripcion, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView img;
        private View mView;
        public ViewHolder(View view){
            super(view);
            mView = view;
            img = view.findViewById(R.id.imgDescripcion);
        }
    }
}
