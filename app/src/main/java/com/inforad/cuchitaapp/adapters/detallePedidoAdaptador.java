package com.inforad.cuchitaapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.PedidoProducto;

public class detallePedidoAdaptador extends FirebaseRecyclerAdapter<PedidoProducto, detallePedidoAdaptador.ViewHolder> {

    private Context mContex;

    public detallePedidoAdaptador(@NonNull FirebaseRecyclerOptions<PedidoProducto> options, Context context) {
        super(options);
        mContex = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull PedidoProducto productoModel) {
        String cadena = String.valueOf(productoModel.getPrecio());

        holder.txtNombre.setText(productoModel.getNombreProducto());
        holder.txtCantidad.setText(productoModel.getCantidad());
        holder.txtPrecio.setText(cadena);

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_listapedidoaceptado, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txtNombre, txtCantidad, txtPrecio;
        private ImageView imgProducto;
        private View mView;

        public ViewHolder(View view){
            super(view);
            mView = view;
            txtNombre = view.findViewById(R.id.tvProducto);
            txtCantidad = view.findViewById(R.id.tvCantidad);
            txtPrecio = view.findViewById(R.id.tvPrecio);

        }
    }
}
