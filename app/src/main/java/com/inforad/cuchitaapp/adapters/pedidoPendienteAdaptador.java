package com.inforad.cuchitaapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.FCMBody;
import com.inforad.cuchitaapp.models.FCMResponse;
import com.inforad.cuchitaapp.models.Pedido;
import com.inforad.cuchitaapp.necesito.categotia;
import com.inforad.cuchitaapp.necesito.pedido;
import com.inforad.cuchitaapp.provider.ClientePedidoProvider;
import com.inforad.cuchitaapp.provider.NotificationProvider;
import com.inforad.cuchitaapp.provider.PedidoProvider;
import com.inforad.cuchitaapp.provider.TokenProvider;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class pedidoPendienteAdaptador extends FirebaseRecyclerAdapter<Pedido, pedidoPendienteAdaptador.ViewHolder> {

    private Context mContext;
    private TokenProvider mTokenProvider;
    private NotificationProvider mNotificationProvider;
    private ClientePedidoProvider mClientePedidoProvider;
    private FirebaseAuth mAuth;
    private String usuario_id;
    private PedidoProvider mPedido;

    public pedidoPendienteAdaptador(FirebaseRecyclerOptions<Pedido> options, Context context){
        super(options);
        mContext = context;
        mTokenProvider = new TokenProvider();
        mNotificationProvider = new NotificationProvider();
        mClientePedidoProvider = new ClientePedidoProvider();
        mAuth = FirebaseAuth.getInstance();
        usuario_id = mAuth.getCurrentUser().getUid();
        mPedido = new PedidoProvider();
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull Pedido pendienteModel) {
        holder.txtNomProducto.setText(pendienteModel.getTienda());
        holder.txtCantidad.setText("Fecha de Entrega: " + pendienteModel.getFecEntrega());
        holder.txtPrecio.setText("S/." + pendienteModel.getPrecioTotal());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendNotification(pendienteModel.getIdTienda(), pendienteModel.getIdTienda(), pendienteModel.getEstado(), pendienteModel.getPrecioTotal());

            }
        });

    }

    private void sendNotification(String tienda_id, String producto_id, String producto, double cantidad) {
        mTokenProvider.getToken(usuario_id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String token = snapshot.child("token").getValue().toString();
                    Map<String, String> map = new HashMap<>();
                    map.put("titulo", "Pedido Pendientes");
                    map.put("contenido", "Tiene un pedido de " + cantidad + " " + producto);
                    map.put("idUsuario", usuario_id);
                    map.put("idProducto", producto_id);
                    FCMBody fcmBody = new FCMBody(token, "high", map);
                    mNotificationProvider.sendNotification(fcmBody).enqueue(new Callback<FCMResponse>() {
                        @Override
                        public void onResponse(Call<FCMResponse> call, Response<FCMResponse> response) {
                            if (response.body() != null){
                                if (response.body().getSuccess() == 1){
                                    verificarEstadoClientePedido(producto_id);
                                }else {
                                    Toast.makeText(mContext, "Error no se pudo enviar la notificación", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<FCMResponse> call, Throwable t) {
                            Log.d("Error", "Error 0001: " + t.getMessage());
                        }
                    });
                }else {
                    Toast.makeText(mContext, "Error Existe TOken Asociado", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void verificarEstadoClientePedido(String idTienda) {

        mPedido.obtenerEstado(mAuth.getCurrentUser().getUid(), idTienda).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String estado = snapshot.getValue().toString();
                    if (estado.equals("CREADO")){
                        Intent intent = new Intent(mContext, pedido.class);
                        intent.putExtra("idTienda", idTienda);
                        mContext.startActivity(intent);
                    }else if (estado.equals("CANCELADO")){
                        Toast.makeText(mContext, "Producto no disponible en Stock", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(mContext, categotia.class);
                        mContext.startActivity(intent);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_listapedidopendiente, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView imgProducto;
        private TextView txtNomProducto, txtCantidad, txtPrecio;
        private View mView;
        public ValueEventListener mListener;


        public ViewHolder(View view){
            super(view);

            mView = view;
            txtNomProducto = view.findViewById(R.id.tvProducto);
            txtCantidad = view.findViewById(R.id.tvCantidad);
            txtPrecio = view.findViewById(R.id.tvPrecio);

        }
    }


}
