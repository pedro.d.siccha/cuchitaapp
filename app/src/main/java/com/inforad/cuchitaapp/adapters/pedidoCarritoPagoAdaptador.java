package com.inforad.cuchitaapp.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.PedidoFinal;
import com.inforad.cuchitaapp.necesito.pagar;
import com.inforad.cuchitaapp.provider.TokenProvider;
import com.inforad.cuchitaapp.provider.negocioProvider;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class pedidoCarritoPagoAdaptador extends FirebaseRecyclerAdapter<PedidoFinal, pedidoCarritoPagoAdaptador.ViewHolder> {

    private Context mContext;
    private TokenProvider mTokenProvider;
    private FirebaseAuth mAuth;
    private String usuario_id, nombreTienda;

    public pedidoCarritoPagoAdaptador(@NonNull FirebaseRecyclerOptions<PedidoFinal> options, Context context) {
        super(options);
        mContext = context;
        mTokenProvider = new TokenProvider();
        mAuth = FirebaseAuth.getInstance();
        usuario_id = mAuth.getCurrentUser().getUid();
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull PedidoFinal pedidoModel) {

        holder.mNegocioProvider.getNegocio(pedidoModel.getIdTienda()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String img = snapshot.child("imagen").getValue().toString();
                    Picasso.with(mContext).load(img).into(holder.imgNegocio);
                    holder.txtNombre.setText(snapshot.child("nombre").getValue().toString());
                    holder.txtCantidad.setText(snapshot.child("descripcion").getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        holder.txtPrecio.setText("S/. " + pedidoModel.getPrecio());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (pedidoModel.getPrecio() > 0){
                    Intent intent = new Intent(mContext, pagar.class);
                    intent.putExtra("idTienda", pedidoModel.getIdTienda());
                    intent.putExtra("idPedido", pedidoModel.getIdCliente());
                    mContext.startActivity(intent);
                }else {
                    Toast.makeText(mContext, "Agregue producos en su pedido", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_listapedidopago, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txtNombre, txtCantidad, txtPrecio;
        private CircleImageView imgNegocio;
        private View mView;
        private Dialog mDialog;
        private negocioProvider mNegocioProvider;

        public ViewHolder(View view){
            super(view);
            mView = view;

            txtNombre = view.findViewById(R.id.tvTienda);
            txtCantidad = view.findViewById(R.id.tvFecPedido);
            txtPrecio = view.findViewById(R.id.tvFecEntregas);
            imgNegocio = view.findViewById(R.id.imgTienda);
            mDialog = new Dialog(mContext);
            mNegocioProvider = new negocioProvider();
        }
    }
}
