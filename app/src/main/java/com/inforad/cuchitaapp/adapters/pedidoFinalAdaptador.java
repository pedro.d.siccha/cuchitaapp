package com.inforad.cuchitaapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.PedidoFinal;

public class pedidoFinalAdaptador extends FirebaseRecyclerAdapter<PedidoFinal, pedidoFinalAdaptador.ViewHolder>{
    private Context mContext;

    public pedidoFinalAdaptador(FirebaseRecyclerOptions<PedidoFinal> options, Context context){
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull pedidoFinalAdaptador.ViewHolder holder, int position, @NonNull PedidoFinal finalModel) {
        /*
        holder.txtNombre.setText(finalModel.getProducto());
        holder.txtCantidad.setText(finalModel.getCantidad());
        holder.txtPrecio.setText(finalModel.getPrecio_total());
        holder.btnRepetirPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext, "Repetir Pedido", Toast.LENGTH_SHORT).show();
            }
        });

         */
    }

    @NonNull
    @Override
    public pedidoFinalAdaptador.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_listapedidofinal, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txtNombre, txtCantidad, txtPrecio;
        private Button btnRepetirPedido;
        private View mView;
        public ViewHolder(View view){
            super(view);
            mView = view;
            txtNombre = view.findViewById(R.id.tvNomProductoFinal);
            txtCantidad = view.findViewById(R.id.tvCantidadFinal);
            txtPrecio = view.findViewById(R.id.tvPrecioFinal);
            btnRepetirPedido = view.findViewById(R.id.btnRepetirFinal);

        }
    }

}
