package com.inforad.cuchitaapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.Tarjeta;

public class tarjetaAdaptador extends FirebaseRecyclerAdapter<Tarjeta, tarjetaAdaptador.ViewHolder> {
    private Context mContext;

    public tarjetaAdaptador(@NonNull FirebaseRecyclerOptions<Tarjeta> options, Context context) {
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull Tarjeta tarjetaModel) {
        holder.txtNumTarjeta.setText(tarjetaModel.getId());
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tarjeta, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txtNumTarjeta;
        private View mView;
        public ViewHolder(View view){
            super(view);
            mView = view;
            txtNumTarjeta = view.findViewById(R.id.tvCodTarjeta);
        }
    }
}
