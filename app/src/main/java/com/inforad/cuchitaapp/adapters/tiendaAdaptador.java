package com.inforad.cuchitaapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.Negocio;
import com.inforad.cuchitaapp.models.PedidoFinal;
import com.inforad.cuchitaapp.necesito.detalleTienda;
import com.inforad.cuchitaapp.provider.PedidoProvider;
import com.inforad.cuchitaapp.provider.UserProvider;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class tiendaAdaptador extends FirebaseRecyclerAdapter<Negocio, tiendaAdaptador.ViewHolder> {

    private Context mContext;
    private String idUsuario;


    public tiendaAdaptador(FirebaseRecyclerOptions<Negocio> options, Context context, String mIdUsuario){
        super(options);
        mContext = context;
        idUsuario = mIdUsuario;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull Negocio negocioModel) {
        String tienda_id = getRef(position).getKey();
        String nomTienda = negocioModel.getNombre();
        holder.txtTienda.setText(negocioModel.getNombre());
        holder.txtDescripcion.setText(negocioModel.getDescripcion());
        holder.txtCategoria.setText(negocioModel.getCategoria());
        String imagen = negocioModel.getImagen();
        Picasso.with(mContext).load(imagen).into(holder.imgTienda);
        holder.mview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.mUserProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (snapshot.exists()){
                            holder.generarPedido(snapshot.child("alias").getValue().toString(), "", idUsuario, negocioModel.getId());
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });

                Intent intent = new Intent(mContext, detalleTienda.class);
                intent.putExtra("tienda_id", tienda_id);
                intent.putExtra("tienda", nomTienda);
                intent.putExtra("imagen", imagen);
                mContext.startActivity(intent);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_listatienda, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txtTienda, txtCategoria, txtDescripcion;
        private CircleImageView imgTienda;
        private View mview;
        private PedidoProvider mPedidoProvider = new PedidoProvider();
        private UserProvider mUserProvider = new UserProvider();
        private void generarPedido(String nombre, String idUsuario, String dni, String idNegocio){
            PedidoFinal pedido = new PedidoFinal(nombre, idUsuario, dni, idNegocio, idUsuario, 0.00, "15", "PENDIENTE");
            mPedidoProvider.create(pedido, idNegocio, dni).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {

                }
            });
        }

        public ViewHolder(View view){
            super(view);
            mview = view;
            txtTienda = view.findViewById(R.id.tvTienda);
            txtDescripcion = view.findViewById(R.id.tvDesripcionTienda);
            txtCategoria = view.findViewById(R.id.tvDocTienda);
            imgTienda = view.findViewById(R.id.imgListaTienda);
        }
    }

}
