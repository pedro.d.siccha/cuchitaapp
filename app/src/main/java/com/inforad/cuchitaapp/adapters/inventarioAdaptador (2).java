package com.inforad.cuchitaapp.adapters;


import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.NegocioInventario;
import com.inforad.cuchitaapp.ofrezco.editarProducto;
import com.squareup.picasso.Picasso;

public class inventarioAdaptador extends FirebaseRecyclerAdapter<NegocioInventario, inventarioAdaptador.ViewHolder> {
    private Context mContext;
    public String rucNegocio;

    public inventarioAdaptador(@NonNull FirebaseRecyclerOptions<NegocioInventario> options, Context context, String idNegocio) {
        super(options);
        mContext = context;
        rucNegocio = idNegocio;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull NegocioInventario productoModel) {
        holder.producto.setText(productoModel.getNombre());
        holder.stock.setText("Stock Actual: " + productoModel.getStock());
        holder.detalle.setText(productoModel.getDetalle());
        holder.precio.setText("Precio por " + productoModel.getUnidadMedida() + ": S/." + productoModel.getPrecioUnidad());
        String imagen = productoModel.getImgProducto();
        Picasso.with(mContext).load(imagen).into(holder.img);
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext, productoModel.getId(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(mContext, editarProducto.class);
                intent.putExtra("producto_id", productoModel.getId());
                intent.putExtra("nombre", productoModel.getNombre());
                intent.putExtra("stock", productoModel.getStock());
                intent.putExtra("stockMaximo", productoModel.getStockMaximo());
                intent.putExtra("stockMinimo", productoModel.getStockMinimo());
                intent.putExtra("unidadMedida", productoModel.getUnidadMedida());
                intent.putExtra("precio", productoModel.getPrecioUnidad());
                intent.putExtra("imagen", productoModel.getImgProducto());
                intent.putExtra("estado", productoModel.getEstado());
                intent.putExtra("idNegocio", rucNegocio);
                mContext.startActivity(intent);
            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_producto, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView producto, stock, detalle, precio;
        private ImageView img;
        private View mView;
        public ViewHolder(View view){
            super(view);
            mView = view;
            producto = view.findViewById(R.id.tvProducto);
            stock = view.findViewById(R.id.tvStock);
            img = view.findViewById(R.id.imgProducto);
            detalle = view.findViewById(R.id.tvDescripcion);
            precio = view.findViewById(R.id.tvPrecio);

        }
    }
}