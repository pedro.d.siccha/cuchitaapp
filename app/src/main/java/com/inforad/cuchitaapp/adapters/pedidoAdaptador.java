package com.inforad.cuchitaapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.PedidoFinal;
import com.inforad.cuchitaapp.necesito.pedido;
import com.inforad.cuchitaapp.provider.TokenProvider;
import com.inforad.cuchitaapp.provider.negocioProvider;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class pedidoAdaptador extends FirebaseRecyclerAdapter<PedidoFinal, pedidoAdaptador.ViewHolder> {

    private Context mContext;
    private TokenProvider mTokenProvider;
    private FirebaseAuth mAuth;
    private String usuario_id, nombreTienda;
    private negocioProvider mNegocioProvider;

    public pedidoAdaptador(@NonNull FirebaseRecyclerOptions<PedidoFinal> options, Context context) {
        super(options);
        mContext = context;
        mTokenProvider = new TokenProvider();
        mAuth = FirebaseAuth.getInstance();
        usuario_id = mAuth.getCurrentUser().getUid();
        mNegocioProvider = new negocioProvider();
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull PedidoFinal pedidoModel) {

        String cadena = String.valueOf(pedidoModel.getPrecio());
        holder.txtPrecio.setText("S/. " + cadena);
        mNegocioProvider.getNegocio(pedidoModel.getIdTienda()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String img = snapshot.child("imagen").getValue().toString();
                    holder.txtNombre.setText(snapshot.child("nombre").getValue().toString());
                    holder.txtCantidad.setText(snapshot.child("descripcion").getValue().toString());
                    Picasso.with(mContext).load(img).into(holder.imgNegocio);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, pedido.class);
                intent.putExtra("tienda_id", pedidoModel.getIdTienda());
                mContext.startActivity(intent);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_listapedidopendiente, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txtNombre, txtCantidad, txtPrecio;
        private CircleImageView imgNegocio;
        private View mView;

        public ViewHolder(View view){
            super(view);
            mView = view;
            txtNombre = view.findViewById(R.id.tvProducto);
            txtCantidad = view.findViewById(R.id.tvCantidad);
            txtPrecio = view.findViewById(R.id.tvPrecio);
            imgNegocio = view.findViewById(R.id.ivProducto);
        }
    }
}
