package com.inforad.cuchitaapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.Cliente;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class clienteAdaptador extends FirebaseRecyclerAdapter<Cliente, clienteAdaptador.ViewHolder> {
    private Context mContext;

    public clienteAdaptador(@NonNull FirebaseRecyclerOptions<Cliente> options, Context context) {
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull clienteAdaptador.ViewHolder holder, int position, @NonNull Cliente clienteModel) {
        if (clienteModel.getDni().toString().equals("")){
            holder.txtNombre.setVisibility(View.GONE);
            holder.imgPerfil.setVisibility(View.GONE);
            holder.txtError.setVisibility(View.VISIBLE);
        }else {
            holder.txtNombre.setVisibility(View.VISIBLE);
            holder.imgPerfil.setVisibility(View.VISIBLE);
            holder.txtError.setVisibility(View.GONE);

            holder.txtNombre.setText(clienteModel.getAlias());
            String imagen = clienteModel.getImagen();
            Picasso.with(mContext).load(imagen).into(holder.imgPerfil);
        }
    }


    @NonNull
    @Override
    public clienteAdaptador.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_likecliente, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private CircleImageView imgPerfil;
        private TextView txtNombre, txtError;
        private View mView;
        public ViewHolder(View view){
            super(view);
            mView = view;
            imgPerfil = view.findViewById(R.id.civCliente);
            txtNombre = view.findViewById(R.id.tvNombre);
            txtNombre = view.findViewById(R.id.tvError);
        }
    }
}
