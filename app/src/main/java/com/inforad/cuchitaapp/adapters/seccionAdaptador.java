package com.inforad.cuchitaapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.Seccion;
import com.inforad.cuchitaapp.models.SeccionProducto;

public class seccionAdaptador extends FirebaseRecyclerAdapter<Seccion, seccionAdaptador.ViewHolder> {
    private Context mContext;
    public String rucNegocio;

    public seccionAdaptador(@NonNull FirebaseRecyclerOptions<Seccion> options, Context context, String idNegocio) {
        super(options);
        mContext = context;
        rucNegocio = idNegocio;
    }


    @Override
    protected void onBindViewHolder(@NonNull seccionAdaptador.ViewHolder holder, int position, @NonNull Seccion seccionModel) {
        holder.txtSeccion.setText(seccionModel.getSeccion());

        Query querySeccion = holder.mDatabase.child("Seccion_Producto").child(seccionModel.getIdNegocio()).child(seccionModel.getSeccion());
        FirebaseRecyclerOptions<SeccionProducto> optionsSeccion = new FirebaseRecyclerOptions.Builder<SeccionProducto>().setQuery(querySeccion, SeccionProducto.class).build();
        holder.adaptadorSeccion = new seccionAdaptadorProducto(optionsSeccion, mContext);
        holder.rvProductos.setAdapter(holder.adaptadorSeccion);
        holder.adaptadorSeccion.startListening();
    }

    @NonNull
    @Override
    public seccionAdaptador.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_seccion, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txtSeccion;
        private View mView;
        private RecyclerView rvProductos;
        private DatabaseReference mDatabase;
        private seccionAdaptadorProducto adaptadorSeccion;
        public ViewHolder(View view){
            super(view);
            mView = view;
            txtSeccion = view.findViewById(R.id.tvSeccion);
            rvProductos = view.findViewById(R.id.recyclerSeccionProducto);
            mDatabase = FirebaseDatabase.getInstance().getReference();
            /*
            LinearLayoutManager linearLayoutManagerSeccion = new LinearLayoutManager(mView.getContext());
            rvProductos.setLayoutManager(linearLayoutManagerSeccion);
             */
        }
    }
}
