package com.inforad.cuchitaapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.Producto;
import com.inforad.cuchitaapp.necesito.agregarProducto;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class productoTiendaAdaptador extends FirebaseRecyclerAdapter<Producto, productoTiendaAdaptador.ViewHolder> {

    private Context mContext;

    public productoTiendaAdaptador(FirebaseRecyclerOptions<Producto> options, Context context, String tienda_id){
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull productoTiendaAdaptador.ViewHolder holder, int position, @NonNull Producto productoModel) {
        holder.txtPrecioUnitario.setText("S/. " + productoModel.getPrecioUnidad());
        holder.txtUnidadMedida.setText(productoModel.getUnidadMedida());
        holder.txtNombre.setText(productoModel.getNombre());

        String imagen = productoModel.getImgProducto();
        Picasso.with(mContext).load(imagen).into(holder.img);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, agregarProducto.class);
                intent.putExtra("idProducto", productoModel.getId());
                mContext.startActivity(intent);
            }
        });
    }

    @NonNull
    @Override
    public productoTiendaAdaptador.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_listaproductotienda, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txtNombre, txtUnidadMedida, txtPrecioUnitario;
        private View mView;
        private CircleImageView img;
        public ViewHolder(View view){
            super(view);
            mView = view;
            txtNombre = view.findViewById(R.id.tvNomProducto);
            txtUnidadMedida = view.findViewById(R.id.tvUnidadMedida);
            txtPrecioUnitario = view.findViewById(R.id.tvPrecioUnidad);
            img = view.findViewById(R.id.imgProducto);

        }
    }

}
