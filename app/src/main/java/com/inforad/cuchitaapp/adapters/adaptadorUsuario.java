package com.inforad.cuchitaapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.icu.text.DateFormat;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.Cliente;
import com.inforad.cuchitaapp.models.PedidoFinal;
import com.inforad.cuchitaapp.models.User;
import com.inforad.cuchitaapp.ofrezco.ventasTienda;
import com.inforad.cuchitaapp.provider.PedidoProvider;
import com.inforad.cuchitaapp.provider.clienteProvider;
import com.inforad.cuchitaapp.provider.negocioProvider;
import com.squareup.picasso.Picasso;

import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

public class adaptadorUsuario extends FirebaseRecyclerAdapter<User, adaptadorUsuario.ViewHolder> {
    private Context mContext;
    private String mIdNegocio;

    public adaptadorUsuario(@NonNull FirebaseRecyclerOptions<User> options, Context context, String idNegocio) {
        super(options);
        mContext = context;
        mIdNegocio = idNegocio;
    }

    @Override
    protected void onBindViewHolder(@NonNull adaptadorUsuario.ViewHolder holder, int position, @NonNull User userModel) {
        holder.txtNombre.setText(userModel.getAlias());
        holder.txtDni.setText(userModel.getDni());
        holder.txtTelefono.setText(userModel.getTelefono());
        String img = userModel.getImagen();
        Picasso.with(mContext).load(img).into(holder.imgPerfil);
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cliente cliente = new Cliente(userModel.getId(), userModel.getImagen(), userModel.getAlias(), userModel.getDni(), userModel.getTelefono());
                holder.mClienteProvider.crearCliente(cliente, mIdNegocio, userModel.getId()).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        holder.mNegocioProvider.getNegocio(mIdNegocio).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                if (snapshot.exists()){
                                    String tienda = snapshot.child("nombre").getValue().toString();
                                    String imagen = snapshot.child("imagen").getValue().toString();

                                    PedidoFinal pedido = new PedidoFinal(mIdNegocio, tienda, imagen, "ACEPTADO", userModel.getId(), 0.00, holder.fhoy, "");
                                    holder.mPedidoProvider.create(pedido, userModel.getId(), mIdNegocio).addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Intent intent = new Intent(mContext, ventasTienda.class);
                                            intent.putExtra("idNegocio", mIdNegocio);
                                            intent.putExtra("idUsuario", userModel.getId());
                                            mContext.startActivity(intent);
                                        }
                                    });
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {

                            }
                        });
                    }
                });
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @NonNull
    @Override
    public adaptadorUsuario.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_listausuarios, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private CircleImageView imgPerfil;
        private TextView txtNombre, txtDni, txtTelefono;
        private View mView;
        private clienteProvider mClienteProvider;
        private negocioProvider mNegocioProvider;
        private PedidoProvider mPedidoProvider;
        private String fhoy;
        private Date fechaActual;
        private DateFormat dateFormat;
        @RequiresApi(api = Build.VERSION_CODES.N)
        public ViewHolder(View view){
            super(view);
            mView = view;
            imgPerfil = view.findViewById(R.id.civUsuario);
            txtNombre = view.findViewById(R.id.tvNombre);
            txtDni = view.findViewById(R.id.tvDni);
            txtTelefono = view.findViewById(R.id.tvTelefono);
            mClienteProvider = new clienteProvider();
            mNegocioProvider = new negocioProvider();
            mPedidoProvider = new PedidoProvider();

            fechaActual = new Date();
            dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            fhoy = dateFormat.format(fechaActual);
        }
    }

}
