package com.inforad.cuchitaapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.PedidoFinal;
import com.inforad.cuchitaapp.ofrezco.pedidoDetalle;
import com.inforad.cuchitaapp.provider.NotificationProvider;
import com.inforad.cuchitaapp.provider.PedidoProvider;
import com.inforad.cuchitaapp.provider.TokenProvider;
import com.inforad.cuchitaapp.provider.UserProvider;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class pedidoPendienteNegocioAdaptador extends FirebaseRecyclerAdapter<PedidoFinal, pedidoPendienteNegocioAdaptador.ViewHolder> {

    private Context mContext;
    private TokenProvider mTokenProvider;
    private NotificationProvider mNotificationProvider;
    private FirebaseAuth mAuth;
    private String usuario_id;
    private PedidoProvider mPedido;
    private UserProvider mUserProvider;

    public pedidoPendienteNegocioAdaptador(FirebaseRecyclerOptions<PedidoFinal> options, Context context){
        super(options);
        mContext = context;
        mTokenProvider = new TokenProvider();
        mNotificationProvider = new NotificationProvider();
        mUserProvider = new UserProvider();
        mAuth = FirebaseAuth.getInstance();
        usuario_id = mAuth.getCurrentUser().getUid();
        mPedido = new PedidoProvider();

    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull PedidoFinal pedidoModel) {

        holder.mUserProvider.obtenerUsuario(pedidoModel.getIdCliente()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    holder.txtNomProducto.setText(snapshot.child("alias").getValue().toString());
                    String img = snapshot.child("imagen").getValue().toString();
                    Picasso.with(mContext).load(img).into(holder.imgCliente);
                }else {
                    holder.txtNomProducto.setText(pedidoModel.getCliente());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


        holder.txtCantidad.setText("El pedido llega en: " + pedidoModel.getTiempoEntrega() + "min");
        holder.txtPrecio.setText("S/." + pedidoModel.getPrecio());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, pedidoDetalle.class);
                intent.putExtra("idCliente", pedidoModel.getIdCliente());
                intent.putExtra("idNegocio", pedidoModel.getIdTienda());
                mContext.startActivity(intent);
            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_listapedidopendientenegocio, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private CircleImageView imgCliente;
        private TextView txtNomProducto, txtCantidad, txtPrecio;
        private View mView;
        private UserProvider mUserProvider;

        public ViewHolder(View view){
            super(view);

            mView = view;
            txtNomProducto = view.findViewById(R.id.tvProducto);
            txtCantidad = view.findViewById(R.id.tvCantidad);
            txtPrecio = view.findViewById(R.id.tvPrecio);
            imgCliente = view.findViewById(R.id.ivUsuario);
            mUserProvider = new UserProvider();
        }
    }

}
