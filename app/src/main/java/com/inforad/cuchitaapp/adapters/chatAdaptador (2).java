package com.inforad.cuchitaapp.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.Chat;

public class chatAdaptador extends FirebaseRecyclerAdapter<Chat, chatAdaptador.ViewHolder> {
    private Context mContext;

    public chatAdaptador(@NonNull FirebaseRecyclerOptions<Chat> options, Context context) {
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull chatAdaptador.ViewHolder holder, int position, @NonNull Chat chatModel) {
//final Chat chat =  chatModel.getClass(position);


        final int maxMargenHorizontal = mContext.getResources().getDimensionPixelSize(R.dimen.chat_margin_max_horizontal);
        final int maxMargenSuperior = mContext.getResources().getDimensionPixelSize(R.dimen.chat_margin_max_top);
        final int minMargen = mContext.getResources().getDimensionPixelSize(R.dimen.chat_margin_min);

        int gravity = Gravity.END;
        Drawable fondo = ContextCompat.getDrawable(mContext, R.drawable.chatenviar);
        int margenStart = maxMargenHorizontal;
        int margenTop = minMargen;
        int margenEnd = minMargen;

        if (!holder.enviadoPorMi(holder.idUsuario, chatModel.getEnvia())){
            gravity = Gravity.START;
            fondo = ContextCompat.getDrawable(mContext, R.drawable.chatrecibir);
            margenEnd = maxMargenHorizontal;
            margenStart = minMargen;
        }

        if (position > 0 && holder.enviadoPorMi(holder.idUsuario, chatModel.getEnvia())){
            margenTop = maxMargenSuperior;
        }

        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) holder.txtChat.getLayoutParams();
        params.gravity = gravity;
        params.setMargins(margenStart, margenTop, margenEnd, minMargen);

        holder.txtChat.setVisibility(View.VISIBLE);
        holder.txtChat.setText(chatModel.getMensaje());
        holder.txtChat.setBackground(fondo);

        /*
        if (chatModel.getMensaje() != null){
            holder.txtChat.setVisibility(View.VISIBLE);
        }else {
            holder.txtChat.setVisibility(View.GONE);
        }

         */
    }

    @NonNull
    @Override
    public chatAdaptador.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private AppCompatImageView foto;
        private TextView txtChat;
        private FirebaseAuth mAut;
        private String idUsuario;
        private View mView;

        private boolean enviadoPorMi(String miId, String idEnviado){
            boolean aux = true;

            if ( miId.equals(idEnviado) ){
                aux = true;
            }else {
                aux = false;
            }

            return aux;
        }

        public ViewHolder(View view){
            super(view);
            mView = view;
            foto = view.findViewById(R.id.imgFoto);
            txtChat = view.findViewById(R.id.tvMensaje);
            mAut = FirebaseAuth.getInstance();
            idUsuario = mAut.getCurrentUser().getUid();

        }
    }

}
