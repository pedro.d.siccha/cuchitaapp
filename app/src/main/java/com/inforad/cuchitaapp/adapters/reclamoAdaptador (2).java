package com.inforad.cuchitaapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.Reclamo;

public class reclamoAdaptador extends FirebaseRecyclerAdapter<Reclamo, reclamoAdaptador.ViewHolder> {
    private Context mContext;

    public reclamoAdaptador(@NonNull FirebaseRecyclerOptions<Reclamo> options, Context context) {
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull Reclamo reclamoModel) {
        holder.txtReclamo.setText(reclamoModel.getReclamo());
        holder.txtEstado.setText(reclamoModel.getEstado());
        if (reclamoModel.getEstado() == "PENDIENTE"){
            holder.txtRespuesta.setVisibility(View.GONE);
        }else {
            holder.txtRespuesta.setVisibility(View.VISIBLE);
            holder.txtRespuesta.setText(reclamoModel.getRespuesta());
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_reclamo, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txtReclamo, txtEstado, txtRespuesta;
        private View mView;

        public ViewHolder(View view){
            super(view);
            mView = view;
            txtReclamo = view.findViewById(R.id.tvReclamo);
            txtEstado = view.findViewById(R.id.tvEstado);
            txtRespuesta = view.findViewById(R.id.tvRespuesta);
        }
    }

}
