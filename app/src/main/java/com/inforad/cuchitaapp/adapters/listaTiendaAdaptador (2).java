package com.inforad.cuchitaapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.NegocioUsuario;
import com.inforad.cuchitaapp.ofrezco.miTienda;
import com.inforad.cuchitaapp.provider.negocioProvider;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class listaTiendaAdaptador extends FirebaseRecyclerAdapter<NegocioUsuario, listaTiendaAdaptador.ViewHolder> {
    private Context mContext;
    private FirebaseAuth mAuth;
    private String usuario_id;

    public listaTiendaAdaptador(@NonNull FirebaseRecyclerOptions<NegocioUsuario> options, Context context) {
        super(options);
        mContext = context;
        mAuth = FirebaseAuth.getInstance();
        usuario_id = mAuth.getCurrentUser().getUid();
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull NegocioUsuario negocioModel) {
        
        holder.mNegocioProvider.getNegocio(negocioModel.getIdTienda()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    holder.txtTienda.setText(snapshot.child("nombre").getValue().toString());
                    holder.txtCategoria.setText(snapshot.child("categoria").getValue().toString());
                    String img = snapshot.child("imagen").getValue().toString();
                    Picasso.with(mContext).load(img).into(holder.img);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, miTienda.class);
                intent.putExtra("negocio_id", negocioModel.getIdTienda());
                mContext.startActivity(intent);
            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_listienda, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private CircleImageView img;
        private TextView txtTienda, txtCategoria;
        private LinearLayout btnTienda;
        private View mView;
        private negocioProvider mNegocioProvider;
        public ViewHolder(View view){
            super(view);
            mView = view;
            img = view.findViewById(R.id.imgCategoriaTienda);
            txtTienda = view.findViewById(R.id.tvNombreTienda);
            txtCategoria = view.findViewById(R.id.tvCategoria);
            mNegocioProvider = new negocioProvider();
        }
    }
}
