package com.inforad.cuchitaapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.ClientePedido;
import com.inforad.cuchitaapp.provider.ClientePedidoProvider;

public class pedidoAceptadoAdaptador extends FirebaseRecyclerAdapter<ClientePedido, pedidoAceptadoAdaptador.ViewHolder> {

    private Context mContext;
    private ClientePedidoProvider mClientePedidoProvider;
    private FirebaseAuth mAuth;
    private String usuario_id;

    public pedidoAceptadoAdaptador(@NonNull FirebaseRecyclerOptions<ClientePedido> options, Context context) {
        super(options);

        mContext = context;
        mClientePedidoProvider = new ClientePedidoProvider();
        mAuth = FirebaseAuth.getInstance();
        usuario_id = mAuth.getCurrentUser().getUid();

    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull ClientePedido pedidoModel) {
        holder.txtNomProducto.setText(pedidoModel.getProducto());
        holder.txtCantidad.setText("Unidades: " + pedidoModel.getCantidad());
        holder.txtPrecio.setText("S/." + pedidoModel.getPrecio_total());
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_listapedidoaceptado, parent, false);
        return new ViewHolder(view);
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView imgProducto;
        private TextView txtNomProducto, txtCantidad, txtPrecio;
        private View mView;
        public ValueEventListener mListener;


        public ViewHolder(View view){
            super(view);

            mView = view;
            txtNomProducto = view.findViewById(R.id.tvProducto);
            txtCantidad = view.findViewById(R.id.tvCantidad);
            txtPrecio = view.findViewById(R.id.tvPrecio);

        }
    }
}
