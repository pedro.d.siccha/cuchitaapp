package com.inforad.cuchitaapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.Direccion;

public class direccionAdaptador extends FirebaseRecyclerAdapter<Direccion, direccionAdaptador.ViewHolder> {
    private Context mContext;

    public direccionAdaptador(@NonNull FirebaseRecyclerOptions<Direccion> options, Context context) {
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull Direccion direccionModel) {
        holder.txtDireccion.setText(direccionModel.getCiudad());
        holder.txtDonde.setText(direccionModel.getDireccion());
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext, "Es en: " + direccionModel.getDireccion(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_direccion, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txtDireccion, txtDonde;
        private View mView;

        public ViewHolder(View view){
            super(view);
            mView = view;
            txtDireccion = view.findViewById(R.id.tvDireccion);
            txtDonde = view.findViewById(R.id.tvNombre);
        }
    }

}
