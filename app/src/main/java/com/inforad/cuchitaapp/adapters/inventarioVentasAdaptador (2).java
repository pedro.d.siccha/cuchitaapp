package com.inforad.cuchitaapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.PedidoProducto;
import com.inforad.cuchitaapp.provider.productoProvider;
import com.squareup.picasso.Picasso;

public class inventarioVentasAdaptador extends FirebaseRecyclerAdapter<PedidoProducto, inventarioVentasAdaptador.ViewHolder> {
    Context mContext;
    String idNegocio, idUsuario;

    public inventarioVentasAdaptador(@NonNull FirebaseRecyclerOptions<PedidoProducto> options, Context context, String mIdNegocio, String mIdUsuario) {
        super(options);
        mContext = context;
        idNegocio = mIdNegocio;
        idUsuario = mIdUsuario;
    }

    @Override
    protected void onBindViewHolder(@NonNull inventarioVentasAdaptador.ViewHolder holder, int position, @NonNull PedidoProducto pedidoModel) {
        holder.txtProducto.setText(pedidoModel.getNombreProducto());
        holder.txtPrecio.setText("S/. " + pedidoModel.getPrecio());
        holder.mProductoProvider.obtenerProducto(pedidoModel.getIdProducto()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String imagen = snapshot.child("imgProducto").getValue().toString();
                    Picasso.with(mContext).load(imagen).into(holder.img);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
/*
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ventaLocal.class);
                intent.putExtra("idNegocio",idNegocio);
                intent.putExtra("idProducto",pedidoModel.getIdProducto());
                intent.putExtra("idUsuario", idUsuario);
                intent.putExtra("nomProducto", pedidoModel.getNombreProducto());
                mContext.startActivity(intent);
            }
        });

 */

    }

    @NonNull
    @Override
    public inventarioVentasAdaptador.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_venta_tienda, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txtProducto, txtPrecio;
        private ImageView img;
        private productoProvider mProductoProvider;
        private View mView;
        public ViewHolder(View view){
            super(view);
            mView = view;
            txtProducto = view.findViewById(R.id.tvProducto);
            txtPrecio = view.findViewById(R.id.tvPrecio);
            img = view.findViewById(R.id.imgProducto);
            mProductoProvider = new productoProvider();
        }
    }
}
