package com.inforad.cuchitaapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.Producto;
import com.inforad.cuchitaapp.necesito.agregarProducto;
import com.inforad.cuchitaapp.provider.negocioProvider;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class busquedaTiendaAdaptador extends RecyclerView.Adapter<busquedaTiendaAdaptador.viewholderProducto> {

    List<Producto> productoList;
    Context mContext;

    public busquedaTiendaAdaptador(List<Producto> productoList, Context context) {
        this.productoList = productoList;
        this.mContext = context;
    }

    @NonNull
    @Override
    public viewholderProducto onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_busquedaproducto, parent, false);
        viewholderProducto holder = new viewholderProducto(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull viewholderProducto holder, int position) {
        Producto pr = productoList.get(position);
        holder.txtProducto.setText(pr.getNombre());
        holder.txtPrecio.setText("S/." + pr.getPrecioUnidad());
        holder.txtTiempo.setText(pr.getTiempoEntrega() + "Min");
        String img = pr.getImgProducto();
        Picasso.with(mContext).load(img).into(holder.imgProducto);
        holder.mNegocioProvider.getNegocio(pr.getIdTienda()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    holder.txtTienda.setText(snapshot.child("nombre").getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, agregarProducto.class);
                intent.putExtra("idProducto", pr.getId());
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return productoList.size();
    }

    public class viewholderProducto extends RecyclerView.ViewHolder {
        private CircleImageView imgProducto;
        private TextView txtProducto, txtTienda, txtPrecio, txtTiempo;
        private negocioProvider mNegocioProvider;
        private View view;
        public viewholderProducto(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            imgProducto = itemView.findViewById(R.id.ivTienda);
            txtProducto = itemView.findViewById(R.id.tvTienda);
            txtPrecio = itemView.findViewById(R.id.tvCosto);
            txtTiempo = itemView.findViewById(R.id.tvTiempo);
            txtTienda = itemView.findViewById(R.id.tvDescProducto);

            mNegocioProvider = new negocioProvider();
        }
    }
}
