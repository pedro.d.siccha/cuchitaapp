package com.inforad.cuchitaapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.SeccionProducto;
import com.inforad.cuchitaapp.provider.negocioInventarioProvider;
import com.squareup.picasso.Picasso;

public class seccionAdaptadorProducto extends FirebaseRecyclerAdapter<SeccionProducto, seccionAdaptadorProducto.ViewHolder> {
    private Context mContext;

    public seccionAdaptadorProducto(@NonNull FirebaseRecyclerOptions<SeccionProducto> options, Context context) {
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull seccionAdaptadorProducto.ViewHolder holder, int position, @NonNull SeccionProducto productoModel) {

        holder.mInvetarioProvider.obtenerProducto(productoModel.getIdNegocio(), productoModel.getIdProducto()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    holder.producto.setText(snapshot.child("nombre").getValue().toString());
                    holder.stock.setText("Stock Actual: " + snapshot.child("stock").getValue().toString());
                    holder.detalle.setText(snapshot.child("detalle").getValue().toString());
                    holder.precio.setText("Precio por " + snapshot.child("unidadMedida").getValue().toString() + ": S/." + snapshot.child("precioUnidad").getValue().toString());
                    String imagen = snapshot.child("imgProducto").getValue().toString();
                    Picasso.with(mContext).load(imagen).into(holder.img);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @NonNull
    @Override
    public seccionAdaptadorProducto.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_producto, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView producto, stock, detalle, precio;
        private ImageView img;
        private negocioInventarioProvider mInvetarioProvider;
        private View mView;
        public ViewHolder(View view){
            super(view);
            mView = view;
            producto = view.findViewById(R.id.tvProducto);
            stock = view.findViewById(R.id.tvStock);
            img = view.findViewById(R.id.imgProducto);
            detalle = view.findViewById(R.id.tvDescripcion);
            precio = view.findViewById(R.id.tvPrecio);
            mInvetarioProvider = new negocioInventarioProvider();

        }
    }
}
