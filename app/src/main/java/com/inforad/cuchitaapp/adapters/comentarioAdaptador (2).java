package com.inforad.cuchitaapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.Comentario;
import com.inforad.cuchitaapp.provider.UserProvider;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class comentarioAdaptador extends FirebaseRecyclerAdapter<Comentario, comentarioAdaptador.ViewHolder> {
    private Context mContext;

    public comentarioAdaptador(@NonNull FirebaseRecyclerOptions<Comentario> options, Context context) {
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull comentarioAdaptador.ViewHolder holder, int position, @NonNull Comentario cometarioModel) {
        holder.txtComentario.setText(cometarioModel.getComentario());
        holder.mUserProvider.obtenerUsuario(cometarioModel.getIdUsuario()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                String img = snapshot.child("imagen").getValue().toString();
                Picasso.with(mContext).load(img).into(holder.imgUsuario);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        if (cometarioModel.getValoracion().equals("LIKE")){
            holder.imgLike.setVisibility(View.VISIBLE);
            holder.imgDisLike.setVisibility(View.GONE);
        }else {
            holder.imgLike.setVisibility(View.GONE);
            holder.imgDisLike.setVisibility(View.VISIBLE);
        }

    }

    @NonNull
    @Override
    public comentarioAdaptador.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comentario, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txtComentario;
        private ImageView imgLike, imgDisLike;
        private CircleImageView imgUsuario;
        private UserProvider mUserProvider;
        private View mView;
        public ViewHolder(View view){
            super(view);
            mView = view;
            txtComentario = view.findViewById(R.id.tvComentario);
            imgLike = view.findViewById(R.id.ivLike);
            imgDisLike = view.findViewById(R.id.ivDisLike);
            imgUsuario = view.findViewById(R.id.civUsuario);

            mUserProvider = new UserProvider();

        }
    }

}
