package com.inforad.cuchitaapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.chat.ViewChat;
import com.inforad.cuchitaapp.models.Contacto;
import com.inforad.cuchitaapp.provider.UserProvider;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class contactosAdaptador extends FirebaseRecyclerAdapter<Contacto, contactosAdaptador.ViewHolder> {

    private Context mContext;

    public contactosAdaptador(@NonNull FirebaseRecyclerOptions<Contacto> options, Context context) {
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull contactosAdaptador.ViewHolder holder, int position, @NonNull Contacto contactoModel) {
        holder.mUserProvider.obtenerUsuario(contactoModel.getIdContacto()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String img = snapshot.child("imagen").getValue().toString();
                    holder.txtNombre.setText(snapshot.child("alias").getValue().toString());
                    Picasso.with(mContext).load(img).into(holder.imgPerfil);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ViewChat.class);
                intent.putExtra("idEnvia", contactoModel.getIdPricipal());
                intent.putExtra("idRecibir", contactoModel.getIdContacto());
                mContext.startActivity(intent);
            }
        });
    }

    @NonNull
    @Override
    public contactosAdaptador.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_user, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private CircleImageView imgPerfil;
        private TextView txtNombre, txtEstado, txtSmsLeer;
        private FirebaseAuth mAuth;
        private String idUsuario;
        private UserProvider mUserProvider;
        private View mView;


        public ViewHolder(View view){
            super(view);
            mView = view;
            imgPerfil = view.findViewById(R.id.civFoto);
            txtNombre = view.findViewById(R.id.tvNombre);
            txtEstado = view.findViewById(R.id.tvEstado);
            txtSmsLeer = view.findViewById(R.id.tvContador);

            mUserProvider = new UserProvider();
            mAuth = FirebaseAuth.getInstance();
            idUsuario = mAuth.getCurrentUser().getUid();
        }
    }

}
