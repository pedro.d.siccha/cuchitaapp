package com.inforad.cuchitaapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.NegocioInventario;
import com.inforad.cuchitaapp.provider.negocioInventarioProvider;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class ofertaAdaptador extends FirebaseRecyclerAdapter<NegocioInventario, ofertaAdaptador.ViewHolder>{
    private Context mContext;

    public ofertaAdaptador(@NonNull FirebaseRecyclerOptions<NegocioInventario> options, Context context) {
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull ofertaAdaptador.ViewHolder holder, int position, @NonNull NegocioInventario inventarioModel) {
        String img = inventarioModel.getImgProducto();
        Picasso.with(mContext).load(img).into(holder.imgProducto);
        holder.txtNombre.setText(inventarioModel.getNombre());
        holder.txtPrecAnterior.setText("");
        holder.txtPrecNuevo.setText("Oferta del " + inventarioModel.getPrecioUnidad());

    }

    @NonNull
    @Override
    public ofertaAdaptador.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ofertas, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private CircleImageView imgProducto;
        private TextView txtNombre, txtPrecAnterior, txtPrecNuevo;
        private View mView;
        protected negocioInventarioProvider mInventarioProvider;
        public ViewHolder(View view){
            super(view);
            mView = view;
            imgProducto = view.findViewById(R.id.civProducto);
            txtNombre = view.findViewById(R.id.tvNombre);
            txtPrecAnterior = view.findViewById(R.id.tvPrecAnterior);
            txtPrecNuevo = view.findViewById(R.id.tvPrecNuevo);

            mInventarioProvider = new negocioInventarioProvider();
        }
    }

}
