package com.inforad.cuchitaapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.Anuncios;
import com.inforad.cuchitaapp.ofrezco.detallesAnuncio;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class anuncioTiendaAdaptador extends FirebaseRecyclerAdapter<Anuncios, anuncioTiendaAdaptador.ViewHolder> {
    private Context mContext;

    public anuncioTiendaAdaptador(@NonNull FirebaseRecyclerOptions<Anuncios> options, Context context) {
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull Anuncios anuncioModel) {
        holder.txtTitulo.setText(anuncioModel.getTitulo());
        holder.txtDescripcion.setText(anuncioModel.getDescripcion());
        holder.txtFecCreacion.setText(anuncioModel.getFecInicio());
        String imagen = anuncioModel.getImagen();
        Picasso.with(mContext).load(imagen).into(holder.img);
        holder.txtDuracion.setText(anuncioModel.getTiempoPublicacion());
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, detallesAnuncio.class);
                intent.putExtra("imagen", anuncioModel.getImagen());
                intent.putExtra("titulo", anuncioModel.getTitulo());
                intent.putExtra("numero", anuncioModel.getDescripcion());
                intent.putExtra("descripcion", anuncioModel.getFecInicio());
                intent.putExtra("duracion", anuncioModel.getTiempoPublicacion());
                mContext.startActivity(intent);
            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_listanunciostienda, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private CircleImageView img;
        private TextView txtTitulo, txtDescripcion, txtFecCreacion, txtDuracion;
        private View mView;
        public ViewHolder(View view){
            super(view);
            mView = view;
            img = view.findViewById(R.id.ivFotoAnuncio);
            txtTitulo = view.findViewById(R.id.tvTitulo);
            txtDescripcion = view.findViewById(R.id.tvDescripcion);
            txtFecCreacion = view.findViewById(R.id.tvFecCreacion);
            txtDuracion = view.findViewById(R.id.txtDuracion);
        }
    }
}
