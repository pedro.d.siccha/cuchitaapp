package com.inforad.cuchitaapp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.NegocioAnuncio;

import java.util.ArrayList;

public class anuncioAdapter extends RecyclerView.Adapter<anuncioAdapter.ViewHolder> {

    private int resource;
    private ArrayList<NegocioAnuncio> anunciosList;

    public anuncioAdapter(ArrayList<NegocioAnuncio> anunciosList, int resource){
        this.anunciosList = anunciosList;
        this.resource = resource;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(resource, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        NegocioAnuncio anuncio = anunciosList.get(position);

        holder.mtxtAnuncio.setText(anuncio.getTitulo());

    }

    @Override
    public int getItemCount() {
        return anunciosList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView mtxtAnuncio;
        private ImageView mimgPost;

        public View view;
        public ViewHolder(View view){
            super(view);

            this.view = view;
            this.mtxtAnuncio = (TextView) view.findViewById(R.id.txtAnuncio);
            this.mimgPost = (ImageView) view.findViewById(R.id.ivSpot);
        }
    }

}
