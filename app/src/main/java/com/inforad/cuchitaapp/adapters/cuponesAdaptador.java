package com.inforad.cuchitaapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.Cupones;
import com.inforad.cuchitaapp.provider.negocioInventarioProvider;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class cuponesAdaptador extends FirebaseRecyclerAdapter<Cupones, cuponesAdaptador.ViewHolder> {
    private Context mContext;

    public cuponesAdaptador(@NonNull FirebaseRecyclerOptions<Cupones> options, Context context) {
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull cuponesAdaptador.ViewHolder holder, int position, @NonNull Cupones cuponesModel) {

        holder.mInventarioProvider.obtenerProducto(cuponesModel.getIdNegocio(), cuponesModel.getIdProducto()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String img = snapshot.child("imgProducto").getValue().toString();
                    Picasso.with(mContext).load(img).into(holder.imgProducto);
                    holder.txtNombre.setText(snapshot.child("nombre").getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        holder.txtPrecAnterior.setText("Antes: S/. " + cuponesModel.getPrecioAnterior());
        holder.txtPrecNuevo.setText("Ahora: S/. " + cuponesModel.getNuevoPrecio());

    }

    @NonNull
    @Override
    public cuponesAdaptador.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ofertas, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private CircleImageView imgProducto;
        private TextView txtNombre, txtPrecAnterior, txtPrecNuevo;
        private View mView;
        protected negocioInventarioProvider mInventarioProvider;
        public ViewHolder(View view){
            super(view);
            mView = view;
            imgProducto = view.findViewById(R.id.civProducto);
            txtNombre = view.findViewById(R.id.tvNombre);
            txtPrecAnterior = view.findViewById(R.id.tvPrecAnterior);
            txtPrecNuevo = view.findViewById(R.id.tvPrecNuevo);

            mInventarioProvider = new negocioInventarioProvider();
        }
    }

}
