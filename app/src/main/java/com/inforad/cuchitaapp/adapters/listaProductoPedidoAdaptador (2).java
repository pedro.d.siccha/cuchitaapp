package com.inforad.cuchitaapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.Producto;
import com.inforad.cuchitaapp.ofrezco.ventaLocal;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class listaProductoPedidoAdaptador extends FirebaseRecyclerAdapter<Producto, listaProductoPedidoAdaptador.ViewHolder> {
    private Context mContext;
    private String mDni, mNombre;

    public listaProductoPedidoAdaptador(@NonNull FirebaseRecyclerOptions<Producto> options, Context context, String dni, String nombre) {
        super(options);
        mContext = context;
        mDni = dni;
        mNombre = nombre;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull Producto productoModel) {
        //String cadena = String.valueOf(productoModel.getPrecioUnidad());
        String img = productoModel.getImgProducto();
        Picasso.with(mContext).load(img).into(holder.imgProducto);
        holder.producto.setText(productoModel.getNombre());
        holder.descripcion.setText(productoModel.getDetalle());
        holder.precio.setText("S/." + productoModel.getPrecioUnidad());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ventaLocal.class);
                intent.putExtra("idNegocio", productoModel.getIdTienda());
                intent.putExtra("idProducto", productoModel.getId());
                intent.putExtra("idUsuario", mDni);
                intent.putExtra("nomProducto", productoModel.getNombre());
                intent.putExtra("dni", mDni);
                intent.putExtra("nombre", mNombre);
                mContext.startActivity(intent);
            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lista_producto_pedido, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView producto, precio, descripcion;
        private CircleImageView imgProducto;
        private View mView;
        public ViewHolder(View view){
            super(view);
            mView = view;
            producto = view.findViewById(R.id.tvProducto);
            descripcion = view.findViewById(R.id.tvDetalles);
            precio = view.findViewById(R.id.tvPrecio);
            imgProducto = view.findViewById(R.id.civProducto);
        }
    }
}
