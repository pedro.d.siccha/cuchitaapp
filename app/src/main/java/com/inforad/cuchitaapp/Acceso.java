package com.inforad.cuchitaapp;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import dmax.dialog.SpotsDialog;

public class Acceso extends AppCompatActivity {

    private static final int RC_SIGN_IN = 123;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    DatabaseReference mDatabase;
    Button botonIngresar;
    TextInputEditText txtPassword, txtEmail;
    AlertDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_acceso);

        botonIngresar = findViewById(R.id.btnIniciarSesion);
        txtEmail = findViewById(R.id.inputUsuario);
        txtPassword = findViewById(R.id.inputPassword);
        mDialog = new SpotsDialog.Builder().setContext(Acceso.this).setMessage("Espere la verificación").build();
        mFirebaseAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                //Este codigo se ejecuta cada vez que se inicia o se cierra sesion
                FirebaseUser user = firebaseAuth.getCurrentUser(); //Extraemos un usuario y lo almacenamos en la varible user

                if (user != null){

                    String id_registro = user.getUid();

                    mDatabase.child("Users").child(id_registro).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            //Aquí se obtienen los datos

                            if (dataSnapshot.exists()){ //Verificamos si existe el nodo mencionado
                                Toast.makeText(Acceso.this, "Registrado", Toast.LENGTH_LONG).show();
                                //Obtenemos los datos campo por campo

                                Intent intent = new Intent(Acceso.this, tipoUsuario.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }else {
                                Intent intent = new Intent(Acceso.this, Registrar.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {
                            Toast.makeText(Acceso.this, error.toString(), Toast.LENGTH_SHORT).show();
                        }
                    });

                }else {
                    //Usuario sin sesion activa o acaba de cerrar sesion
                    /*
                    AuthUI.IdpConfig facebookIdp = new AuthUI.IdpConfig.FacebookBuilder().setPermissions(Arrays.asList("user_friends", "user_gender")).build();
                    AuthUI.IdpConfig googleIdp = new AuthUI.IdpConfig.GoogleBuilder().build();
                    AuthMethodPickerLayout loguinPersonalizado = new AuthMethodPickerLayout.Builder(R.layout.activity_acceso).setFacebookButtonId(R.id.btnFacebook).setGoogleButtonId(R.id.btnGoogle).build();

                    startActivityForResult(AuthUI.getInstance()
                                           .createSignInIntentBuilder() //Se crea la vista del loguin
                                           .setIsSmartLockEnabled(false) //Recordar cuentas y contraseñas de logueo anterior
                                           .setTosAndPrivacyPolicyUrls("https://cuchitapp.com", "https://cuchitapp.com") //Asignar Politicas de privacidad
                                           .setAvailableProviders(Arrays.asList(facebookIdp, googleIdp)).setAuthMethodPickerLayout(loguinPersonalizado).build(), RC_SIGN_IN);

                     */
                }

            }
        };
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN){
            if (resultCode == RESULT_OK){
                //Aquí ya esta iniciada la sesion, se puede ejecutar la siguiente accion
                Toast.makeText(this, "Bienvenido... Sesion iniciada correctamente", Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(this, "Error... No se pudo iniciar sesion", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void login(){
        String email = txtEmail.getText().toString();
        String pass = txtPassword.getText().toString();

        if(!email.isEmpty() && !pass.isEmpty()){
            if(pass.length() >= 6){
                mFirebaseAuth.signInWithEmailAndPassword(email, pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            Toast.makeText(Acceso.this, "Bienvenido Login Normal....", Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(Acceso.this, "La contraseña es incorrecta", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        mFirebaseAuth.addAuthStateListener(mAuthStateListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAuthStateListener != null){
            mFirebaseAuth.removeAuthStateListener(mAuthStateListener);
        }
    }
}