package com.inforad.cuchitaapp.procesos;

import com.google.firebase.auth.FirebaseAuth;

public class procesador {

    FirebaseAuth mAuth;

    public String obtenerId(){
        return mAuth.getInstance().getCurrentUser().getUid();
    }

    public boolean existeSesion(){
        boolean existe = false;
        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() != null){
            existe = true;
        }
        return existe;
    }

}
