package com.inforad.cuchitaapp.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.cuchitaapp.models.SeccionProducto;

public class seccionProductoProvider {
    DatabaseReference mDatabase;

    public seccionProductoProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Seccion_Producto");
    }

    public Task<Void> crearSeccionProducto(SeccionProducto seccion, String idNegocio, String idSeccion, String idProducto){
        return mDatabase.child(idNegocio).child(idSeccion).child(idProducto).setValue(seccion);
    }

    public DatabaseReference obtenerSeccion(String idNegocio){
        return mDatabase.child(idNegocio);
    }

    public DatabaseReference obtenerSeccionProducto(String idNegocio, String idSeccion){
        return mDatabase.child(idNegocio).child(idSeccion);
    }

}
