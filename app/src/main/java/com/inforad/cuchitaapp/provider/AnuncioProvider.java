package com.inforad.cuchitaapp.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.cuchitaapp.models.Anuncios;

public class AnuncioProvider {
    DatabaseReference mDatabase;

    public AnuncioProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Anuncios");
    }

    public Task<Void> create(Anuncios anuncio){
        return mDatabase.push().setValue(anuncio);
    }

    public DatabaseReference obtenerAnucios( String idNegocio){
        return mDatabase;
    }
}
