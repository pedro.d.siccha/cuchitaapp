package com.inforad.cuchitaapp.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.cuchitaapp.models.Negocio;

import java.util.HashMap;
import java.util.Map;

public class negocioProvider {

    private DatabaseReference mDatabase;

    public negocioProvider(){
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Negocio");
    }

    public DatabaseReference getDatosNegocio(String negocio_id){
        return mDatabase.child(negocio_id);
    }

    public Task<Void> crearNegocio(Negocio negocio, String idUsuario, String ruc){
        return mDatabase.child(ruc).setValue(negocio);
    }

    public Task<Void> actualizar(Negocio negocio){
        Map<String, Object> map = new HashMap<>();
        map.put("nombre", negocio.getNombre());
        map.put("imagen", negocio.getImagen());
        return mDatabase.child(negocio.getId()).updateChildren(map);
    }

    public DatabaseReference getNegocio(String idNegocio){
        return mDatabase.child(idNegocio);
    }


}
