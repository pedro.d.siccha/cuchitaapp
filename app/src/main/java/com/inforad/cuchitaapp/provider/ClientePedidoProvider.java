package com.inforad.cuchitaapp.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.cuchitaapp.models.ClientePedido;

import java.util.HashMap;
import java.util.Map;

public class ClientePedidoProvider {

    private DatabaseReference mDatabase;

    public ClientePedidoProvider() {

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Cliente_Pedido");

    }

    public Task<Void> create(ClientePedido clientePedido){
        return mDatabase.child(clientePedido.getUsuario_id()).child(clientePedido.getProducto_id()).setValue(clientePedido);
    }

    public Task<Void> actualizarEstado(String idClientePedido, String idProducto, String estado){
        Map<String, Object> map = new HashMap<>();
        map.put("estado", estado);
        return mDatabase.child(idClientePedido).child(idProducto).updateChildren(map);
    }

    public DatabaseReference getEstado( String idClientePedido, String idProducto){
        return mDatabase.child(idClientePedido).child(idProducto).child("estado");
    }

}
