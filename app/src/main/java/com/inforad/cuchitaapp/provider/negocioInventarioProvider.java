package com.inforad.cuchitaapp.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class negocioInventarioProvider {

    private DatabaseReference mDatabase;

    public negocioInventarioProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Negocio_Inventario");
    }

    public Task<Void> actualizar(String idNegocio, String idProducto, String nombre, String precioUnidad, String stock, String stockMaximo, String stockMinimo, String unidadMedida){
        Map<String, Object> map = new HashMap<>();
        map.put("nombre", nombre);
        map.put("precioUnidad", precioUnidad);
        map.put("stock", stock);
        map.put("stockMaximo", stockMaximo);
        map.put("stockMinimo", stockMinimo);
        map.put("unidadMedida", unidadMedida);
        return mDatabase.child(idNegocio).child(idProducto).updateChildren(map);
    }

    public DatabaseReference obtenerEstado( String idNegocio, String idProducto){
        return mDatabase.child(idNegocio).child(idProducto).child("estado");
    }

    public Task<Void> actualizarEstado(String idNegocio, String idProducto, String estado){
        Map<String, Object> map = new HashMap<>();
        map.put("estado", estado);
        return mDatabase.child(idNegocio).child(idProducto).updateChildren(map);
    }

    public Task<Void> actualizarStock(String idNegocio, String idProducto, String stock){
        Map<String, Object> map = new HashMap<>();
        map.put("stock", stock);
        return mDatabase.child(idNegocio).child(idProducto).updateChildren(map);
    }

    public DatabaseReference obtenerProducto( String idNegocio, String idProducto){
        return mDatabase.child(idNegocio).child(idProducto);
    }
}
