package com.inforad.cuchitaapp.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.cuchitaapp.models.Producto;

import java.util.HashMap;
import java.util.Map;

public class productoProvider {

    DatabaseReference mDatabase;

    public productoProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Producto");
    }

    public Task<Void> create(Producto producto, String idProducto){
        return mDatabase.child(idProducto).setValue(producto);
    }

    public DatabaseReference obtenerProducto(String idProducto){
        return mDatabase.child(idProducto);
    }

    public Task<Void> actualizarStock(String idProducto, String estado){
        Map<String, Object> map = new HashMap<>();
        map.put("stock", estado);
        return mDatabase.child(idProducto).updateChildren(map);
    }

}
