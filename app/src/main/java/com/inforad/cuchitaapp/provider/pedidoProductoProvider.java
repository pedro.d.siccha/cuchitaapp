package com.inforad.cuchitaapp.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.cuchitaapp.models.PedidoProducto;

import java.util.HashMap;
import java.util.Map;

public class pedidoProductoProvider {

    DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private FirebaseUser user;

    public pedidoProductoProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Pedido");
    }

    private String obtenerIdUsuario(){
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        String user_id = user.getUid();
        return user_id;
    }

    public Task<Void> create(PedidoProducto producto, String idTienda, String idUsuario, String idProducto){
        return mDatabase.child(idTienda).child(idUsuario).child("Producto").child(idProducto).setValue(producto);
    }

    public DatabaseReference obtenerPedido(String idNegocio, String idUsuario){
        return mDatabase.child(idNegocio).child(idUsuario);
    }

    public DatabaseReference obtenerProductoProvider(String idNegocio, String idCliente){
        return mDatabase.child(idNegocio).child(idCliente).child("Producto");
    }

    public Task<Void> actualizarEstado(String idNegocio, String idUsuario, String estado){
        Map<String, Object> map = new HashMap<>();
        map.put("estado", estado);
        return mDatabase.child(idNegocio).child(idUsuario).updateChildren(map);
    }

    public Task<Void> actualizarPrecio(String idNegocio, String idUsuario, double precio){
        Map<String, Object> map = new HashMap<>();
        map.put("precio", precio);
        return mDatabase.child(idNegocio).child(idUsuario).updateChildren(map);
    }

}
