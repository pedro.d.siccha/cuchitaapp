package com.inforad.cuchitaapp.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.cuchitaapp.models.tipoUsuarioModel;

public class tipoUsuarioProvider {

    private DatabaseReference mDatabase;

    public tipoUsuarioProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("tipo_usuario");
    }

    public Task<Void> guardarTipoUsuario(tipoUsuarioModel tipoUsuario, String tipo){
        return mDatabase.child(tipo).push().setValue(tipoUsuario);
    }

    public DatabaseReference obtenerTipoUsuario(String tipoUsuario){
        return mDatabase.child(tipoUsuario);
    }

}
