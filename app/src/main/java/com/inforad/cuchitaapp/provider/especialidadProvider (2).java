package com.inforad.cuchitaapp.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.cuchitaapp.models.Profesional;

public class especialidadProvider {

    DatabaseReference mDatabase;

    public especialidadProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Especialidad");
    }

    public Task<Void> create(Profesional profesional){
        return mDatabase.push().setValue(profesional);
    }

}
