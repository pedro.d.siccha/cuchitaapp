package com.inforad.cuchitaapp.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.cuchitaapp.models.Profesional;

public class profesionalProvider {

    DatabaseReference mDatabase;

    public profesionalProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Profesional");
    }

    public Task<Void> create(Profesional profesional, String idUsuario){
        return mDatabase.child(idUsuario).setValue(profesional);
    }
}
