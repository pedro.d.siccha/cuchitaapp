package com.inforad.cuchitaapp.provider;

import com.inforad.cuchitaapp.models.FCMBody;
import com.inforad.cuchitaapp.models.FCMResponse;
import com.inforad.cuchitaapp.retrofit.IFCMApi;
import com.inforad.cuchitaapp.retrofit.retrofitCliente;

import retrofit2.Call;

public class NotificationProvider {

    private String url = "https://fcm.googleapis.com";

    public NotificationProvider() {
    }

    public Call<FCMResponse> sendNotification(FCMBody body){
        return retrofitCliente.getClientObject(url).create(IFCMApi.class).send(body);

    }
}
