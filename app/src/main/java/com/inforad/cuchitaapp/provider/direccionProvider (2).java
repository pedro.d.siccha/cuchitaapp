package com.inforad.cuchitaapp.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.cuchitaapp.models.Direccion;

public class direccionProvider {

    private DatabaseReference mDatabase;

    public direccionProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("direccion_local");
    }

    public Task<Void> guardarDireccion(Direccion direccion, String idUsuario, String tipo){
        return mDatabase.child(idUsuario).child(tipo).setValue(direccion);
    }

    public DatabaseReference obtenerDireccion(String idUsuario){
        return mDatabase.child(idUsuario);
    }

    public DatabaseReference obtenerDireccionLista(String idUsuario, String categoria){
        return mDatabase.child(idUsuario).child(categoria);
    }

}
