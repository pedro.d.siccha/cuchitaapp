package com.inforad.cuchitaapp.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.cuchitaapp.models.ListaCompra;

import java.util.HashMap;
import java.util.Map;

public class ListCompraProvider {

    private DatabaseReference mDatabase;

    public ListCompraProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Cliente_ListaCompra");
    }

    public Task<Void> create(ListaCompra listaCompra){
        return mDatabase.child(listaCompra.getUsuario_id()).setValue(listaCompra);
    }

    public DatabaseReference obtenerEstado( String idUsuario){
        return mDatabase.child(idUsuario).child("estado");
    }

    public Task<Void> actualizarEstado(String idUsuario, String estado){
        Map<String, Object> map = new HashMap<>();
        map.put("estado", estado);
        return mDatabase.child(idUsuario).updateChildren(map);
    }

    public DatabaseReference obtenerListaCompra(String idUsuario){
        return mDatabase.child(idUsuario);
    }

}
