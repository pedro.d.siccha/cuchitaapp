package com.inforad.cuchitaapp.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.cuchitaapp.models.ProfesionalProyecto;

public class profesionalProyectoProvider {

    DatabaseReference mDatabase;

    public profesionalProyectoProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Especialidad");
    }

    public Task<Void> create(ProfesionalProyecto profesionalProyecto){
        return mDatabase.push().setValue(profesionalProyecto);
    }

}
