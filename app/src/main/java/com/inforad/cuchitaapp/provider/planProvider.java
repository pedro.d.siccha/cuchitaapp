package com.inforad.cuchitaapp.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.cuchitaapp.models.Plan;

public class planProvider {
    DatabaseReference mDatabase;

    public planProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Plan");
    }

    public Task<Void> crearPlanNegocio(Plan plan, String idUsuario, String idTienda){
        return mDatabase.child("Negocio").child(idUsuario).child(idTienda).setValue(plan);
    }

    public Task<Void> crearPlanCliente(Plan plan, String idUsuario){
        return mDatabase.child("Cliente").child(idUsuario).setValue(plan);
    }

    public DatabaseReference obtenerPlanNegocio(String idUsuario, String idTienda){
        return mDatabase.child("Negocio").child(idUsuario).child(idTienda);
    }

    public DatabaseReference obtenerPlanCliente(String idUsuario){
        return mDatabase.child("Cliente").child(idUsuario);
    }

}
