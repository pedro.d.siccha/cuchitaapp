package com.inforad.cuchitaapp.provider;

import com.google.firebase.auth.FirebaseAuth;

public class AuthProvider {

    public FirebaseAuth mAuth;

    public AuthProvider(){
        mAuth = FirebaseAuth.getInstance();
    }

    public void cerrarSesion(){
        mAuth.signOut();
    }

    public String usuario_id(){
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    public boolean existeSesion(){
        boolean existe = false;
        if (mAuth.getCurrentUser() != null){
            existe = true;
        }
        return existe;
    }

}
