package com.inforad.cuchitaapp.provider;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class GeofireProvider {

    private DatabaseReference mDatabase;
    private GeoFire mGeofire;

    public GeofireProvider(){
        mDatabase = FirebaseDatabase.getInstance().getReference().child("direccion");
        mGeofire = new GeoFire(mDatabase);
    }

    public  void guadarLocalizacion(String idUsuario, LatLng latLng){
        mGeofire.setLocation(idUsuario, new GeoLocation(latLng.latitude, latLng.longitude));
    }

    public void guadarDireccion(String idUsuario, LatLng latLng){
        mGeofire.setLocation(idUsuario, new GeoLocation(latLng.latitude, latLng.longitude));
    }

    public void eliminarLocalizacion(String idUsuario){
        mGeofire.removeLocation(idUsuario);
    }

}
