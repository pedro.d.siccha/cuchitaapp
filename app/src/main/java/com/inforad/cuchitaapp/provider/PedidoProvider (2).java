package com.inforad.cuchitaapp.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.cuchitaapp.models.PedidoFinal;

import java.util.HashMap;
import java.util.Map;

public class PedidoProvider {

    DatabaseReference mDatabase;

    public PedidoProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Pedido");
    }

    public Task<Void> create(PedidoFinal pedido, String idTienda, String idCliente){
        return mDatabase.child(idTienda).child(idCliente).setValue(pedido);
    }

    public DatabaseReference obtenerEstado( String idTienda, String idCliente){
        return mDatabase.child(idTienda).child(idCliente).child("estado");
    }

    public Task<Void> actualizarEstado(String idTienda, String idCliente, String estado){
        Map<String, Object> map = new HashMap<>();
        map.put("estado", estado);
        return mDatabase.child(idTienda).child(idCliente).updateChildren(map);
    }

    public DatabaseReference obtenerPedido( String idTienda, String idCliente){
        return mDatabase.child(idTienda).child(idCliente);
    }

    public Task<Void> actualizarPrecio(String idTienda, String idCliente, double precio){
        Map<String, Object> map = new HashMap<>();
        map.put("precio", precio);
        return mDatabase.child(idTienda).child(idCliente).updateChildren(map);
    }

}
