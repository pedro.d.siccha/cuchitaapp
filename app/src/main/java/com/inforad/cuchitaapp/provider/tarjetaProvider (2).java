package com.inforad.cuchitaapp.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.cuchitaapp.models.Tarjeta;

public class tarjetaProvider {

    DatabaseReference mDatabase;

    public tarjetaProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Tarjeta");
    }

    public Task<Void> crear(Tarjeta tarjeta, String idUsuario, String idTarjeta){
        return mDatabase.child(idUsuario).child(idTarjeta).setValue(tarjeta);
    }

    public DatabaseReference obtenerTarjeta(String idUsuario, String idTarjeta){
        return mDatabase.child(idUsuario).child(idTarjeta);
    }
}
