package com.inforad.cuchitaapp.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.cuchitaapp.models.User;

import java.util.HashMap;
import java.util.Map;

public class UserProvider {
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;

    public UserProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Users");
        mAuth = FirebaseAuth.getInstance();
    }

    public Task<Void> crear(User user){
        return mDatabase.child(mAuth.getCurrentUser().getUid()).setValue(user);
    }

    public Task<Void> actualizarUsuario(String idUsuario, String alias, String apellido, String dni, String nombre, String telefono){
        Map<String, Object> map = new HashMap<>();
        map.put("alias", alias);
        map.put("apellido", apellido);
        map.put("dni", dni);
        map.put("nombre", nombre);
        map.put("telefono", telefono);
        return mDatabase.child(idUsuario).updateChildren(map);
    }

    public DatabaseReference obtenerUsuario(String idUsuario) {
        return mDatabase.child(idUsuario);
    }

    public Task<Void> actualizarPlan(String idUsuario, String plan){
        Map<String, Object> map = new HashMap<>();
        map.put("plan", plan);
        return mDatabase.child(idUsuario).updateChildren(map);
    }

    public Task<Void> actualizarEstado(String idUsuario, String estado){
        Map<String, Object> map = new HashMap<>();
        map.put("estado", estado);
        return mDatabase.child(idUsuario).updateChildren(map);
    }

    public Task<Void> actualizarActividad(String idUsuario, String actividad){
        Map<String, Object> map = new HashMap<>();
        map.put("actividad", actividad);
        return mDatabase.child(idUsuario).updateChildren(map);
    }

}
