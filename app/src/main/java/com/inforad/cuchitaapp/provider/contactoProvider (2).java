package com.inforad.cuchitaapp.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.cuchitaapp.models.Contacto;

public class contactoProvider {
    DatabaseReference mDatabase;

    public contactoProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference();
    }

    public Task<Void> crearContactoUsuario(Contacto contacto, String idUserPrincipal, String idUserContacto){
        return mDatabase.child("Users").child(idUserPrincipal).child("Contacto").child(idUserContacto).setValue(contacto);
    }

    public Task<Void> crearContactoNegocio(Contacto contacto, String idUserPrincipal, String idUserContacto){
        return mDatabase.child("Negocio").child(idUserPrincipal).child("Contacto").child(idUserContacto).setValue(contacto);
    }

}
