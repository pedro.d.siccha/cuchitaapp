package com.inforad.cuchitaapp.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.cuchitaapp.models.Seccion;

public class seccionProvider {

    DatabaseReference mDatabase;

    public seccionProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Seccion");
    }

    public Task<Void> crearSeccion(Seccion seccion, String idNegocio, String nombre){
        return mDatabase.child(idNegocio).child(nombre).setValue(seccion);
    }

    public DatabaseReference obtenerSeccion(String idNegocio){
        return mDatabase.child(idNegocio);
    }

}
