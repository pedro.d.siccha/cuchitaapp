package com.inforad.cuchitaapp.provider;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.cuchitaapp.models.Cliente;
import com.google.android.gms.tasks.Task;

public class clienteProvider {
    private DatabaseReference mDatabase;

    public clienteProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Negocio");
    }

    public Task<Void> crearCliente(Cliente cliente, String idNegocio, String idUsuario){
        return mDatabase.child(idNegocio).child("Cliente").child(idUsuario).setValue(cliente);
    }

}