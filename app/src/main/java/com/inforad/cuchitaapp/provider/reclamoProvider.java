package com.inforad.cuchitaapp.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.cuchitaapp.models.Reclamo;

public class reclamoProvider {
    DatabaseReference mDatabase;

    public reclamoProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Reclamo");
    }

    public Task<Void> create(Reclamo reclamo, String idUsuario){
        return mDatabase.child(idUsuario).push().setValue(reclamo);
    }
}
