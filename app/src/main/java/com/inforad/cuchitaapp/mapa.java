package com.inforad.cuchitaapp;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.RectangularBounds;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.maps.android.SphericalUtil;
import com.inforad.cuchitaapp.provider.AuthProvider;
import com.inforad.cuchitaapp.provider.GeofireProvider;
import com.inforad.cuchitaapp.provider.TokenProvider;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class mapa extends AppCompatActivity implements OnMapReadyCallback {

    private AuthProvider mAuthProvider;
    private GoogleMap mMap;
    private SupportMapFragment mMapFragment;
    private LocationRequest mLocationRequest;
    private FusedLocationProviderClient mFusedLocation;
    private final static int LOCATION_REQUEST_CODE = 1;
    private final static int SETTINGS_REQUEST_CODE = 2;
    private Marker mMarker;
    private Button btnConectar;
    private boolean isConnectar = false;
    private LatLng mCurrentLatLng;
    private GeofireProvider mGeofireProvider;
    private FirebaseAuth mFirebaseAuth;
    private AutocompleteSupportFragment mAutocomplete;
    private PlacesClient mPlaces;
    private String direccion, ciudad, pais, barrio;
    private LatLng direccionLatLng;
    private GoogleMap.OnCameraIdleListener mCameraListener;
    private TokenProvider mTokenProvide;
    private Spinner cbUbicacion;
    private double latitud, longitud;


    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            for (Location location : locationResult.getLocations()) {
                if (getApplicationContext() != null) {
                    //Obtener la localizazion del usuario en tiempo real
                    mCurrentLatLng = new LatLng(location.getLatitude(), location.getLongitude());
                    mMap.moveCamera(CameraUpdateFactory.newCameraPosition(
                            new CameraPosition.Builder().target(new LatLng(location.getLatitude(), location.getLongitude())).zoom(15f).build()
                    ));
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_mapa);

        mFusedLocation = LocationServices.getFusedLocationProviderClient(this);
        mGeofireProvider = new GeofireProvider();
        mFirebaseAuth = FirebaseAuth.getInstance();
        cbUbicacion = findViewById(R.id.spUnicacion);
        String[] ubicacion = {"CASA", "TRABAJO", "OFICINA"};
        ArrayList<String> arrayList = new ArrayList<>(Arrays.asList(ubicacion));
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, R.layout.style_spinner, arrayList);
        cbUbicacion.setAdapter(arrayAdapter);

        mMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapa);
        mMapFragment.getMapAsync(this);

        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), getResources().getString(R.string.google_maps_key));
        }

        mPlaces = Places.createClient(this);
        instanceAutocompleteDireccion();
        onCameraMove();

        mTokenProvide = new TokenProvider();
        generateToken();

        btnConectar = findViewById(R.id.btnConectarMapa);
        btnConectar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mapa.this, confirDireccion.class);
                intent.putExtra("direccion", direccion);
                intent.putExtra("ciudad", ciudad);
                intent.putExtra("pais", pais);
                intent.putExtra("latitud", latitud);
                intent.putExtra("longitud", longitud);
                intent.putExtra("tipoDireccion", cbUbicacion.getSelectedItem().toString());
                intent.putExtra("barrio", barrio);
                startActivity(intent);
            }
        });
    }

    private void instanceAutocompleteDireccion() {
        mAutocomplete = (AutocompleteSupportFragment) getSupportFragmentManager().findFragmentById(R.id.placesAutocomplete);
        mAutocomplete.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.LAT_LNG, Place.Field.NAME));
        mAutocomplete.setHint("Dirección");
        mAutocomplete.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(@NonNull Place place) {
                direccion = place.getName();
                direccionLatLng = place.getLatLng();
            }

            @Override
            public void onError(@NonNull Status status) {
                Log.i("TAG", "An error occurred: " + status);
            }
        });
    }

    private void onCameraMove() {
        mCameraListener = new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                try {
                    Geocoder geocoder = new Geocoder(mapa.this);
                    direccionLatLng = mMap.getCameraPosition().target;
                    List<Address> addressList = geocoder.getFromLocation(direccionLatLng.latitude, direccionLatLng.longitude, 1);
                    ciudad = addressList.get(0).getLocality();
                    pais = addressList.get(0).getCountryName();
                    direccion = addressList.get(0).getAddressLine(0);
                    barrio = addressList.get(0).getSubLocality();
                    //direccion = dir + " " + ciudad;
                    mAutocomplete.setText(direccion);
                    latitud = direccionLatLng.latitude;
                    longitud = direccionLatLng.longitude;
                } catch (Exception e) {
                    Log.d("Mensaje de Error", "Error: " + e.getMessage());
                }
            }
        };
    }

    private void LimitarDireccion() {
        //Aqui se limita la distancia de busqueda
        LatLng NorteSize = SphericalUtil.computeOffset(mCurrentLatLng, 5000, 0);
        LatLng SurSize = SphericalUtil.computeOffset(mCurrentLatLng, 5000, 180);
        mAutocomplete.setCountry("PE");
        mAutocomplete.setLocationBias(RectangularBounds.newInstance(SurSize, NorteSize));

    }

    private void actualizarLocalizacion() {
        //Toast.makeText(mapa.this, mFirebaseAuth.getCurrentUser().getUid(), Toast.LENGTH_SHORT).show();
        if (mFirebaseAuth.getCurrentUser() != null && mCurrentLatLng != null) {
            mGeofireProvider.guadarLocalizacion(mFirebaseAuth.getCurrentUser().getUid(), mCurrentLatLng);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setOnCameraIdleListener(mCameraListener);


        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(5);
        startLocation();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == LOCATION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    if (gpsActivado()){
                        mFusedLocation.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                        mMap.setMyLocationEnabled(true);
                    }else {
                        showAlertDialogNoGps();
                    }

                } else {
                    checkLocationPermissions();
                }
            } else {
                checkLocationPermissions();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SETTINGS_REQUEST_CODE && gpsActivado()) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mFusedLocation.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
            mMap.setMyLocationEnabled(true);
        }else if(requestCode == SETTINGS_REQUEST_CODE && !gpsActivado()) {
            showAlertDialogNoGps();
        }
    }

    private void showAlertDialogNoGps(){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Por favor active su ubicación, para continuar").setPositiveButton("CONFIGURACIONES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), SETTINGS_REQUEST_CODE);
            }
        }).create().show();
    }

    private boolean gpsActivado(){
        boolean activado = false;

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            activado = true;
        }
        return activado;
    }

    private void startLocation(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){ //Validamos la versión de android
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                if (gpsActivado()){
                   // btnConectar.setText("DESCONECTARSE");
                    isConnectar = true;
                    mFusedLocation.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                    mMap.setMyLocationEnabled(true);
                }else {
                    showAlertDialogNoGps();
                }
            }else {
                checkLocationPermissions();
            }
        }else {
            if (gpsActivado()){
                mFusedLocation.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                mMap.setMyLocationEnabled(false);
            }else {
                showAlertDialogNoGps();
            }

        }
    }

    private void checkLocationPermissions(){
        if (ContextCompat.checkSelfPermission(mapa.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            if(ActivityCompat.shouldShowRequestPermissionRationale(mapa.this, Manifest.permission.ACCESS_FINE_LOCATION)){
                new AlertDialog.Builder(mapa.this).setTitle("Proporcina los permisos para continuar").setMessage("Esta aplicación requiere de los permisos de ubicación para poder utilizarse").setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ActivityCompat.requestPermissions(mapa.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
                    }
                }).create().show();
            }else {
                ActivityCompat.requestPermissions(mapa.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
            if (item.getItemId() == R.id.action_cerrar){
                //logout();
            }
        return super.onOptionsItemSelected(item);
    }

    void generateToken(){
        mTokenProvide.create(mFirebaseAuth.getCurrentUser().getUid());
    }

}