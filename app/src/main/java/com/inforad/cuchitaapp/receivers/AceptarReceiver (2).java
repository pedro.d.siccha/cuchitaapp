package com.inforad.cuchitaapp.receivers;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.inforad.cuchitaapp.provider.ClientePedidoProvider;

public class AceptarReceiver extends BroadcastReceiver {

    private ClientePedidoProvider mClientePedidoProvider;

    @Override
    public void onReceive(Context context, Intent intent) {
        String idUsuario = intent.getExtras().getString("idUsuario");
        String idProducto = intent.getExtras().getString("idProducto");
        mClientePedidoProvider = new ClientePedidoProvider();
        mClientePedidoProvider.actualizarEstado(idUsuario, idProducto, "ACEPTADO");

        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.cancel(2);
    }
}
