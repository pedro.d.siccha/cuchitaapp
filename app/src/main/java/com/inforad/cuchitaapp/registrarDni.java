package com.inforad.cuchitaapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.hbb20.CountryCodePicker;
import com.inforad.cuchitaapp.models.User;
import com.inforad.cuchitaapp.models.tipoUsuarioModel;
import com.inforad.cuchitaapp.provider.UserProvider;
import com.inforad.cuchitaapp.provider.tipoUsuarioProvider;

public class registrarDni extends AppCompatActivity {


    private TextInputEditText numCelular, dni;
    private Button btnSiguiente;
    private CountryCodePicker codigoRegion;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private String urlImagen, idUsuario, nombreNegocio, numTelefono, nombre, apellido, nickname, numDni, seleccion;
    private ProgressDialog mProgressDialog;
    private UserProvider mUserProvider;
    private tipoUsuarioProvider mTipoUserProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_registrar_dni);

        Iniciar();

        btnSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                guardarDatosUsuario(idUsuario, nombre, apellido, nickname, numTelefono, numDni, urlImagen);
            }
        });


    }

    private void Iniciar(){

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mUserProvider = new UserProvider();
        mTipoUserProvider = new tipoUsuarioProvider();
        mProgressDialog = new ProgressDialog(this);
        idUsuario = mAuth.getCurrentUser().getUid();

        numCelular = findViewById(R.id.inputNumCelular);
        dni = findViewById(R.id.inputDni);
        btnSiguiente = findViewById(R.id.btnSiguiente);
        codigoRegion = findViewById(R.id.cbRegion);


        numTelefono = "+" + codigoRegion.getFullNumber() + numCelular.getText().toString();

        obtenerDatos();

    }

    private void obtenerDatos(){
        nombre = getIntent().getStringExtra("nombre");
        apellido = getIntent().getStringExtra("apellido");
        nickname = getIntent().getStringExtra("nickname");
        numDni = getIntent().getStringExtra("dni");
        seleccion = getIntent().getStringExtra("genero");
        urlImagen = getIntent().getStringExtra("imgUsuario");
    }


    void guardarDatosUsuario(String id, String nombre, String apellido, String nickname, String numTelefono, String numDni, String imgPerfil){

        User user = new User(id, nombre, apellido, nickname, numTelefono, imgPerfil, numDni, "BASICO", "ACTIVO", "TUTORIAL", "ADMINISTRADOR");
        mUserProvider.crear(user).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

                tipoUsuarioModel tipo = new tipoUsuarioModel("ADMINISTRADOR", idUsuario);
                mTipoUserProvider.guardarTipoUsuario(tipo, idUsuario).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Intent intent = new Intent(registrarDni.this, viewDireccion.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                });
            }
        });
    }


}