package com.inforad.cuchitaapp.ofrezco;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.adapters.imagenDescripcionAdaptador;
import com.inforad.cuchitaapp.models.Negocio;
import com.inforad.cuchitaapp.models.NegocioCategoria;
import com.inforad.cuchitaapp.models.NegocioPlan;
import com.inforad.cuchitaapp.models.NegocioUsuario;
import com.inforad.cuchitaapp.models.imgDescNegocio;
import com.inforad.cuchitaapp.provider.UserProvider;
import com.inforad.cuchitaapp.provider.negocioProvider;
import com.inforad.cuchitaapp.utilidades.CompresorBitmapImage;
import com.inforad.cuchitaapp.utilidades.FileUtilidades;

import java.io.File;

public class descripcionNegocio extends AppCompatActivity {

    private Button btnContinuar;
    private EditText txtDescripcion;
    private String categoria, urlLogo, nombreNegocio, idUsuario, plan, ruc, iconoCat;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabaseNegocio, mDatabasePlan, mDatabaseCategoria, mDataBaseNegocioUsuario, mDatabaseImagenNegocio;
    private ProgressDialog mProgressDialog;
    private DatabaseReference mDatabase;
    private LinearLayout ivMostrar;
    private final int GALLERY_REQUEST = 1;
    private File mImageFile;
    private TextView lblNickName;
    private RecyclerView recyclerViewImagenes;
    private imagenDescripcionAdaptador adaptadorImagen;
    private ImageView imgFoto;
    private negocioProvider mNegocioProvider;
    private UserProvider mUsuarioProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_descripcion_negocio);

        Iniciar();

        btnContinuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mProgressDialog.setMessage("Generando Negocio...");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.show();
                generarNegocio(plan, txtDescripcion.getText().toString(), categoria, urlLogo, nombreNegocio, idUsuario);
                //guardarImagen();
            }
        });

        ivMostrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mostrarGaleria();
            }
        });
    }

    private void generarNegocio(String plan, String descripcion, String categoria, String urlLogo, String nombreNegocio, String idUsuario) {

        Negocio negocio = new Negocio(ruc, nombreNegocio, descripcion, urlLogo, categoria, plan, idUsuario, "", "");
        mNegocioProvider.crearNegocio(negocio, idUsuario, ruc).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                mDatabaseNegocio.child(ruc).setValue(negocio).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        regNegocioUsuario(idUsuario, "ADMINISTRADOR");
                    }
                });
            }
        });
    }

    private void regNegocioUsuario(String idUsuario, String tipoUsuario){

        NegocioUsuario negocio = new NegocioUsuario(ruc, tipoUsuario, idUsuario);

        mDataBaseNegocioUsuario.child(idUsuario).child(ruc).setValue(negocio).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                regNegocioPlan("31/10/2020", "30/11/2020");
            }
        });
    }

    private void regNegocioPlan(String fechaInicio, String fechaPago){

        NegocioPlan negocio = new NegocioPlan();
        negocio.setFecInicio(fechaInicio);
        negocio.setFecPago(fechaPago);
        negocio.setEstado("PAGADO");

        mDatabasePlan.child(plan).child(ruc).setValue(negocio).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                regNegocioCategoria(categoria, iconoCat);
            }
        });
    }

    private void regNegocioCategoria(String categoria, String icono){

        NegocioCategoria negocio = new NegocioCategoria();
        negocio.setIco(icono);

        mDatabaseCategoria.child(categoria).child(ruc).setValue(negocio).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                mProgressDialog.dismiss();
                Intent intent = new Intent(descripcionNegocio.this, perfilNegocio.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
    }

    private void regImgDescNegocio(String idNegocio, String nomNegocio, String url, String num){

        imgDescNegocio imgNegocio = new imgDescNegocio();
        imgNegocio.setId(idNegocio);
        imgNegocio.setNombreImagen(nomNegocio);
        imgNegocio.setUrl(url);
        imgNegocio.setOrdenImg(num);

        String nombre = nomNegocio;
        nombre = nombre.replaceAll("[^A-Za-z0-9]","");

        mDatabaseImagenNegocio.child(idNegocio).child(nombre).setValue(imgNegocio).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(descripcionNegocio.this, "Se guardo bien", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void Iniciar(){
        btnContinuar = findViewById(R.id.btnContinuarDN);
        txtDescripcion = findViewById(R.id.txtDescripcionNegocio);
        lblNickName = findViewById(R.id.lblNickNameEmpresa);
        imgFoto = findViewById(R.id.ivFoto);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabaseNegocio = FirebaseDatabase.getInstance().getReference().child("Negocio_Activo");
        mDatabasePlan = FirebaseDatabase.getInstance().getReference().child("Negocio_Plan");
        mDatabaseCategoria = FirebaseDatabase.getInstance().getReference().child("Negocio_Categoria");
        mDataBaseNegocioUsuario = FirebaseDatabase.getInstance().getReference().child("Negocio_Usuario");
        mDatabaseImagenNegocio = FirebaseDatabase.getInstance().getReference().child("Negocio_Imagen_Descripcion");
        mUsuarioProvider = new UserProvider();

        mNegocioProvider = new negocioProvider();

        mProgressDialog = new ProgressDialog(this);
        ivMostrar = findViewById(R.id.btnMostrarFoto);
        obtenerDatosActivityAnterior();
        verificarFotos();

        lblNickName.setText(nombreNegocio);
        recyclerViewImagenes = findViewById(R.id.recyclerImagenesDescripcion);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerViewImagenes.setLayoutManager(linearLayoutManager);
    }

    private void obtenerDatosActivityAnterior(){
        categoria = getIntent().getStringExtra("categoria");
        urlLogo = getIntent().getStringExtra("urlLogo");
        nombreNegocio = getIntent().getStringExtra("nombreNegocio");
        idUsuario = getIntent().getStringExtra("idUsuario");
        plan = getIntent().getStringExtra("plan");
        ruc = getIntent().getStringExtra("ruc");
        iconoCat = getIntent().getStringExtra("iconoCat");
    }

    private void mostrarGaleria(){
        Intent GaleriaIntent = new Intent(Intent.ACTION_GET_CONTENT);
        GaleriaIntent.setType("image/*");
        startActivityForResult(GaleriaIntent, GALLERY_REQUEST);
        //guardarImagen();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == GALLERY_REQUEST && resultCode == RESULT_OK){
            try {
                mImageFile = FileUtilidades.from(this, data.getData());
                imgFoto.setImageBitmap(BitmapFactory.decodeFile(mImageFile.getAbsolutePath()));
                guardarImagen();
                mProgressDialog.setMessage("Cargando Imagen...");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.show();
            }catch (Exception e){
                Log.d("Error:", "Mensaje de error: " + e.getMessage());
            }
        }

    }

    private void guardarImagen(){
        byte[] imageByte = CompresorBitmapImage.getImage(this, mImageFile.getPath(), 500, 500);
        StorageReference storage = FirebaseStorage.getInstance().getReference().child("imgDetalleNegocio").child(ruc).child(mImageFile.getName());
        UploadTask uploadTask = storage.putBytes(imageByte);
        uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()){
                    storage.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            String img = uri.toString();
                            //String idUsuario = mAuth.getCurrentUser().getUid();
                            regImgDescNegocio(ruc, mImageFile.getName(), img, "");
                            mProgressDialog.dismiss();
                            //verificarFotos();
                        }
                    });
                }else {
                    Toast.makeText(descripcionNegocio.this, "Hubo un error al subir la imagen", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void verificarFotos(){
        mDatabase.child("Negocio_Imagen_Descripcion").child(ruc).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    Long contador = snapshot.getChildrenCount();
                    mUsuarioProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            if (snapshot.exists()){
                                String plan = snapshot.child("plan").getValue().toString();
                                Toast.makeText(descripcionNegocio.this, plan + " " +contador.intValue(), Toast.LENGTH_SHORT).show();

                                switch (plan){
                                    case "BASICO":
                                        if (contador.intValue() > 1){
                                            ivMostrar.setVisibility(View.GONE);
                                            Toast.makeText(descripcionNegocio.this, "Para ingresar más fotos, por favor actualice su plan", Toast.LENGTH_SHORT).show();
                                        }else {
                                            ivMostrar.setVisibility(View.VISIBLE);
                                        }
                                        break;
                                    case "ESTANDAR":
                                        if (contador.intValue() > 3){
                                            ivMostrar.setVisibility(View.GONE);
                                            Toast.makeText(descripcionNegocio.this, "Para ingresar más fotos, por favor actualice su plan", Toast.LENGTH_SHORT).show();
                                        }else {
                                            ivMostrar.setVisibility(View.VISIBLE);
                                        }
                                        break;
                                    case "PREMIUM":
                                        if (contador.intValue() > 7){
                                            ivMostrar.setVisibility(View.GONE);
                                            Toast.makeText(descripcionNegocio.this, "Límite máximo", Toast.LENGTH_SHORT).show();
                                        }else {
                                            ivMostrar.setVisibility(View.VISIBLE);
                                        }
                                        break;
                                }

                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        verificarFotos();
        Query query = mDatabase.child("Negocio_Imagen_Descripcion").child(ruc);
        FirebaseRecyclerOptions<imgDescNegocio> options = new FirebaseRecyclerOptions.Builder<imgDescNegocio>().setQuery(query, imgDescNegocio.class).build();
        adaptadorImagen = new imagenDescripcionAdaptador(options, descripcionNegocio.this);
        recyclerViewImagenes.setAdapter(adaptadorImagen);
        adaptadorImagen.startListening();
    }
}