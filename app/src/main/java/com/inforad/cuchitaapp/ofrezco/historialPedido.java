package com.inforad.cuchitaapp.ofrezco;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.adapters.pedidoPendienteNegocioAdaptador;
import com.inforad.cuchitaapp.models.PedidoFinal;

public class historialPedido extends AppCompatActivity {

    private RecyclerView recyclerListaHistorialPedido;
    private DatabaseReference mDatabase;
    private pedidoPendienteNegocioAdaptador adaptadorPedidoPendiente;
    private FirebaseAuth mAuth;
    private String usuario_id, idNegocio;
    private BottomNavigationView btnNavegacion;
    private ImageView btnAtras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_historial_pedido);
        Iniciar();

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(historialPedido.this, miTienda.class);
                intent.putExtra("negocio_id", idNegocio);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == event.KEYCODE_BACK){
            Intent intent = new Intent(historialPedido.this, miTienda.class);
            intent.putExtra("negocio_id", idNegocio);
            startActivity(intent);
        }
        return super.onKeyUp(keyCode, event);
    }

    private void Iniciar() {
        btnAtras = findViewById(R.id.ivAtras);
        recyclerListaHistorialPedido = findViewById(R.id.recyclerListaHistorialPedido);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerListaHistorialPedido.setLayoutManager(linearLayoutManager);

        mAuth = FirebaseAuth.getInstance();
        usuario_id = mAuth.getCurrentUser().getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        btnNavegacion = findViewById(R.id.bnvNavegacion);
        btnNavegacion.setSelectedItemId(R.id.historialpedido);

        btnNavegacion.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.pedidopendiente:
                        Intent intent2 = new Intent(getApplicationContext(), pedidoNegocio.class);
                        intent2.putExtra("idNegocio", idNegocio);
                        startActivity(intent2);
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.pedidoaceptado:
                        Intent intent = new Intent(getApplicationContext(), pedidoAceptado.class);
                        intent.putExtra("idNegocio", idNegocio);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.pedidoenviado:
                        Intent intent1 = new Intent(getApplicationContext(), pedidoEnviado.class);
                        intent1.putExtra("idNegocio", idNegocio);
                        startActivity(intent1);
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.historialpedido:
                        return true;
                }
                return false;
            }
        });

        obtenerDatos();
    }

    private void obtenerDatos() {
        idNegocio = getIntent().getStringExtra("idNegocio");
    }

    @Override
    protected void onStart() {
        super.onStart();

        Query queryPendiente = mDatabase.child("Pedido").child(idNegocio).orderByChild("estado").equalTo("FINAL");
        FirebaseRecyclerOptions<PedidoFinal> options = new FirebaseRecyclerOptions.Builder<PedidoFinal>().setQuery(queryPendiente, PedidoFinal.class).build();
        adaptadorPedidoPendiente = new pedidoPendienteNegocioAdaptador(options, historialPedido.this);
        recyclerListaHistorialPedido.setAdapter(adaptadorPedidoPendiente);
        adaptadorPedidoPendiente.startListening();
    }
}