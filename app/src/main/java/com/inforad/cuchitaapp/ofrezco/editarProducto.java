package com.inforad.cuchitaapp.ofrezco;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.provider.negocioInventarioProvider;
import com.inforad.cuchitaapp.utilidades.CompresorBitmapImage;
import com.inforad.cuchitaapp.utilidades.FileUtilidades;
import com.squareup.picasso.Picasso;

import java.io.File;


public class editarProducto extends AppCompatActivity {

    private ImageView imgPerfilProducto;
    private EditText  txtNombre, txtStock, txtStockMaximo, txtStockMinimo, txtUnidMedida, txtPrecio;
    private String producto_id, estado, nombre, stock, stockMaximo, stockMinimo, unidadMedida, precio, imagen, idNegocio;
    private Button btnEliminar, btnBaja, btnGuardar, btnCancelar;
    private negocioInventarioProvider mInventarioProvider ;
    private final int GALLERY_REQUEST = 1;
    private File mImageFile;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_editar_producto);
        Iniciar();

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mProgressDialog.setMessage("Cargando Imagen...");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.show();
                mInventarioProvider.actualizar(idNegocio, producto_id, txtNombre.getText().toString(), txtPrecio.getText().toString(), txtStock.getText().toString(), txtStockMaximo.getText().toString(), txtStockMinimo.getText().toString(), txtUnidMedida.getText().toString());
                //guardarImagen(txtNombre.getText().toString(),idNegocio);
                mProgressDialog.dismiss();
                Intent intent = new Intent(editarProducto.this, inventario.class);
                startActivity(intent);
            }
        });

        imgPerfilProducto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                abrirGaleria();
            }
        });
    }

    private void Iniciar(){

        imgPerfilProducto = findViewById(R.id.imgEditProducto);
        txtNombre = findViewById(R.id.inputEditNombre);
        txtStock = findViewById(R.id.inputEditStock);
        txtStockMaximo = findViewById(R.id.inputEditSotckMaximo);
        txtStockMinimo = findViewById(R.id.inputEditStockMinimo);
        txtUnidMedida = findViewById(R.id.inputEditUnidadMedida);
        txtPrecio = findViewById(R.id.inputEditPrecio);
        btnEliminar = findViewById(R.id.eliminarProducto);
        btnBaja = findViewById(R.id.darBajaProducto);
        btnGuardar = findViewById(R.id.btnEditGuardar);
        btnCancelar = findViewById(R.id.btnEditCancelar);
        mInventarioProvider = new negocioInventarioProvider();
        //mDatabase = FirebaseDatabase.getInstance().getReference();
        mProgressDialog = new ProgressDialog(this);
        obtenerDatosAnteriores();

    }

    private void obtenerDatosAnteriores(){
        producto_id = getIntent().getStringExtra("producto_id");
        nombre = getIntent().getStringExtra("nombre");
        stock = getIntent().getStringExtra("stock");
        stockMaximo = getIntent().getStringExtra("stockMaximo");
        stockMinimo = getIntent().getStringExtra("stockMinimo");
        unidadMedida = getIntent().getStringExtra("unidadMedida");
        precio = getIntent().getStringExtra("precio");
        imagen = getIntent().getStringExtra("imagen");
        estado = getIntent().getStringExtra("estado");
        idNegocio = getIntent().getStringExtra("idNegocio");
        asignarDatos();
    }

    private void asignarDatos(){
        txtNombre.setText(nombre);
        txtStock.setText(stock);
        txtStockMinimo.setText(stockMinimo);
        txtStockMaximo.setText(stockMaximo);
        txtUnidMedida.setText(unidadMedida);
        txtPrecio.setText(precio);
        Picasso.with(editarProducto.this).load(imagen).into(imgPerfilProducto);
    }

    private void abrirGaleria(){
        Intent GaleriaIntent = new Intent(Intent.ACTION_GET_CONTENT);
        GaleriaIntent.setType("image/*");
        startActivityForResult(GaleriaIntent, GALLERY_REQUEST);
    }

    private void guardarImagen(String nombre, String ruc){
        byte[] imageByte = CompresorBitmapImage.getImage(this, mImageFile.getPath(), 500, 500);
        StorageReference storage = FirebaseStorage.getInstance().getReference().child("imgProducto").child(ruc).child(nombre + ".jpg");
        UploadTask uploadTask = storage.putBytes(imageByte);
        uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()){
                    storage.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            mProgressDialog.dismiss();

                            //mInventarioProvider.actualizar(ruc, producto_id, uri.toString(), txtNombre.getText().toString(), txtPrecio.getText().toString(), txtStock.getText().toString(), txtStockMaximo.getText().toString(), txtStockMinimo.getText().toString(), txtUnidMedida.getText().toString());
                            Intent intent = new Intent(editarProducto.this, inventario.class);
                            startActivity(intent);
                        }
                    });
                }else {
                    Toast.makeText(editarProducto.this, "Hubo un error al subir la imagen", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == GALLERY_REQUEST && resultCode == RESULT_OK){
            try {
                mImageFile = FileUtilidades.from(this, data.getData());
                imgPerfilProducto.setImageBitmap(BitmapFactory.decodeFile(mImageFile.getAbsolutePath()));
            }catch (Exception e){
                Log.d("Error:", "Mensaje de error: " + e.getMessage());
            }
        }

    }

}