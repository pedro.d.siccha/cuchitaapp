package com.inforad.cuchitaapp.ofrezco;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.hbb20.CountryCodePicker;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.User;
import com.inforad.cuchitaapp.models.tipoUsuarioModel;
import com.inforad.cuchitaapp.provider.UserProvider;
import com.inforad.cuchitaapp.provider.tipoUsuarioProvider;
import com.inforad.cuchitaapp.utilidades.CompresorBitmapImage;
import com.inforad.cuchitaapp.utilidades.FileUtilidades;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import de.hdodenhof.circleimageview.CircleImageView;

public class nuevoEmpleado extends AppCompatActivity {

    private CircleImageView imgPerfil;
    private TextInputEditText inputNombre, inputApellido, inputAlias, inputDni, inputCelular, inputCorreo;
    private RadioButton rbtnVaron, rbtnMujer;
    private String seleccion, urlImagen, idNegocio;
    private CountryCodePicker codigoRegion;
    private DatabaseReference mDatabase;
    private File mImageFile;
    private ProgressDialog mProgressDialog;
    private tipoUsuarioProvider mTipoUserProvider;
    private Spinner cbTipoUsuario;
    private final int GALLERY_REQUEST = 1;
    private UserProvider mUserProvider;
    private FirebaseAuth mAuth;
    private Button btnGuardar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_nuevo_empleado);
        Iniciar();

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                guardarUsuario();
            }
        });

    }

    private void Iniciar() {
        cbTipoUsuario = findViewById(R.id.spTipoUsuario);
        String[] ubicacion = {"SELECCIONAR TIPO USUARIO... ", "ADMINISTRADOR", "MOZO", "REPARTIDOR", "OTRO"};
        ArrayList<String> arrayList = new ArrayList<>(Arrays.asList(ubicacion));
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, R.layout.style_spinner, arrayList);
        cbTipoUsuario.setAdapter(arrayAdapter);

        imgPerfil = findViewById(R.id.civPerfil);
        inputNombre = findViewById(R.id.etNombre);
        inputApellido = findViewById(R.id.etApellido);
        inputAlias = findViewById(R.id.etAlias);
        inputDni = findViewById(R.id.etDni);
        inputCelular = findViewById(R.id.etTelefono);
        inputCorreo = findViewById(R.id.etCorreo);
        rbtnVaron = findViewById(R.id.rbVaron);
        rbtnMujer = findViewById(R.id.rbMujer);
        codigoRegion = findViewById(R.id.cbRegion);
        btnGuardar = findViewById(R.id.bGuardar);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mProgressDialog = new ProgressDialog(this);
        mUserProvider = new UserProvider();
        mAuth = FirebaseAuth.getInstance();

        cargarDatos();
    }

    private void cargarDatos() {
        idNegocio = getIntent().getStringExtra("idNegocio");
    }

    private void abrirGaleria(){
        Intent GaleriaIntent = new Intent(Intent.ACTION_GET_CONTENT);
        GaleriaIntent.setType("image/*");
        startActivityForResult(GaleriaIntent, GALLERY_REQUEST);
    }

    private void guardarUsuario(){

        mAuth.createUserWithEmailAndPassword(inputCorreo.getText().toString(), inputDni.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                    String idNuevoUsuario = user.getUid();
                    guardarImagen(idNuevoUsuario, inputNombre.getText().toString(), inputApellido.getText().toString(), inputAlias.getText().toString(), inputCelular.getText().toString(), inputDni.getText().toString());
                }else{
                    Toast.makeText(nuevoEmpleado.this, "Hubo un error en el registro del empleado", Toast.LENGTH_SHORT).show();
                }
            }
        });
        
    }

    private void guardarImagen(String id, String nombre, String apellido, String nickname, String numTelefono, String numDni){
        byte[] imageByte = CompresorBitmapImage.getImage(this, mImageFile.getPath(), 500, 500);
        StorageReference storage = FirebaseStorage.getInstance().getReference().child("imgUsuario").child(id + ".jpg");
        UploadTask uploadTask = storage.putBytes(imageByte);
        uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()){
                    storage.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            String img = uri.toString();
                            mProgressDialog.dismiss();
                            guardarDatosUsuario(id, nombre, apellido, nickname, numTelefono, numDni, img);
                        }
                    });
                }else {
                    Toast.makeText(nuevoEmpleado.this, "Hubo un error al subir la imagen", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == GALLERY_REQUEST && resultCode == RESULT_OK){
            try {
                mImageFile = FileUtilidades.from(this, data.getData());
                imgPerfil.setImageBitmap(BitmapFactory.decodeFile(mImageFile.getAbsolutePath()));
            }catch (Exception e){
                Log.d("Error:", "Mensaje de error: " + e.getMessage());
            }
        }

    }

    void guardarDatosUsuario(String id, String nombre, String apellido, String nickname, String numTelefono, String numDni, String imgPerfil){

        User user = new User(id, nombre, apellido, nickname, numTelefono, imgPerfil, numDni, "BASICO", "ACTIVO", "TUTORIAL", "ADMINISTRADOR");
        mUserProvider.crear(user).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                tipoUsuarioModel tipo = new tipoUsuarioModel(cbTipoUsuario.getSelectedItem().toString(), id);
                mTipoUserProvider.guardarTipoUsuario(tipo, id).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        FirebaseAuth.getInstance().signOut();
                    }
                });
            }
        });
    }

}