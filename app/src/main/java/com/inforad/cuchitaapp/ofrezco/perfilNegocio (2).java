package com.inforad.cuchitaapp.ofrezco;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.adapters.listaTiendaAdaptador;
import com.inforad.cuchitaapp.models.NegocioUsuario;
import com.inforad.cuchitaapp.provider.NotificationProvider;
import com.inforad.cuchitaapp.provider.TokenProvider;
import com.inforad.cuchitaapp.provider.UserProvider;
import com.inforad.cuchitaapp.provider.negocioProvider;

public class perfilNegocio extends AppCompatActivity {

    private LinearLayout btnAnuncios, btnTienda, btnGanancias, btnEstadisticas, btnCostos, btnAjustes;
    private ImageView imgCategoria, btnAgregar;
    private TextView txtTienda, txtCategoria;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private DatabaseReference mDatabase;
    private String tipoUsuario, user_id, nombre, imagen, descripcion, idNegocio;
    private FirebaseUser user;
    private negocioProvider mNegocioProvider;
    private TokenProvider mTokenProvide;
    private NotificationProvider mNotificationProvider;
    private RecyclerView recyclerTienda;
    private listaTiendaAdaptador tiendaAdaptador;
    private UserProvider mUserProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_perfil_negocio);
        Inicar();

        mNegocioProvider = new negocioProvider();

        mTokenProvide = new TokenProvider();
        generateToken();

        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(perfilNegocio.this, nombreEmpresa.class);
                intent.putExtra("plan", "BASICO");
                startActivity(intent);
            }
        });

        btnAnuncios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(perfilNegocio.this, anunciosTienda.class);
                intent.putExtra("idNegocio", idNegocio);
                startActivity(intent);
            }
        });


/*
        btnTienda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(perfilNegocio.this, miTienda.class);
                intent.putExtra("negocio_id", idNegocio);
                intent.putExtra("usuario_id", user_id);
                startActivity(intent);
            }
        });
*/
        btnGanancias.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(perfilNegocio.this, gananciaTienda.class);
                /*intent.putExtra("negocio_id", idNegocio);
                intent.putExtra("usuario_id", user_id);

                 */
                startActivity(intent);
            }
        });

        btnEstadisticas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(perfilNegocio.this, estadisticasGenerales.class);
                /*intent.putExtra("negocio_id", idNegocio);
                intent.putExtra("usuario_id", user_id);

                 */
                startActivity(intent);
            }
        });

        btnCostos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(perfilNegocio.this, costosOperaciones.class);
                /*intent.putExtra("negocio_id", idNegocio);
                intent.putExtra("usuario_id", user_id);

                 */
                startActivity(intent);
            }
        });

        btnAjustes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(perfilNegocio.this, AjustesGenerales.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == event.KEYCODE_BACK){
            Intent intent = new Intent(perfilNegocio.this, com.inforad.cuchitaapp.tipoUsuario.class);
            startActivity(intent);
        }
        return super.onKeyDown(keyCode, event);
    }

    private void obtenerDatosNegocio(String idNegocio) {
        mNegocioProvider.getNegocio(idNegocio).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String nombEmpresa = snapshot.child("nombre").getValue().toString();
//                    txtTienda.setText(nombEmpresa);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void Inicar(){
        btnAnuncios = findViewById(R.id.llAnuncios);

        btnGanancias = findViewById(R.id.llGanancias);
        btnEstadisticas = findViewById(R.id.llEstadistica);
        btnCostos = findViewById(R.id.llCosto);
        btnAjustes = findViewById(R.id.llAjustes);
        imgCategoria = findViewById(R.id.imgCategoriaTienda);
        txtTienda = findViewById(R.id.tvNombreTienda);
        txtCategoria = findViewById(R.id.tvCategoria);
        btnAgregar = findViewById(R.id.btnAgregarTienda);
        mFirebaseAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        user_id = mFirebaseAuth.getCurrentUser().getUid();
        mNotificationProvider = new NotificationProvider();
        this.idNegocio = "";
        mUserProvider = new UserProvider();

        recyclerTienda = findViewById(R.id.recyclerListaTienda);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerTienda.setLayoutManager(linearLayoutManager);

        obtenerIdNegocio();
        verificar();
    }

    private void obtenerIdNegocio(){

        mDatabase.child("Negocio_Usuario").child(user_id).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                NegocioUsuario negocio = snapshot.getValue(NegocioUsuario.class);
                idNegocio = snapshot.getKey();
                obtenerDatosNegocio(idNegocio);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    void generateToken(){
        mTokenProvide.create(mFirebaseAuth.getCurrentUser().getUid());
    }

    private void verificar(){
        mDatabase.child("Negocio_Usuario").child(user_id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    Long contador = snapshot.getChildrenCount();
                    mUserProvider.obtenerUsuario(user_id).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            if (snapshot.exists()){
                                String plan = snapshot.child("plan").getValue().toString();
                                switch (plan){
                                    case "BASICO":
                                        btnAgregar.setVisibility(View.GONE);
                                        break;
                                    case "ESTANDAR":
                                        if (contador.intValue() > 2){
                                            btnAgregar.setVisibility(View.GONE);
                                            Toast.makeText(perfilNegocio.this, "Para ingresar más fotos, por favor actualice su plan", Toast.LENGTH_SHORT).show();
                                        }else {
                                            btnAgregar.setVisibility(View.VISIBLE);
                                        }
                                        break;
                                    case "PREMIUM":
                                        btnAgregar.setVisibility(View.VISIBLE);
                                        break;
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        verificar();
        obtenerIdNegocio();
    }

    @Override
    protected void onStart() {
        super.onStart();

        verificar();
        obtenerIdNegocio();

        Query query = mDatabase.child("Negocio_Usuario").child(user_id);
        FirebaseRecyclerOptions<NegocioUsuario> options = new FirebaseRecyclerOptions.Builder<NegocioUsuario>().setQuery(query, NegocioUsuario.class).build();
        tiendaAdaptador = new listaTiendaAdaptador(options, perfilNegocio.this);
        recyclerTienda.setAdapter(tiendaAdaptador);
        tiendaAdaptador.startListening();

    }

}