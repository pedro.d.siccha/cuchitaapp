package com.inforad.cuchitaapp.ofrezco;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.adapters.listaProductoPedidoAdaptador;
import com.inforad.cuchitaapp.models.Producto;

public class agregarProductoVenta extends AppCompatActivity {

    private ImageView btnAtras;
    private RecyclerView rvListaProductos;
    private Button btnAceptar;
    private String idNegocio, dni, nombre;
    private DatabaseReference mDatabase;
    private listaProductoPedidoAdaptador adaptadorProducto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_agregar_producto_venta);
        Iniciar();

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(agregarProductoVenta.this, ventasTienda.class);
                intent.putExtra("idNegocio", idNegocio);
                intent.putExtra("nombre", nombre);
                intent.putExtra("dni", dni);
                startActivity(intent);
            }
        });

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(agregarProductoVenta.this, ventasTienda.class);
                intent.putExtra("idNegocio", idNegocio);
                intent.putExtra("nombre", nombre);
                intent.putExtra("dni", dni);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == event.KEYCODE_BACK){
            Intent intent = new Intent(agregarProductoVenta.this, ventasTienda.class);
            intent.putExtra("idNegocio", idNegocio);
            intent.putExtra("nombre", nombre);
            intent.putExtra("dni", dni);
            startActivity(intent);
        }
        return super.onKeyUp(keyCode, event);
    }

    private void Iniciar() {
        btnAtras = findViewById(R.id.ivAtras);
        btnAceptar = findViewById(R.id.bAceptar);
        rvListaProductos = findViewById(R.id.rvProductos);
        LinearLayoutManager linearLayoutManagerProductos = new LinearLayoutManager(this);
        rvListaProductos.setLayoutManager(linearLayoutManagerProductos);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        obtenerDatos();
    }

    private void obtenerDatos() {
        idNegocio = getIntent().getStringExtra("idNegocio");
        nombre = getIntent().getStringExtra("nombre");
        dni = getIntent().getStringExtra("dni");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Query queryProducto = mDatabase.child("Producto").orderByChild("idTienda").equalTo(idNegocio);
        FirebaseRecyclerOptions<Producto> options = new FirebaseRecyclerOptions.Builder<Producto>().setQuery(queryProducto, Producto.class).build();
        adaptadorProducto = new listaProductoPedidoAdaptador(options, agregarProductoVenta.this, dni, nombre);
        rvListaProductos.setAdapter(adaptadorProducto);
        adaptadorProducto.startListening();

    }
}