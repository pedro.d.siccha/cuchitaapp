package com.inforad.cuchitaapp.ofrezco;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.adapters.adaptadorUsuario;
import com.inforad.cuchitaapp.models.PedidoFinal;
import com.inforad.cuchitaapp.provider.PedidoProvider;
import com.inforad.cuchitaapp.provider.UserProvider;

public class generarPedido extends AppCompatActivity {

    private EditText txtDni, txtNombre;
    private FirebaseAuth mAuth;
    private UserProvider mUserProvider;
    private String idNegocio, idUsuario;
    private DatabaseReference mDatabase;
    private Button btnSiguiente;
    private adaptadorUsuario usuarioAdaptador;
    private ImageView btnAtras;
    private PedidoProvider mPedidoProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_generar_pedido);
        Iniciar();

        btnSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                generarPedido();
            }
        });

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(generarPedido.this, miTienda.class);
                intent.putExtra("negocio_id", idNegocio);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == event.KEYCODE_BACK){
            Intent intent = new Intent(generarPedido.this, miTienda.class);
            intent.putExtra("negocio_id", idNegocio);
            startActivity(intent);
        }
        return super.onKeyUp(keyCode, event);
    }

    private void Iniciar() {
        btnAtras = findViewById(R.id.ivAtras);
        btnSiguiente = findViewById(R.id.bNuevo);
        txtDni = findViewById(R.id.etDni);
        txtNombre = findViewById(R.id.etNombre);

        mUserProvider = new UserProvider();
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mPedidoProvider = new PedidoProvider();

        idUsuario = mAuth.getCurrentUser().getUid();

        obtenerDatos();

    }

    private void obtenerDatos() {
        idNegocio = getIntent().getStringExtra("idNegocio");
    }

    private void generarPedido(){
        PedidoFinal pedido = new PedidoFinal(txtNombre.getText().toString(), idUsuario, txtDni.getText().toString(), idNegocio, idUsuario, 0.00, "15", "PENDIENTE");
        mPedidoProvider.create(pedido, idNegocio, txtDni.getText().toString()).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Intent intent = new Intent(generarPedido.this, ventasTienda.class);
                intent.putExtra("dni", txtDni.getText().toString());
                intent.putExtra("nombre", txtNombre.getText().toString());
                intent.putExtra("idNegocio", idNegocio);
                startActivity(intent);
            }
        });
    }


}