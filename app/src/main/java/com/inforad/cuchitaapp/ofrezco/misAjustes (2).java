package com.inforad.cuchitaapp.ofrezco;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.MainActivity;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.adapters.reclamoAdaptador;
import com.inforad.cuchitaapp.adapters.tarjetaAdaptador;
import com.inforad.cuchitaapp.models.Reclamo;
import com.inforad.cuchitaapp.models.Tarjeta;
import com.inforad.cuchitaapp.necesito.ajustes;
import com.inforad.cuchitaapp.provider.negocioProvider;
import com.inforad.cuchitaapp.provider.reclamoProvider;

public class misAjustes extends AppCompatActivity {

    private TextView txtNombre;
    private EditText inputReclamo;
    private ImageView btnAgregarReclamo, btnAgregarTarjeta, btnSalir, btnAgregarEmpleado;
    private RecyclerView listaReclamo, listaTarjetas, listaEmpleado;
    private DatabaseReference mDatabase;
    private String idNegocio, idUsuario;
    private reclamoAdaptador adaptadorReclamo;
    private reclamoProvider mReclamoProvider;
    private FirebaseAuth mAuth;
    private negocioProvider mNegocioProvider;
    private tarjetaAdaptador adaptadorTarjeta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_mis_ajustes);
        Iniciar();

        btnAgregarReclamo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                guardarReclamo();
            }
        });

        txtNombre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(misAjustes.this, editarNegocio.class);
                intent.putExtra("idNegocio", idNegocio);
                startActivity(intent);
            }
        });

        btnAgregarTarjeta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(misAjustes.this, nuevaTarjeta.class);
                intent.putExtra("idNegocio", idNegocio);
                startActivity(intent);
            }
        });

        btnAgregarEmpleado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(misAjustes.this, nuevoEmpleado.class);
                intent.putExtra("idNegocio", idNegocio);
                startActivity(intent);
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
                Intent inte = new Intent(misAjustes.this, MainActivity.class);
                startActivity(inte);
                finish();
            }
        });

    }

    private void guardarReclamo() {
        Reclamo reclamo = new Reclamo(inputReclamo.getText().toString(), idUsuario, idNegocio, "PENDIENTE", "");
        mReclamoProvider.create(reclamo, idNegocio).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(misAjustes.this, "Reclamo enviado correctamente", Toast.LENGTH_SHORT).show();
                inputReclamo.setText("");
            }
        });
    }

    private void Iniciar() {
        txtNombre = findViewById(R.id.tvNombre);
        inputReclamo = findViewById(R.id.etReclamo);
        btnAgregarReclamo = findViewById(R.id.ivAgregarReclamo);
        btnAgregarTarjeta = findViewById(R.id.ivAgregarTarjeta);
        btnAgregarEmpleado = findViewById(R.id.ivAgregarEmpleado);
        btnSalir = findViewById(R.id.ivSalir);
        listaReclamo = findViewById(R.id.recyclerReclamo);
        LinearLayoutManager linearLayoutManagerReclamo = new LinearLayoutManager(this);
        listaReclamo.setLayoutManager(linearLayoutManagerReclamo);
        listaTarjetas = findViewById(R.id.recyclerTarjeta);
        LinearLayoutManager linearLayoutManagerTarjeta = new LinearLayoutManager(this);
        listaTarjetas.setLayoutManager(linearLayoutManagerTarjeta);
        listaEmpleado = findViewById(R.id.recyclerEmpleado);
        LinearLayoutManager linearLayoutManagerEmpleado = new LinearLayoutManager(this);
        listaEmpleado.setLayoutManager(linearLayoutManagerEmpleado);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mReclamoProvider = new reclamoProvider();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mNegocioProvider = new negocioProvider();

        obtenerDatos();

    }

    private void obtenerDatos() {
        idNegocio = getIntent().getStringExtra("idNegocio");

        mNegocioProvider.getNegocio(idNegocio).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    txtNombre.setText(snapshot.child("nombre").getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(misAjustes.this, "Error: " + error.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void mostrarReclamos() {
        Query queryReclamo = mDatabase.child("Reclamo").child(idNegocio);
        FirebaseRecyclerOptions<Reclamo> options = new FirebaseRecyclerOptions.Builder<Reclamo>().setQuery(queryReclamo, Reclamo.class).build();
        adaptadorReclamo = new reclamoAdaptador(options, misAjustes.this);
        listaReclamo.setAdapter(adaptadorReclamo);
        adaptadorReclamo.startListening();
    }

    private void mostrarTarjetas() {
        Query queryTarjeta = mDatabase.child("Tarjeta").child(idNegocio);
        FirebaseRecyclerOptions<Tarjeta> options = new FirebaseRecyclerOptions.Builder<Tarjeta>().setQuery(queryTarjeta, Tarjeta.class).build();
        adaptadorTarjeta = new tarjetaAdaptador(options, misAjustes.this);
        listaTarjetas.setAdapter(adaptadorTarjeta);
        adaptadorTarjeta.startListening();
    }

    private void mostrarEmpleados() {
        Query queryReclamo = mDatabase.child("Reclamo").child(idNegocio);
        FirebaseRecyclerOptions<Reclamo> options = new FirebaseRecyclerOptions.Builder<Reclamo>().setQuery(queryReclamo, Reclamo.class).build();
        adaptadorReclamo = new reclamoAdaptador(options, misAjustes.this);
        listaReclamo.setAdapter(adaptadorReclamo);
        adaptadorReclamo.startListening();

    }

    @Override
    protected void onStart() {
        super.onStart();
        mostrarReclamos();
        mostrarTarjetas();
        mostrarEmpleados();


    }




}