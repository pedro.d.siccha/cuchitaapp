package com.inforad.cuchitaapp.ofrezco;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.Anuncios;
import com.inforad.cuchitaapp.provider.AnuncioProvider;
import com.inforad.cuchitaapp.provider.UserProvider;
import com.inforad.cuchitaapp.utilidades.CompresorBitmapImage;
import com.inforad.cuchitaapp.utilidades.FileUtilidades;

import java.io.File;

public class cargarAnuncio extends AppCompatActivity {

    private ImageView imgAnuncio;
    private Button guardar;
    private EditText titulo, descripcion, feccreacion;
    private String usuario_id, tienda_id;
    private final int GALLERY_REQUEST = 1;
    private ProgressDialog mProgressDialog;
    private File mImageFile;
    private AnuncioProvider anuncioProvider;
    private UserProvider mUserProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_cargar_anuncio);
        Iniciar();

        imgAnuncio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                abrirGaleria();
            }
        });

        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mProgressDialog.setMessage("Cargar Anuncio...");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.show();
                guardarImagen(tienda_id, titulo.getText().toString(), descripcion.getText().toString(), feccreacion.getText().toString(), "", "PUBLICADO");
            }
        });
    }

    private void Iniciar(){

        imgAnuncio = findViewById(R.id.ivAnuncio);
        guardar = findViewById(R.id.btnGuardar);
        titulo = findViewById(R.id.inputTitulo);
        descripcion = findViewById(R.id.inputDescripcion);
        feccreacion = findViewById(R.id.inputFecInicio);
        obtenerDatos();
        anuncioProvider = new AnuncioProvider();
        mUserProvider = new UserProvider();
        mProgressDialog = new ProgressDialog(this);

    }

    private void obtenerDatos(){
        usuario_id = getIntent().getStringExtra("idUsuario");
        tienda_id = getIntent().getStringExtra("idTienda");

    }

    private void abrirGaleria(){
        Intent GaleriaIntent = new Intent(Intent.ACTION_GET_CONTENT);
        GaleriaIntent.setType("image/*");
        startActivityForResult(GaleriaIntent, GALLERY_REQUEST);
    }

    private void guardarImagen(String id, String titulo, String descripcion, String feccreacion, String tiempo, String estado){
        byte[] imageByte = CompresorBitmapImage.getImage(this, mImageFile.getPath(), 500, 500);
        StorageReference storage = FirebaseStorage.getInstance().getReference().child("imgAnuncios").child(id + ".jpg");
        UploadTask uploadTask = storage.putBytes(imageByte);
        uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()){
                    storage.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            String img = uri.toString();
                            mProgressDialog.dismiss();
                            Anuncios anuncios = new Anuncios(id, titulo, descripcion, feccreacion, feccreacion, tiempo, estado, img, tienda_id, usuario_id);
                            anuncioProvider.create(anuncios).addOnSuccessListener(new OnSuccessListener<Void>() {
                               @Override
                               public void onSuccess(Void aVoid) {
                                   Toast.makeText(cargarAnuncio.this, "El Anuncio se guardó correctamente", Toast.LENGTH_SHORT).show();
                                   Intent intent = new Intent(cargarAnuncio.this, anunciosTienda.class);
                                   intent.putExtra("idNegocio", tienda_id);
                                   startActivity(intent);
                               }
                           });
                        }
                    });
                }else {
                    Toast.makeText(cargarAnuncio.this, "Hubo un error al subir la imagen", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == GALLERY_REQUEST && resultCode == RESULT_OK){
            try {
                mImageFile = FileUtilidades.from(this, data.getData());
                imgAnuncio.setImageBitmap(BitmapFactory.decodeFile(mImageFile.getAbsolutePath()));
            }catch (Exception e){
                Log.d("Error:", "Mensaje de error: " + e.getMessage());
            }
        }
    }
}