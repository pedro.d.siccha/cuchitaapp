package com.inforad.cuchitaapp.ofrezco;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.adapters.anuncioTiendaAdaptador;
import com.inforad.cuchitaapp.models.Anuncios;
import com.inforad.cuchitaapp.provider.AnuncioProvider;
import com.inforad.cuchitaapp.provider.UserProvider;
import com.inforad.cuchitaapp.provider.negocioProvider;

public class anunciosTienda extends AppCompatActivity {
    private Button btnSubir;
    private RecyclerView lisaAnuncios;
    private FirebaseAuth mAuth;
    private String usuario_id, tienda_id, plan;
    private DatabaseReference mDatabase;
    private anuncioTiendaAdaptador adaptadorAnuncio;
    private AnuncioProvider mAnuncioProvider;
    private negocioProvider mNegocioProvider;
    private UserProvider mUserProvider;
    private ImageView btnAtras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_anuncios_tienda);
        Iniciar();

        btnSubir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(anunciosTienda.this, cargarAnuncio.class);
                intent.putExtra("idUsuario", usuario_id);
                intent.putExtra("idTienda", tienda_id);
                startActivity(intent);
            }
        });

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(anunciosTienda.this, perfilNegocio.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == event.KEYCODE_BACK){
            Intent intent = new Intent(anunciosTienda.this, perfilNegocio.class);
            startActivity(intent);
        }
        return super.onKeyDown(keyCode, event);
    }

    private void Iniciar(){
        btnSubir = findViewById(R.id.btnSubirAnuncios);
        lisaAnuncios = findViewById(R.id.recyclerAnuncios);
        btnAtras = findViewById(R.id.ivAtras);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        lisaAnuncios.setLayoutManager(linearLayoutManager);
        mAuth = FirebaseAuth.getInstance();
        usuario_id = mAuth.getCurrentUser().getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAnuncioProvider = new AnuncioProvider();
        mNegocioProvider = new negocioProvider();
        mUserProvider = new UserProvider();

        verificar();
    }

    private void obtenerDatos(){
        tienda_id = getIntent().getStringExtra("idNegocio");
    }

    private void verificar(){

        mAnuncioProvider.obtenerAnucios(usuario_id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    Long contador = snapshot.getChildrenCount();
                    mUserProvider.obtenerUsuario(usuario_id).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            if (snapshot.exists()){
                                String plan = snapshot.child("plan").getValue().toString();
                                switch (plan){
                                    case "BASICO":
                                        if (contador.intValue() > 1){
                                            btnSubir.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    Intent intent = new Intent(anunciosTienda.this, comprarAnuncio.class);
                                                    intent.putExtra("idUsuario", usuario_id);
                                                    intent.putExtra("idTienda", tienda_id);
                                                    startActivity(intent);
                                                }
                                            });
                                        }
                                        break;
                                    case "ESTANDAR":
                                        if (contador.intValue() > 2){
                                            btnSubir.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    Intent intent = new Intent(anunciosTienda.this, comprarAnuncio.class);
                                                    intent.putExtra("idUsuario", usuario_id);
                                                    intent.putExtra("idTienda", tienda_id);
                                                    startActivity(intent);
                                                }
                                            });
                                        }
                                        break;
                                    case "PREMIUM":
                                        if (contador.intValue() > 4){
                                            btnSubir.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    Intent intent = new Intent(anunciosTienda.this, comprarAnuncio.class);
                                                    intent.putExtra("idUsuario", usuario_id);
                                                    intent.putExtra("idTienda", tienda_id);
                                                    startActivity(intent);
                                                }
                                            });
                                        }
                                        break;
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        verificar();
        obtenerDatos();

        Query query = mDatabase.child("Anuncios").orderByChild("idUsuario").equalTo(usuario_id);
        FirebaseRecyclerOptions<Anuncios> options = new FirebaseRecyclerOptions.Builder<Anuncios>().setQuery(query, Anuncios.class).build();
        adaptadorAnuncio = new anuncioTiendaAdaptador(options, anunciosTienda.this);
        lisaAnuncios.setAdapter(adaptadorAnuncio);
        adaptadorAnuncio.startListening();
    }
}