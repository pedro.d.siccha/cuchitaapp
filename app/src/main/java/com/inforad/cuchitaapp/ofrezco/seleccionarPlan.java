package com.inforad.cuchitaapp.ofrezco;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.cuchitaapp.R;

public class seleccionarPlan extends AppCompatActivity {

    private LinearLayout btnBasico, btnEstandar, btnPremium;
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_seleccionar_plan);

        Iniciar();



        btnBasico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mostrarDetalle("BASICO");
            }
        });

        btnEstandar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mostrarDetalle("ESTANDAR");
            }
        });

        btnPremium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mostrarDetalle("PREMIUM");
            }
        });
    }

    private void Iniciar(){
        btnBasico = findViewById(R.id.btnPlanBasico);
        btnEstandar = findViewById(R.id.btnPlanEstandar);
        btnPremium = findViewById(R.id.btnPlanPremium);


        mProgressDialog = new ProgressDialog(this);
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Negocio_Activo");
    }

    private void mostrarDetalle(String plan){

        switch (plan){
            case "BASICO":{

                Intent intent = new Intent(seleccionarPlan.this, detalleBasico.class);
                startActivity(intent);
            } break;
            case "ESTANDAR":{
                Intent intent = new Intent(seleccionarPlan.this, detalleEstandar.class);
                startActivity(intent);
            } break;
            case "PREMIUM": {
                Intent intent = new Intent(seleccionarPlan.this, detallePremium.class);
                startActivity(intent);
            } break;
        }

    }



    @Override
    protected void onResume() {
        super.onResume();

    }
}