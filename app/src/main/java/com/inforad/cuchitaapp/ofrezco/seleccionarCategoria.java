package com.inforad.cuchitaapp.ofrezco;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.inforad.cuchitaapp.R;

public class seleccionarCategoria extends AppCompatActivity {

   RelativeLayout btnAnuncios, btnComida, btnMercado, btnListaCompra, btnMascota, btnPc, btnProfesional, btnServicio, btnFarmacia, btnFerreteria, btnModa, btnJuguete, btnTransporte;
    private String urlLogo, nombreNegocio, idUsuario, plan, ruc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_seleccionar_categoria);
        Iniciar();
/*
        btnAnuncios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(seleccionarCategoria.this, "ANUNCIOS", Toast.LENGTH_SHORT).show();
            }
        });

 */

        btnComida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seleccionarCategoria("Comida", urlLogo, nombreNegocio, idUsuario, "@drawable/catcomidabebida");
            }
        });

        btnMercado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seleccionarCategoria("Mercado", urlLogo, nombreNegocio, idUsuario, "@drawable/catmarkets");
            }
        });

        btnListaCompra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seleccionarCategoria("Compra", urlLogo, nombreNegocio, idUsuario, "@drawable/catlistacompra");
            }
        });

        btnMascota.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seleccionarCategoria("Mascota", urlLogo, nombreNegocio, idUsuario, "@drawable/catmascota");
            }
        });

        btnPc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seleccionarCategoria("Pc", urlLogo, nombreNegocio, idUsuario, "@drawable/catpc");
            }
        });
/*
        btnProfesional.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seleccionarCategoria("Profesional", urlLogo, nombreNegocio, idUsuario, "@drawable/catprofesional");
            }
        });

 */
/*
        btnServicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seleccionarCategoria("Servicio", urlLogo, nombreNegocio, idUsuario, "@drawable/cattecnico");
            }
        });

 */

        btnFarmacia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seleccionarCategoria("Farmacia", urlLogo, nombreNegocio, idUsuario, "@drawable/catfarmacia");
            }
        });

        btnFerreteria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seleccionarCategoria("Ferreteria", urlLogo, nombreNegocio, idUsuario, "@drawable/catferreteria");
            }
        });

        btnModa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seleccionarCategoria("Moda", urlLogo, nombreNegocio, idUsuario, "@drawable/catmoda");
            }
        });

        btnJuguete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seleccionarCategoria("Juguete", urlLogo, nombreNegocio, idUsuario, "@drawable/catjuguetes");
            }
        });

        btnTransporte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seleccionarCategoria("Transporte", urlLogo, nombreNegocio, idUsuario, "@drawable/cattransporte");
            }
        });
    }

    private void seleccionarCategoria(String categoria, String urlLogo, String nombreNegocio, String idUsuario, String iconoCat){
        Intent intent = new Intent(seleccionarCategoria.this, descripcionNegocio.class);
        intent.putExtra("categoria", categoria);
        intent.putExtra("urlLogo", urlLogo);
        intent.putExtra("nombreNegocio", nombreNegocio);
        intent.putExtra("idUsuario", idUsuario);
        intent.putExtra("plan", plan);
        intent.putExtra("ruc", ruc);
        intent.putExtra("iconoCat", iconoCat);
        startActivity(intent);
    }

    private void Iniciar(){
        //btnAnuncios = findViewById(R.id.btnAnuncio);
        btnComida = findViewById(R.id.btnCatComida);
        btnMercado = findViewById(R.id.btnCatMercado);
        btnListaCompra = findViewById(R.id.btnCatListaCompra);
        btnMascota = findViewById(R.id.btnCatMascota);
        btnPc = findViewById(R.id.btnCatPc);
       // btnProfesional = findViewById(R.id.btnCatProfesional);
        //btnServicio = findViewById(R.id.btnCatTecnico);
        btnFarmacia = findViewById(R.id.btnCatFarmacia);
        btnFerreteria = findViewById(R.id.btnCatFerreteria);
        btnModa = findViewById(R.id.btnCatModa);
        btnJuguete = findViewById(R.id.btnCatJuguete);
        btnTransporte = findViewById(R.id.btnCatTransporte);
        DatosActovityAnterior();
    }

    private void DatosActovityAnterior(){
        urlLogo = getIntent().getStringExtra("urlLogo");
        nombreNegocio = getIntent().getStringExtra("nombreNegocio");
        idUsuario = getIntent().getStringExtra("idUsuario");
        plan = getIntent().getStringExtra("plan");
        ruc = getIntent().getStringExtra("ruc");
    }

}