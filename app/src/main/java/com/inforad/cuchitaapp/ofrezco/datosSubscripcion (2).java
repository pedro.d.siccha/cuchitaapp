package com.inforad.cuchitaapp.ofrezco;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.provider.UserProvider;

import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;

public class datosSubscripcion extends AppCompatActivity {

    private static final int SCAN_RESULT = 100;
    private TextView txtTarjeta, txtFecha;
    private Button btnEscanear, btnOmitirs;
    private String precio, plan, idUsuario;
    private UserProvider mUserProvider;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_datos_subscripcion);
        init();

        btnOmitirs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mUserProvider.actualizarActividad(idUsuario, "INICIO").addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Intent intent = new Intent(datosSubscripcion.this, nombreEmpresa.class);
                        intent.putExtra("plan", plan);
                        startActivity(intent);
                    }
                });
            }
        });

        btnEscanear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verificarTarjeta();
            }
        });

    }

    private void init(){

        txtFecha = findViewById(R.id.tvFechaTarjeta);
        txtTarjeta = findViewById(R.id.tvTarjeta);
        btnEscanear = findViewById(R.id.btnVerificar);
        btnEscanear.setText("VERIFICAR");
        btnOmitirs = findViewById(R.id.btnPasar);

        mUserProvider = new UserProvider();
        mAuth = FirebaseAuth.getInstance();

        idUsuario = mAuth.getCurrentUser().getUid();

        obtenerDatosActivityAnterior();
    }

    private void obtenerDatosActivityAnterior(){
        precio = getIntent().getStringExtra("precio");
        plan = getIntent().getStringExtra("plan");
    }

    public void verificarTarjeta(){

        if (btnEscanear.getText().toString() == "VERIFICAR"){
            Intent intent = new Intent(datosSubscripcion.this, CardIOActivity.class)
                    .putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true)
                    .putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, false)
                    .putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false);
            startActivityForResult(intent, SCAN_RESULT);
        }else {

            String fechaTarjeta = (String) txtFecha.getText();
            String numTarjeta = (String) txtTarjeta.getText();

            Intent intent = new Intent(datosSubscripcion.this, aceptarSuscripcion.class);
            intent.putExtra("fechaTarjeta", fechaTarjeta);
            intent.putExtra("numTarjeta", numTarjeta);
            intent.putExtra("precio", precio);
            intent.putExtra("plan", plan);
            startActivity(intent);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == SCAN_RESULT){

            if (data != null  && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)){
                CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);
                txtTarjeta.setText(scanResult.getRedactedCardNumber());
                if (scanResult.isExpiryValid()){
                    String mes = String.valueOf(scanResult.expiryMonth);
                    String anio = String.valueOf(scanResult.expiryYear);
                    txtFecha.setText(mes + "/" + anio);

                    btnEscanear.setText("Pagar S/. " + precio);
                }
            }
        }
    }
}