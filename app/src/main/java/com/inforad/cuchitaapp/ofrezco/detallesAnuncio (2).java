package com.inforad.cuchitaapp.ofrezco;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.necesito.contacto;
import com.squareup.picasso.Picasso;

public class detallesAnuncio extends AppCompatActivity {

    private ImageView imgAnuncio;
    private TextView txtTitulo, txtNumero, txtDescripcion;
    private FloatingActionButton btnMensaje;
    private String img, titulo, numero, descripcion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_detalles_anuncio);
        Inicio();

        btnMensaje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(detallesAnuncio.this, contacto.class);
                startActivity(intent);
            }
        });
    }

    private void Inicio() {
        imgAnuncio = findViewById(R.id.ivAnuncio);
        txtTitulo = findViewById(R.id.tvTituloAnuncio);
        txtDescripcion = findViewById(R.id.tvDetalles);
        txtNumero = findViewById(R.id.tvNumContacto);
        btnMensaje = findViewById(R.id.fabMensajes);

        obtenerDatos();

    }

    private void obtenerDatos(){
        img = getIntent().getStringExtra("imagen");
        titulo = getIntent().getStringExtra("titulo");
        numero = getIntent().getStringExtra("numero");
        descripcion = getIntent().getStringExtra("descripcion");

        Picasso.with(detallesAnuncio.this).load(img).into(imgAnuncio);
        txtTitulo.setText(titulo);
        txtNumero.setText(numero);
        txtDescripcion.setText(descripcion);
    }
}