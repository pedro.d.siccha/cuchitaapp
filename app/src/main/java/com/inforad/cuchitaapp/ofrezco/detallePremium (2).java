package com.inforad.cuchitaapp.ofrezco;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.provider.UserProvider;

public class detallePremium extends AppCompatActivity {

    private Button btnSuscribir;
    private UserProvider mUserProvider;
    private FirebaseAuth mAuth;
    private String idUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_detalle_premium);
        inicar();

        btnSuscribir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mUserProvider.actualizarActividad(idUsuario, "INICIO").addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        mUserProvider.actualizarPlan(idUsuario, "PREMIUM").addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Intent intent = new Intent(detallePremium.this, nombreEmpresa.class);
                                intent.putExtra("plan", "PREMIUM");
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }
                        });
                    }
                });
            }
        });
    }

    private void inicar(){
        btnSuscribir = findViewById(R.id.pagarPremium);
        mUserProvider = new UserProvider();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
    }
}