package com.inforad.cuchitaapp.ofrezco;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.Seccion;
import com.inforad.cuchitaapp.provider.seccionProvider;

import java.io.File;

public class nuevaSeccion extends AppCompatActivity {

    private String idNegocio;
    private EditText txtSeccion;
    private Button btnGuardar;
    private final int GALLERY_REQUEST = 1;
    private seccionProvider mSeccionProvider;
    private ProgressDialog mProgressDialog;
    private FirebaseAuth mAuth;
    private File mImageFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_nueva_seccion);

        Iniciar();

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mProgressDialog.setMessage("Generando sección...");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.show();

                guardar();



            }
        });
    }

    private void guardar() {

        Seccion seccion = new Seccion("", idNegocio, txtSeccion.getText().toString(), "", "ACTIVO");
        mSeccionProvider.crearSeccion(seccion, idNegocio, txtSeccion.getText().toString()).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                mProgressDialog.dismiss();
                Intent intent = new Intent(nuevaSeccion.this, inventario.class);
                intent.putExtra("idNegocio", idNegocio);
                startActivity(intent);
            }
        });
    }

    private void Iniciar() {
        txtSeccion = findViewById(R.id.txtSeccion);
        btnGuardar = findViewById(R.id.bGuardar);

        mSeccionProvider = new seccionProvider();
        mProgressDialog = new ProgressDialog(this);
        mAuth = FirebaseAuth.getInstance();

        obtenerDatos();
    }

    private void obtenerDatos() {

        idNegocio = getIntent().getStringExtra("idNegocio");

    }
}