package com.inforad.cuchitaapp.ofrezco;

import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.hbb20.CountryCodePicker;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.adapters.direccionAdaptador;
import com.inforad.cuchitaapp.models.Direccion;
import com.inforad.cuchitaapp.provider.UserProvider;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class editarUsuario extends AppCompatActivity {

    private TextInputEditText inputNombre, inputApellido, inputAlias, inputDni, inputTelefono;
    private CountryCodePicker cbTelefono;
    private RecyclerView listaDireccion;
    private CircleImageView imgPerfil;
    private Button btnGuardar;
    private FirebaseAuth mAuth;
    private String idUsuario;
    private UserProvider mUserProvider;
    private DatabaseReference mDatabase;
    private direccionAdaptador adaptadorDireccion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_editar_usuario);
        Iniciar();

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                guardar();
            }
        });
    }

    private void guardar() {
        mUserProvider.actualizarUsuario(idUsuario, inputAlias.getText().toString(), inputApellido.getText().toString(), inputDni.getText().toString(), inputNombre.getText().toString(), inputTelefono.getText().toString()).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(editarUsuario.this, "Se actualizó correctamente", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void Iniciar() {
        inputNombre = findViewById(R.id.etNombre);
        inputApellido = findViewById(R.id.etApellido);
        inputAlias = findViewById(R.id.etAlias);
        inputDni = findViewById(R.id.etDni);
        inputTelefono = findViewById(R.id.etTelefono);
        cbTelefono = findViewById(R.id.cbRegion);
        imgPerfil = findViewById(R.id.civPerfil);
        btnGuardar = findViewById(R.id.bGuardar);
        listaDireccion = findViewById(R.id.rvDirecciones);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        listaDireccion.setLayoutManager(linearLayoutManager);

        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mUserProvider = new UserProvider();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        cargarDatos();
    }

    private void cargarDatos() {
        mUserProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String img = snapshot.child("imagen").getValue().toString();
                    inputNombre.setText(snapshot.child("nombre").getValue().toString());
                    inputApellido.setText(snapshot.child("apellido").getValue().toString());
                    inputAlias.setText(snapshot.child("alias").getValue().toString());
                    inputDni.setText(snapshot.child("dni").getValue().toString());
                    inputTelefono.setText(snapshot.child("telefono").getValue().toString());
                    Picasso.with(editarUsuario.this).load(img).into(imgPerfil);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Query queryReclamo = mDatabase.child("direccion_local").child(idUsuario);
        FirebaseRecyclerOptions<Direccion> options = new FirebaseRecyclerOptions.Builder<Direccion>().setQuery(queryReclamo, Direccion.class).build();
        adaptadorDireccion = new direccionAdaptador(options, editarUsuario.this);
        listaDireccion.setAdapter(adaptadorDireccion);
        adaptadorDireccion.startListening();
    }
}