package com.inforad.cuchitaapp.ofrezco;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.PedidoProducto;
import com.inforad.cuchitaapp.provider.negocioInventarioProvider;
import com.inforad.cuchitaapp.provider.pedidoProductoProvider;
import com.inforad.cuchitaapp.provider.productoProvider;
import com.squareup.picasso.Picasso;

public class ventaLocal extends AppCompatActivity {

    private TextView txtPrecio, txtProducto;
    private ImageView imgProducto;
    private EditText inputCantidad;
    private Button btnAceptar;
    private String idNegocio, idProducto, idUsuario, nomProducto, dni, nombre;
    private negocioInventarioProvider mInventarioProvider;
    private pedidoProductoProvider mPedidoProvider;
    private double precio, stock;
    private productoProvider mProductoProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_venta_local);
        Iniciar();

        inputCantidad.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int inicio, int contador, int fin) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!charSequence.toString().equals(null)){
                    if (Double.parseDouble(charSequence.toString()) > stock){
                        Toast.makeText(ventaLocal.this, "Por favor ingrese una cantidad correcta", Toast.LENGTH_SHORT).show();
                        inputCantidad.setFocusable(true);
                        btnAceptar.setEnabled(false);
                    }else {
                        txtPrecio.setText("S/. " + precio*Double.parseDouble(charSequence.toString()));
                        btnAceptar.setEnabled(true);
                    }
                }else {
                    txtPrecio.setText("S/. 0.00");
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double nuevaCantidad = Double.parseDouble(inputCantidad.getText().toString())*precio;
                PedidoProducto producto = new PedidoProducto(idNegocio, idUsuario, idProducto, nomProducto, inputCantidad.getText().toString(), nuevaCantidad);
                mPedidoProvider.create(producto, idNegocio, idUsuario, idProducto).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        actualizarStock(idProducto, Double.parseDouble(inputCantidad.getText().toString()), nuevaCantidad);

                    }
                });
            }
        });
    }

    private void actualizarStock(String idProducto, double cantidad, double addPrecio){
        mProductoProvider.obtenerProducto(idProducto).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String stock = snapshot.child("stock").getValue().toString();
                    double stockActual = Double.parseDouble(stock);
                    double nuevoStock = stockActual - cantidad;
                    String actualizado = String.valueOf(nuevoStock);

                    mProductoProvider.actualizarStock(idProducto, actualizado).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            actualizarPrecioPedido(addPrecio);
                        }
                    });

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void actualizarPrecioPedido(double precioProducto){
        mPedidoProvider.obtenerPedido(idNegocio, dni).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String prec = snapshot.child("precio").getValue().toString();
                    double precio = Double.parseDouble(prec);
                    double nuevoPrecio = precio + precioProducto;

                    mPedidoProvider.actualizarPrecio(idNegocio, idUsuario, nuevoPrecio).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Intent intent = new Intent(ventaLocal.this, ventasTienda.class);
                            intent.putExtra("idNegocio", idNegocio);
                            intent.putExtra("nombre", nombre);
                            intent.putExtra("dni", dni);
                            startActivity(intent);
                        }
                    });

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void Iniciar() {
        txtPrecio = findViewById(R.id.tvPrecio);
        txtProducto = findViewById(R.id.tvProducto);
        imgProducto = findViewById(R.id.ivProducto);
        inputCantidad = findViewById(R.id.etCantidad);
        btnAceptar = findViewById(R.id.bAceptar);

        btnAceptar.setEnabled(true);

        mInventarioProvider = new negocioInventarioProvider();
        mPedidoProvider = new pedidoProductoProvider();
        mProductoProvider = new productoProvider();


        cargarDatos();
    }

    private void cargarDatos() {
        idNegocio = getIntent().getStringExtra("idNegocio");
        idProducto = getIntent().getStringExtra("idProducto");
        idUsuario = getIntent().getStringExtra("idUsuario");
        nomProducto = getIntent().getStringExtra("nomProducto");
        dni = getIntent().getStringExtra("dni");
        nombre = getIntent().getStringExtra("nombre");

        mProductoProvider.obtenerProducto(idProducto).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    txtProducto.setText(snapshot.child("nombre").getValue().toString());
                    precio = Double.parseDouble(snapshot.child("precioUnidad").getValue().toString());
                    stock = Double.parseDouble(snapshot.child("stock").getValue().toString());
                    String imagen = snapshot.child("imgProducto").getValue().toString();
                    Picasso.with(ventaLocal.this).load(imagen).into(imgProducto);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}