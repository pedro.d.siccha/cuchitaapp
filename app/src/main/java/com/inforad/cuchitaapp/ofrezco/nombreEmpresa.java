package com.inforad.cuchitaapp.ofrezco;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.provider.negocioProvider;

import java.io.File;


public class nombreEmpresa extends AppCompatActivity {

    private TextInputEditText txtNombreEmpresa, txtRuc;
    private Button btnSiguiente;
    private File mImageFile;
    private String nombreNegocio, Ruc, urlImagen, idUsuario, plan;
    private final int GALLERY_REQUEST = 1;
    private ProgressDialog mProgressDialog;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private negocioProvider mNegocioProvider;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_nombre_empresa);
        Iniciar();

        btnSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(nombreEmpresa.this, seleccionarCategoria.class);
                intent.putExtra("urlLogo", "https://firebasestorage.googleapis.com/v0/b/cuchitasac-f8294.appspot.com/o/imgUsuario%2Ftienda.png?alt=media&token=25549af9-b318-4a16-a04b-e8f1c19dd91d");
                intent.putExtra("nombreNegocio", txtNombreEmpresa.getText().toString());
                intent.putExtra("idUsuario", idUsuario);
                intent.putExtra("plan", plan);
                intent.putExtra("ruc", txtRuc.getText().toString());
                startActivity(intent);

            }
        });
    }

    private void Iniciar(){

        FirebaseUser user = mFirebaseAuth.getInstance().getCurrentUser();
        String id_registro = user.getUid();
        txtNombreEmpresa = findViewById(R.id.txtNomNegocio);
        txtRuc = findViewById(R.id.txtRucNegocio);
        btnSiguiente = findViewById(R.id.btnContinuarNN);
        mProgressDialog = new ProgressDialog(this);
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        idUsuario = mAuth.getCurrentUser().getUid();
        mNegocioProvider = new negocioProvider();
        obtenerDatosActivityAnterior();
    }

    private void obtenerDatosActivityAnterior(){
        plan = getIntent().getStringExtra("plan");
    }

    /*
    private void guardarAnuncio(){
        Negocio negocio = new Negocio();
        negocio.setNombre(nombreNegocio);
        negocio.setImagen(urlLogo);
        negocio.setDescripcion(descripcion);

        mDatabaseNegocio.child(ruc).setValue(negocio).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

                regNegocioUsuario(idUsuario, "ADMINISTRADOR");

            }
        });
    }

     */
}