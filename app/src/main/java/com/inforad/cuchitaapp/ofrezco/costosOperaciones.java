package com.inforad.cuchitaapp.ofrezco;

import android.os.Bundle;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.inforad.cuchitaapp.R;

public class costosOperaciones extends AppCompatActivity {

    private String idNegocio, idUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_costos_operaciones);
        obtenerDatosAnteriores();
    }

    private void obtenerDatosAnteriores(){
        idNegocio = getIntent().getStringExtra("negocio_id");
        idUsuario = getIntent().getStringExtra("usuario_id");
    }
}