package com.inforad.cuchitaapp.ofrezco;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.adapters.inventarioVentasAdaptador;
import com.inforad.cuchitaapp.models.PedidoProducto;
import com.inforad.cuchitaapp.provider.PedidoProvider;
import com.inforad.cuchitaapp.provider.negocioInventarioProvider;

public class ventasTienda extends AppCompatActivity {

    private String idNegocio, nombre, dni;
    private RecyclerView recyclerViewProductos;
    private inventarioVentasAdaptador adaptadorInventario;
    private negocioInventarioProvider mInvetarioProvider;
    private DatabaseReference mDatabase;
    private TextView txtCliente;
    private Button btnAgregar, btnConfirmar;
    private ImageView btnAtras;
    private PedidoProvider mPedidoProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_ventas_tienda);

        Iniciar();

        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ventasTienda.this, agregarProductoVenta.class);
                intent.putExtra("idNegocio", idNegocio);
                intent.putExtra("dni", dni);
                intent.putExtra("nombre", nombre);
                startActivity(intent);
            }
        });

        btnConfirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPedidoProvider.actualizarEstado(idNegocio, dni, "ACEPTADO").addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(ventasTienda.this, "Preparando pedido del Cliente: " + nombre, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(ventasTienda.this, generarPedido.class);
                        intent.putExtra("idNegocio", idNegocio);
                        startActivity(intent);
                    }
                });
            }
        });

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ventasTienda.this, generarPedido.class);
                intent.putExtra("idNegocio", idNegocio);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == event.KEYCODE_BACK){
            Intent intent = new Intent(ventasTienda.this, generarPedido.class);
            intent.putExtra("idNegocio", idNegocio);
            startActivity(intent);
        }
        return super.onKeyUp(keyCode, event);
    }

    private void Iniciar() {
        txtCliente = findViewById(R.id.tvNombre);
        btnAgregar = findViewById(R.id.bAgregar);
        btnConfirmar = findViewById(R.id.bConfirmar);
        btnAtras = findViewById(R.id.ivAtras);

        recyclerViewProductos = findViewById(R.id.recyclerProductos);
        LinearLayoutManager linearLayoutManagerProductos = new LinearLayoutManager(this);
        recyclerViewProductos.setLayoutManager(linearLayoutManagerProductos);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mInvetarioProvider = new negocioInventarioProvider();
        mPedidoProvider = new PedidoProvider();

        obtenerDatos();
    }

    private void obtenerDatos() {
        idNegocio = getIntent().getStringExtra("idNegocio");
        nombre = getIntent().getStringExtra("nombre");
        dni = getIntent().getStringExtra("dni");

        txtCliente.setText(nombre);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Query query = mDatabase.child("Pedido").child(idNegocio).child(dni).child("Producto");

        FirebaseRecyclerOptions<PedidoProducto> options = new FirebaseRecyclerOptions.Builder<PedidoProducto>().setQuery(query, PedidoProducto.class).build();
        adaptadorInventario = new inventarioVentasAdaptador(options, ventasTienda.this, idNegocio, dni);
        recyclerViewProductos.setAdapter(adaptadorInventario);
        adaptadorInventario.startListening();

    }
}