package com.inforad.cuchitaapp.ofrezco;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.adapters.productoPedidoAdaptador;
import com.inforad.cuchitaapp.models.PedidoProducto;
import com.inforad.cuchitaapp.provider.UserProvider;
import com.inforad.cuchitaapp.provider.direccionProvider;
import com.inforad.cuchitaapp.provider.pedidoProductoProvider;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class pedidoDetalle extends AppCompatActivity {

    private CircleImageView imgPerfil;
    private TextView txtCliente, txtDni, txtTelefono, txtDireccion;
    private Button btnAceptar;
    private String idCliente, idNegocio;
    private UserProvider mUserProvider;
    private pedidoProductoProvider mPedidoProvider;
    private DatabaseReference mDatabase;
    private RecyclerView recyclerListaProductos;
    private productoPedidoAdaptador adaptadorProductoPedido;
    private direccionProvider mDireccionProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_pedido_detalle);
        Iniciar();
    }

    private void Iniciar() {

        imgPerfil = findViewById(R.id.civPerfilCliente);
        txtCliente = findViewById(R.id.tvNombreCliente);
        txtDni = findViewById(R.id.tvDni);
        txtTelefono = findViewById(R.id.tvTelefono);
        btnAceptar = findViewById(R.id.bAceptar);
        txtDireccion = findViewById(R.id.tvDireccion);
        recyclerListaProductos = findViewById(R.id.recyclerListaProductos);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerListaProductos.setLayoutManager(linearLayoutManager);

        mUserProvider = new UserProvider();
        mPedidoProvider = new pedidoProductoProvider();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDireccionProvider = new direccionProvider();

        cargarDatos();

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                funcion();
            }
        });
    }

    private void cargarDatos() {
        idCliente = getIntent().getStringExtra("idCliente");
        idNegocio = getIntent().getStringExtra("idNegocio");

        mDireccionProvider.obtenerDireccionLista(idCliente,"CASA").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    txtDireccion.setText(snapshot.child("ciudad").getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        mUserProvider.obtenerUsuario(idCliente).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String img = snapshot.child("imagen").getValue().toString();
                    Picasso.with(pedidoDetalle.this).load(img).into(imgPerfil);
                    txtCliente.setText(snapshot.child("alias").getValue().toString());
                    txtDni.setText(snapshot.child("dni").getValue().toString());
                    txtTelefono.setText(snapshot.child("telefono").getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        mPedidoProvider.obtenerPedido(idNegocio, idCliente).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                switch (snapshot.child("estado").getValue().toString()){
                    case "PENDIENTE":
                        //Cuando el pedido esta en espera para el cliente, y el boton ACEPTAR da pase para que el pedido se acepte
                        btnAceptar.setText("ACEPTAR");
                        break;
                    case "ACEPTAR":
                        //Cuando el pedido esta aceptado, y el boton ENVIAR es para que avise que está listo para el envio
                        btnAceptar.setText("ENVIAR");
                        break;
                    case "ENVIADO":
                        //Cuando el pedido esta en espera para el cliente, y el boton ACEPTAR da pase para que el pedido se acepte
                        btnAceptar.setText("ENTREGADO");
                        break;
                    case "FIN":
                        btnAceptar.setText("COBRAR");
                        break;
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    public void funcion()
    {

        switch (btnAceptar.getText().toString())
        {
            case "PENDIENTE":
                mPedidoProvider.actualizarEstado(idNegocio, idCliente, "PENDIENTE").addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Intent intent = new Intent(pedidoDetalle.this, pedidoNegocio.class);
                        intent.putExtra("idNegocio", idNegocio);
                        startActivity(intent);
                    }
                });
                break;
            case "ACEPTAR":
                mPedidoProvider.actualizarEstado(idNegocio, idCliente, "ACEPTAR").addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Intent intent = new Intent(pedidoDetalle.this, pedidoAceptado.class);
                        intent.putExtra("idNegocio", idNegocio);
                        startActivity(intent);
                    }
                });
                break;
            case "ENVIAR":
                mPedidoProvider.actualizarEstado(idNegocio, idCliente, "ENVIADO").addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Intent intent = new Intent(pedidoDetalle.this, pedidoEnviado.class);
                        intent.putExtra("idNegocio", idNegocio);
                        startActivity(intent);
                    }
                });
                break;
            case "ENTREGADO":
                mPedidoProvider.actualizarEstado(idNegocio, idCliente, "FIN").addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(pedidoDetalle.this, "PEDIDO FINALIZADO", Toast.LENGTH_SHORT).show();
                    }
                });
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        Query queryPendiente = mDatabase.child("Pedido").child(idNegocio).child(idCliente).child("PRODUCTO");
        FirebaseRecyclerOptions<PedidoProducto> options = new FirebaseRecyclerOptions.Builder<PedidoProducto>().setQuery(queryPendiente, PedidoProducto.class).build();
        adaptadorProductoPedido = new productoPedidoAdaptador(options, pedidoDetalle.this, idNegocio);
        recyclerListaProductos.setAdapter(adaptadorProductoPedido);
        adaptadorProductoPedido.startListening();

    }
}