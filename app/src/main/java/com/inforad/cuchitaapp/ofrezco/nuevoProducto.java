package com.inforad.cuchitaapp.ofrezco;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.Producto;
import com.inforad.cuchitaapp.models.Seccion;
import com.inforad.cuchitaapp.models.SeccionProducto;
import com.inforad.cuchitaapp.provider.productoProvider;
import com.inforad.cuchitaapp.provider.seccionProductoProvider;
import com.inforad.cuchitaapp.utilidades.CompresorBitmapImage;
import com.inforad.cuchitaapp.utilidades.FileUtilidades;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class nuevoProducto extends AppCompatActivity {

    private EditText txtProducto,  txtStock, txtStockMinimo, txtStockMaximo, txtTiempo, txtPrecio, txtDescripcion, txtCodigoProducto;
    private Button btnAceptar, btnCancelar;
    private String idNegocio, idUsuario;
    private FirebaseAuth mAuth;
    private Spinner cbUnidadMedida, scSeccion;
    private CircleImageView ivProducto;
    private DatabaseReference mDatabase;
    private final int GALLERY_REQUEST = 1;
    private File mImageFile;
    private ProgressDialog mProgressDialog;
    private seccionProductoProvider mSeccionProductoProvider;
    private productoProvider mProductoProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_nuevo_producto);
        Iniciar();

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mProgressDialog.setMessage("Cargando Imagen...");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.show();

                guardarImagen(txtProducto.getText().toString(), idNegocio);
            }
        });

        ivProducto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                abrirGaleria();
            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(nuevoProducto.this, inventario.class);
                intent.putExtra("idNegocio", idNegocio);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == event.KEYCODE_BACK){
            Intent intent = new Intent(nuevoProducto.this, inventario.class);
            intent.putExtra("idNegocio", idNegocio);
            startActivity(intent);
        }
        return super.onKeyUp(keyCode, event);
    }

    private void Iniciar(){
        txtCodigoProducto = findViewById(R.id.inputCod);
        txtProducto = findViewById(R.id.inputProducto);
        txtStock = findViewById(R.id.inputStock);
        txtStockMinimo = findViewById(R.id.inputStockMinimo);
        txtStockMaximo = findViewById(R.id.inputStockMaximo);
        txtPrecio = findViewById(R.id.inputPrecUnidad);
        txtDescripcion = findViewById(R.id.inputDescripcion);
        txtTiempo = findViewById(R.id.inputTiempo);
        btnAceptar = findViewById(R.id.Aceptar);
        btnCancelar = findViewById(R.id.Cancelar);
        cbUnidadMedida = findViewById(R.id.spUnidadMedida);
        ivProducto = findViewById(R.id.imgProducto);
        scSeccion = findViewById(R.id.spSeccion);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mProgressDialog = new ProgressDialog(this);
        mSeccionProductoProvider = new seccionProductoProvider();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mProductoProvider = new productoProvider();
        obtenerDatosAnteriores();
        listaSeccion();
    }

    public void listaSeccion(){
        List<Seccion> seccion = new ArrayList<>();
        mDatabase.child("Seccion").child(idNegocio).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    for (DataSnapshot ds: snapshot.getChildren()){
                        String idSeccion = ds.getKey();

                        seccion.add(new Seccion(idSeccion, idNegocio, idSeccion, "", ""));
                    }
                    ArrayAdapter<Seccion> arrayAdapter = new ArrayAdapter<>(nuevoProducto.this, android.R.layout.simple_dropdown_item_1line, seccion);
                    scSeccion.setAdapter(arrayAdapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void abrirGaleria(){
        Intent GaleriaIntent = new Intent(Intent.ACTION_GET_CONTENT);
        GaleriaIntent.setType("image/*");
        startActivityForResult(GaleriaIntent, GALLERY_REQUEST);
    }

    private void guardarImagen(String nombre, String ruc){
        byte[] imageByte = CompresorBitmapImage.getImage(this, mImageFile.getPath(), 500, 500);
        StorageReference storage = FirebaseStorage.getInstance().getReference().child("imgProducto").child(ruc).child(nombre + ".jpg");
        UploadTask uploadTask = storage.putBytes(imageByte);
        uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()){
                    storage.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            mProgressDialog.dismiss();
                            generarProducto(txtCodigoProducto.getText().toString(), scSeccion.getSelectedItem().toString(), txtProducto.getText().toString(), txtDescripcion.getText().toString(), txtStock.getText().toString(), txtStockMinimo.getText().toString(), txtStockMaximo.getText().toString(), cbUnidadMedida.getSelectedItem().toString(), txtPrecio.getText().toString(),uri.toString(), txtTiempo.getText().toString(), "ACTIVO");
                        }
                    });
                }else {
                    Toast.makeText(nuevoProducto.this, "Hubo un error al subir la imagen", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == GALLERY_REQUEST && resultCode == RESULT_OK){
            try {
                mImageFile = FileUtilidades.from(this, data.getData());
                ivProducto.setImageBitmap(BitmapFactory.decodeFile(mImageFile.getAbsolutePath()));
            }catch (Exception e){
                Log.d("Error:", "Mensaje de error: " + e.getMessage());
            }
        }

    }

    private void obtenerDatosAnteriores(){
        idNegocio = getIntent().getStringExtra("idNegocio");
    }

    private void generarProducto(String codProducto, String idSeccion, String nombre, String detalle, String stock, String stockMinimo, String stockMaximo, String unidadMedida, String precioUnidad,String imagen, String tiempo, String estado){

        String id = idNegocio + "-" + codProducto;

        Producto producto = new Producto(id, idNegocio,  idSeccion, idUsuario, nombre, detalle, stock, stockMinimo, stockMaximo, unidadMedida, precioUnidad, imagen, tiempo, estado);
        mProductoProvider.create(producto, id).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                SeccionProducto seccion = new SeccionProducto(scSeccion.getSelectedItem().toString(), idNegocio, idUsuario, nombre);
                mSeccionProductoProvider.crearSeccionProducto(seccion, idNegocio, scSeccion.getSelectedItem().toString(), nombre).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Intent intent = new Intent(nuevoProducto.this, inventario.class);
                        intent.putExtra("idNegocio", idNegocio);
                        startActivity(intent);
                    }
                });
            }
        });
    }
}