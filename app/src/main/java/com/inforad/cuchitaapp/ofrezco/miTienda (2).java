package com.inforad.cuchitaapp.ofrezco;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.necesito.contacto;
import com.inforad.cuchitaapp.provider.PedidoProvider;
import com.inforad.cuchitaapp.provider.negocioProvider;

public class miTienda extends AppCompatActivity {

    private LinearLayout btnPedido, btnProductos, btnComentario, btnOfertas, btnVentas, btnAjustes;
    private String idNegocio, idUsuario;
    private PedidoProvider pedidoProvider;
    private TextView txtNombre;
    private FirebaseAuth mAuth;
    private negocioProvider mNegocioProvider;
    private FloatingActionButton btnMensaje;
    private ImageView btnAtras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_mi_tienda);

        Iniciar();

        btnPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(miTienda.this, pedidoNegocio.class);
                intent.putExtra("idNegocio", idNegocio);
                startActivity(intent);
            }
        });

        btnProductos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(miTienda.this, inventario.class);
                intent.putExtra("idNegocio", idNegocio);
                startActivity(intent);
            }
        });

        btnComentario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(miTienda.this, administrarComentarios.class);
                intent.putExtra("idNegocio", idNegocio);
                startActivity(intent);
            }
        });

        btnOfertas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(miTienda.this, administrarOfertas.class);
                intent.putExtra("idNegocio", idNegocio);
                startActivity(intent);
            }
        });

        btnVentas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(miTienda.this, generarPedido.class);
                intent.putExtra("idNegocio", idNegocio);
                startActivity(intent);
            }
        });

        btnAjustes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(miTienda.this, misAjustes.class);
                intent.putExtra("idNegocio", idNegocio);
                startActivity(intent);
            }
        });

        btnMensaje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(miTienda.this, contacto.class);
                startActivity(intent);
            }
        });

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(miTienda.this, perfilNegocio.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == event.KEYCODE_BACK){
            Intent intent = new Intent(miTienda.this, perfilNegocio.class);
            startActivity(intent);
        }
        return super.onKeyUp(keyCode, event);
    }

    private void Iniciar() {
        btnPedido = findViewById(R.id.llPedido);
        btnProductos = findViewById(R.id.llProductos);
        btnComentario = findViewById(R.id.llComentarios);
        btnOfertas = findViewById(R.id.llOfertas);
        txtNombre = findViewById(R.id.tvNombreTienda);
        btnVentas = findViewById(R.id.llVenta);
        btnAjustes = findViewById(R.id.llAjustes);
        btnMensaje = findViewById(R.id.fabMensajes);
        btnAtras = findViewById(R.id.ivAtras);

        pedidoProvider = new PedidoProvider();
        mAuth = FirebaseAuth.getInstance();
        mNegocioProvider = new negocioProvider();

        idUsuario = mAuth.getCurrentUser().getUid();

        obtenerDatosAnteriores();
/*
        pedidoProvider.obtenerPedido(idUsuario, idNegocio).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    txtNombre.setText(snapshot.child("nombre").getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

 */

    }

    private void obtenerDatosAnteriores(){

        idNegocio = getIntent().getStringExtra("negocio_id");

        mNegocioProvider.getNegocio(idNegocio).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    txtNombre.setText(snapshot.child("nombre").getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }
}