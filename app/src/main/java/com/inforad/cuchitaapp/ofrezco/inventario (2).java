package com.inforad.cuchitaapp.ofrezco;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.adapters.inventarioAdaptador;
import com.inforad.cuchitaapp.adapters.seccionAdaptador;
import com.inforad.cuchitaapp.models.NegocioInventario;
import com.inforad.cuchitaapp.models.Seccion;

import java.util.ArrayList;
import java.util.List;

public class inventario extends AppCompatActivity {

    private LinearLayout btnNuevoProducto, btnNuevaSeccion;
    private String idNegocio;
    private RecyclerView recyclerViewInventario;
    private RecyclerView recyclerViewSeccion;
    private inventarioAdaptador adaptadorInventario;
    private seccionAdaptador adaptadorSeccion;
    private DatabaseReference mDatabase;
    //private ArrayList<NegocioAnuncio> mAnuncioList = new ArrayList<>();
    private List<NegocioInventario> producto = new ArrayList<>();
    private ImageView btnAtras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_inventario);
        obtenerDatosAnteriores();
        Iniciar();

        btnNuevoProducto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(inventario.this, nuevoProducto.class);
                intent.putExtra("idNegocio", idNegocio);
                startActivity(intent);
            }
        });

        btnNuevaSeccion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(inventario.this, nuevaSeccion.class);
                intent.putExtra("idNegocio", idNegocio);
                startActivity(intent);
            }
        });

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(inventario.this, miTienda.class);
                intent.putExtra("negocio_id", idNegocio);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == event.KEYCODE_BACK){
            Intent intent = new Intent(inventario.this, miTienda.class);
            intent.putExtra("negocio_id", idNegocio);
            startActivity(intent);
        }
        return super.onKeyUp(keyCode, event);
    }

    private void Iniciar(){
        btnAtras = findViewById(R.id.ivAtras);
        btnNuevoProducto = findViewById(R.id.btnProducto);
        btnNuevaSeccion = findViewById(R.id.btnSeccion);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        recyclerViewInventario = findViewById(R.id.recyclerProductos);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerViewInventario.setLayoutManager(linearLayoutManager);
        recyclerViewSeccion = findViewById(R.id.recyclerSeccion);
        LinearLayoutManager linearLayoutManagerSeccion = new LinearLayoutManager(this);
        recyclerViewSeccion.setLayoutManager(linearLayoutManagerSeccion);
    }

    private void obtenerDatosAnteriores(){
        idNegocio = getIntent().getStringExtra("idNegocio");
    }

    @Override
    protected void onStart() {
        super.onStart();

        Query query = mDatabase.child("Producto").orderByChild("idTienda").equalTo(idNegocio);
        FirebaseRecyclerOptions<NegocioInventario> options = new FirebaseRecyclerOptions.Builder<NegocioInventario>().setQuery(query, NegocioInventario.class).build();
        adaptadorInventario = new inventarioAdaptador(options, inventario.this, idNegocio);
        recyclerViewInventario.setAdapter(adaptadorInventario);
        adaptadorInventario.startListening();

        Query querySeccion = mDatabase.child("Seccion").child(idNegocio);
        FirebaseRecyclerOptions<Seccion> optionsSeccion = new FirebaseRecyclerOptions.Builder<Seccion>().setQuery(querySeccion, Seccion.class).build();
        adaptadorSeccion = new seccionAdaptador(optionsSeccion, inventario.this, idNegocio);
        recyclerViewSeccion.setAdapter(adaptadorSeccion);
        adaptadorSeccion.startListening();

    }
}