package com.inforad.cuchitaapp.ofrezco;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.adapters.cuponesAdaptador;
import com.inforad.cuchitaapp.adapters.descuentoAdaptador;
import com.inforad.cuchitaapp.adapters.ofertaAdaptador;
import com.inforad.cuchitaapp.models.Cupones;
import com.inforad.cuchitaapp.models.NegocioInventario;
import com.inforad.cuchitaapp.provider.negocioProvider;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class administrarOfertas extends AppCompatActivity {

    private CircleImageView imgTienda;
    private TextView txtNombreTienda;
    private LinearLayout btnAgregarOferta, btnAgregarDescuento, btnAgregarCupon;
    private RecyclerView recyclerListaOferta, recyclerListaDescuento, recyclerListaCupon;
    private String idNegocio;
    private negocioProvider mNegocioProvider;
    private DatabaseReference mDatabase;
    private ofertaAdaptador adaptadorOferta;
    private descuentoAdaptador adaptadorDescuento;
    private cuponesAdaptador adaptadorCupones;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_administrar_ofertas);
        Iniciar();

        btnAgregarOferta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(administrarOfertas.this, agregarOferta.class);
                intent.putExtra("idNegocio", idNegocio);
                startActivity(intent);
            }
        });

        btnAgregarDescuento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(administrarOfertas.this, agregarDescuento.class);
                intent.putExtra("idNegocio", idNegocio);
                startActivity(intent);
            }
        });

        btnAgregarCupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(administrarOfertas.this, agregarCupon.class);
                intent.putExtra("idNegocio", idNegocio);
                startActivity(intent);
            }
        });

    }

    private void Iniciar() {
        imgTienda = findViewById(R.id.civPerfilTienda);
        txtNombreTienda = findViewById(R.id.tvNombreTienda);
        btnAgregarOferta = findViewById(R.id.llNuevaOferta);
        btnAgregarDescuento = findViewById(R.id.llNuevoDescuento);
        btnAgregarCupon = findViewById(R.id.llNuevoCupon);
        recyclerListaOferta = findViewById(R.id.recyclerListaOfertas);
        LinearLayoutManager linearLayoutManagerOferta = new LinearLayoutManager(this);
        recyclerListaOferta.setLayoutManager(linearLayoutManagerOferta);
        recyclerListaDescuento = findViewById(R.id.recyclerListaDescuentos);
        LinearLayoutManager linearLayoutManagerDescuento = new LinearLayoutManager(this);
        recyclerListaDescuento.setLayoutManager(linearLayoutManagerDescuento);
        recyclerListaCupon = findViewById(R.id.recyclerListaCupones);
        LinearLayoutManager linearLayoutManagerCupon = new LinearLayoutManager(this);
        recyclerListaCupon.setLayoutManager(linearLayoutManagerCupon);

        mNegocioProvider = new negocioProvider();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        cargarDatos();
    }

    private void cargarDatos() {
        idNegocio = getIntent().getStringExtra("idNegocio");

        mNegocioProvider.getNegocio(idNegocio).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String img = snapshot.child("imagen").getValue().toString();
                    Picasso.with(administrarOfertas.this).load(img).into(imgTienda);
                    txtNombreTienda.setText(snapshot.child("nombre").getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        //Cargar lista de Ofertas
        Query queryOferta = mDatabase.child("Negocio_Inventario").child(idNegocio).orderByChild("estado").equalTo("OFERTA");
        FirebaseRecyclerOptions<NegocioInventario> optionsOfertas = new FirebaseRecyclerOptions.Builder<NegocioInventario>().setQuery(queryOferta, NegocioInventario.class).build();
        adaptadorOferta = new ofertaAdaptador(optionsOfertas, administrarOfertas.this);
        recyclerListaOferta.setAdapter(adaptadorOferta);
        adaptadorOferta.startListening();

        //Cargar lista de Descuentos
        Query queryDescuento = mDatabase.child("Negocio_Inventario").child(idNegocio).orderByChild("estado").equalTo("DESCUENTO");
        FirebaseRecyclerOptions<NegocioInventario> optionsDescuento = new FirebaseRecyclerOptions.Builder<NegocioInventario>().setQuery(queryDescuento, NegocioInventario.class).build();
        adaptadorDescuento = new descuentoAdaptador(optionsDescuento, administrarOfertas.this);
        recyclerListaDescuento.setAdapter(adaptadorDescuento);
        adaptadorDescuento.startListening();

        //Cargar lista de Cupones
        Query queryCupones = mDatabase.child("Cupones").child(idNegocio);
        FirebaseRecyclerOptions<Cupones> optionsCupones = new FirebaseRecyclerOptions.Builder<Cupones>().setQuery(queryCupones, Cupones.class).build();
        adaptadorCupones = new cuponesAdaptador(optionsCupones, administrarOfertas.this);
        recyclerListaCupon.setAdapter(adaptadorCupones);
        adaptadorCupones.startListening();
    }
}