package com.inforad.cuchitaapp.ofrezco;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.MainActivity;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.adapters.reclamoAdaptador;
import com.inforad.cuchitaapp.models.Reclamo;
import com.inforad.cuchitaapp.provider.UserProvider;
import com.inforad.cuchitaapp.provider.reclamoProvider;

public class AjustesGenerales extends AppCompatActivity {

    private LinearLayout btnMiInformacion, btnSubscripcion;
    private ImageView btnAgregarReclamo, btnCerrarSesion;
    private EditText inputReclamo;
    private TextView txtNombre, txtTiempo, txtCosto, txtPlan;
    private RecyclerView rvReclamos;
    private FirebaseAuth mAuth;
    private String idUsuario;
    private UserProvider mUserProvider;
    private DatabaseReference mDatabase;
    private reclamoAdaptador adaptadorReclamo;
    private reclamoProvider mReclamoProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_ajustes_generales);
        Iniciar();

        btnAgregarReclamo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GuardarReclamo();
            }
        });

        btnMiInformacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AjustesGenerales.this, editarUsuario.class);
                startActivity(intent);
            }
        });

        btnSubscripcion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AjustesGenerales.this, seleccionarPlan.class);
                startActivity(intent);
            }
        });

        btnCerrarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
                Intent inte = new Intent(AjustesGenerales.this, MainActivity.class);
                startActivity(inte);
                finish();
            }
        });
    }

    private void GuardarReclamo() {
        Reclamo reclamo = new Reclamo(inputReclamo.getText().toString(), idUsuario, "", "PENDIENTE", "");
        mReclamoProvider.create(reclamo, idUsuario).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(AjustesGenerales.this, "Reclamo enviado correctamente", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void Iniciar() {
        btnMiInformacion = findViewById(R.id.llMiInformacion);
        btnSubscripcion = findViewById(R.id.llSubscripcion);
        btnAgregarReclamo = findViewById(R.id.ivAgregarReclamo);
        btnCerrarSesion = findViewById(R.id.ivSalir);
        inputReclamo = findViewById(R.id.txtReclamo);
        txtNombre = findViewById(R.id.tvNombre);
        txtTiempo = findViewById(R.id.tvTiempo);
        txtCosto = findViewById(R.id.tvCosto);
        txtPlan = findViewById(R.id.tvDescripcion);
        rvReclamos = findViewById(R.id.recyclerReclamo);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvReclamos.setLayoutManager(linearLayoutManager);

        mAuth = FirebaseAuth.getInstance();
        mUserProvider = new UserProvider();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mReclamoProvider = new reclamoProvider();

        cargarDatos();
    }

    private void cargarDatos() {

        idUsuario = mAuth.getCurrentUser().getUid();

        mUserProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    txtNombre.setText(snapshot.child("alias").getValue().toString());

                    switch (snapshot.child("plan").getValue().toString()){
                        case "BASICO":
                            txtTiempo.setText("1 Mes de prueba");
                            txtPlan.setText("PLAN BÁSICO");
                            txtCosto.setText("S/. 0.00");
                            break;
                        case "ESTANDAR":
                            txtTiempo.setText("1 Mes de prueba");
                            txtPlan.setText("PLAN ESTANDAR");
                            txtCosto.setText("S/. 0.00");
                            break;
                        case "PREMIUM":
                            txtTiempo.setText("1 Mes de prueba");
                            txtPlan.setText("PLAN PREMIUM");
                            txtCosto.setText("S/. 0.00");
                            break;
                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        Query queryReclamo = mDatabase.child("Reclamo").child(idUsuario);
        FirebaseRecyclerOptions<Reclamo> options = new FirebaseRecyclerOptions.Builder<Reclamo>().setQuery(queryReclamo, Reclamo.class).build();
        adaptadorReclamo = new reclamoAdaptador(options, AjustesGenerales.this);
        rvReclamos.setAdapter(adaptadorReclamo);
        adaptadorReclamo.startListening();
    }
}