package com.inforad.cuchitaapp.ofrezco;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.inforad.cuchitaapp.R;

public class aceptarSuscripcion extends AppCompatActivity {

    private Button btnPagar;
    private String plan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_aceptar_suscripcion);
        Iniciar();

        btnPagar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(aceptarSuscripcion.this, nombreEmpresa.class);
                intent.putExtra("plan", plan);
                startActivity(intent);
            }
        });
    }

    private void Iniciar(){
        btnPagar = findViewById(R.id.btnAceptarPago);
    }
    private void obtenerDatosActivityAnterior(){
        plan = getIntent().getStringExtra("plan");
    }

}