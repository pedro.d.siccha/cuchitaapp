package com.inforad.cuchitaapp.ofrezco;

import android.os.Bundle;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.adapters.comentarioAdaptador;
import com.inforad.cuchitaapp.models.Comentario;
import com.inforad.cuchitaapp.provider.negocioProvider;

public class administrarComentarios extends AppCompatActivity {

    private TextView txtNombre;
    private String idNegocio;
    private negocioProvider mNegocioProvider;
    private RecyclerView recyclerViewComentarios;
    private comentarioAdaptador adaptadorComentario;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_administrar_comentarios);
        Iniciar();
    }

    private void Iniciar() {
        txtNombre = findViewById(R.id.tvNombre);
        recyclerViewComentarios = findViewById(R.id.recyclerComentarios);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerViewComentarios.setLayoutManager(linearLayoutManager);

        mNegocioProvider = new negocioProvider();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        cargarDatos();
    }

    private void cargarDatos() {
        idNegocio = getIntent().getStringExtra("idNegocio");

        mNegocioProvider.getNegocio(idNegocio).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    txtNombre.setText(snapshot.child("nombre").getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

        Query query = mDatabase.child("Comentario").child(idNegocio);
        FirebaseRecyclerOptions<Comentario> options = new FirebaseRecyclerOptions.Builder<Comentario>().setQuery(query, Comentario.class).build();
        adaptadorComentario = new comentarioAdaptador(options, administrarComentarios.this);
        recyclerViewComentarios.setAdapter(adaptadorComentario);
        adaptadorComentario.startListening();

    }
}