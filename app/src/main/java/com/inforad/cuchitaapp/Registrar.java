package com.inforad.cuchitaapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.inforad.cuchitaapp.utilidades.CompresorBitmapImage;
import com.inforad.cuchitaapp.utilidades.FileUtilidades;

import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;

public class Registrar extends AppCompatActivity {

    private Button btnSiguiente;
    private CircleImageView btnImagen;
    private TextInputEditText txtNombre, txtApellido, txtNickName, txtDni;
    private String idUsuario, apellido, nickName, dni;
    private RadioButton rbtnVaron, rbtnMujer, rbtnNoEspecificar;
    private String seleccion;
    private File mImageFile;
    private final int GALLERY_REQUEST = 1;
    private ProgressDialog mProgressDialog;
    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_registrar);

        Iniciar();

        btnSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String idUsuario = mAuth.getCurrentUser().getUid();
                if (mImageFile != null){
                    mProgressDialog.setMessage("Cargando Imagen...");
                    mProgressDialog.setCanceledOnTouchOutside(false);
                    mProgressDialog.show();
                    guardarImagen(idUsuario, txtNombre.getText().toString(), txtApellido.getText().toString(), txtNickName.getText().toString(), txtDni.getText().toString(), seleccion);
                }else {
                    if (seleccion == "Varón"){
                        mProgressDialog.dismiss();
                        String img = "https://firebasestorage.googleapis.com/v0/b/cuchitasac-f8294.appspot.com/o/imgUsuario%2Fdefault.png?alt=media&token=19833469-0252-4066-9806-973fffe64b59";
                        siguiente(txtNombre.getText().toString(), txtApellido.getText().toString(), txtNickName.getText().toString(), txtDni.getText().toString(), img, seleccion);
                    }else if(seleccion == "Mujer"){
                        mProgressDialog.dismiss();
                        String img = "https://firebasestorage.googleapis.com/v0/b/cuchitasac-f8294.appspot.com/o/imgUsuario%2Fdefaultmujer.png?alt=media&token=d22f0aef-ca2b-4be9-94e1-fbc04b6ed623";
                        siguiente(txtNombre.getText().toString(), txtApellido.getText().toString(), txtNickName.getText().toString(), txtDni.getText().toString(), img, seleccion);
                    }else{
                        mProgressDialog.dismiss();
                        String img = "https://firebasestorage.googleapis.com/v0/b/cuchitasac-f8294.appspot.com/o/imgUsuario%2Fuser_sin_definir.png?alt=media&token=2cf45b1e-156e-45ac-85a1-fbe9091990aa";
                        siguiente(txtNombre.getText().toString(), txtApellido.getText().toString(), txtNickName.getText().toString(), txtDni.getText().toString(), img, seleccion);
                    }
                }
            }
        });

        btnImagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                abrirGaleria();
            }
        });
    }

    private void Iniciar(){
        btnSiguiente = findViewById(R.id.btnContinuar);
        txtNombre = findViewById(R.id.inputNombreUsuario);
        txtApellido = findViewById(R.id.inputApellidoUsuario);
        txtNickName = findViewById(R.id.inputNickUsuario);
        txtDni = findViewById(R.id.inputDni);
        rbtnVaron = findViewById(R.id.rbVaron);
        rbtnMujer = findViewById(R.id.rbMujer);
        rbtnNoEspecificar = findViewById(R.id.rbNoEspecificar);
        btnImagen = findViewById(R.id.circleImgUsuario);
        mProgressDialog = new ProgressDialog(this);

        mAuth = FirebaseAuth.getInstance();

        idUsuario = mAuth.getCurrentUser().getUid();
    }

    private void siguiente(String nombre, String apellido, String nickname, String dni, String url, String seleccion){
        validarSeleccion();

        Intent intent = new Intent(Registrar.this, registrarDni.class);
        intent.putExtra("nombre", nombre);
        intent.putExtra("apellido", apellido);
        intent.putExtra("nickname", nickname);
        intent.putExtra("dni", dni);
        intent.putExtra("genero", seleccion);
        intent.putExtra("imgUsuario", url);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);

    }

    private void validarSeleccion(){
        if (rbtnVaron.isChecked()){
            seleccion = "Varón";
        }

        if (rbtnMujer.isChecked()){
            seleccion = "Mujer";
        }

        if (rbtnNoEspecificar.isChecked()){
            seleccion = "No Especificar";
        }
    }

    private void abrirGaleria(){
        Intent GaleriaIntent = new Intent(Intent.ACTION_GET_CONTENT);
        GaleriaIntent.setType("image/*");
        startActivityForResult(GaleriaIntent, GALLERY_REQUEST);
    }

    private void guardarImagen(String id, String nombre, String apellido, String nickname, String dni, String seleccion){
        byte[] imageByte = CompresorBitmapImage.getImage(this, mImageFile.getPath(), 500, 500);
        StorageReference storage = FirebaseStorage.getInstance().getReference().child("imgUsuario").child(id + ".jpg");
        UploadTask uploadTask = storage.putBytes(imageByte);
        uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()){
                    storage.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            String img = uri.toString();
                            mProgressDialog.dismiss();
                            siguiente(nombre, apellido, nickname, dni, img, seleccion);
                        }
                    });
                }else {
                    Toast.makeText(Registrar.this, "Hubo un error al subir la imagen", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == GALLERY_REQUEST && resultCode == RESULT_OK){
            try {
                mImageFile = FileUtilidades.from(this, data.getData());
                btnImagen.setImageBitmap(BitmapFactory.decodeFile(mImageFile.getAbsolutePath()));
            }catch (Exception e){
                Log.d("Error:", "Mensaje de error: " + e.getMessage());
            }
        }
    }

}