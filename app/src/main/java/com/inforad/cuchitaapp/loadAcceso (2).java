package com.inforad.cuchitaapp;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.firebase.ui.auth.AuthMethodPickerLayout;
import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Arrays;

import dmax.dialog.SpotsDialog;

public class loadAcceso extends AppCompatActivity {

    private static final int RC_SIGN_IN = 123;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference mDatabase;
    private AlertDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_load_acceso);

        mAuth = FirebaseAuth.getInstance();
        mDialog = new SpotsDialog.Builder().setContext(loadAcceso.this).setMessage("Espere la verificación").build();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

                FirebaseUser usuario = firebaseAuth.getCurrentUser();
                if (usuario!= null){

                    String id_registro = usuario.getUid();

/*
                    mDatabase.child("Users").child(id_registro).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            if (snapshot.exists()){
                                Toast.makeText(loadAcceso.this, "Registrado", Toast.LENGTH_LONG).show();
                                //Obtenemos los datos campo por campo

                                Intent intent = new Intent(loadAcceso.this, tipoUsuario.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }else {
                                Intent intent = new Intent(loadAcceso.this, Registrar.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {
                            Toast.makeText(loadAcceso.this, "Error: " + error.toString(), Toast.LENGTH_SHORT).show();
                        }
                    });
*/
                    mDatabase.child("Users").child(id_registro).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            if (snapshot.exists()){ //Verificamos si existe el nodo mencionado
                                Toast.makeText(loadAcceso.this, "Registrado", Toast.LENGTH_LONG).show();
                                //Obtenemos los datos campo por campo

                                Intent intent = new Intent(loadAcceso.this, tipoUsuario.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }else {
                                Intent intent = new Intent(loadAcceso.this, Registrar.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });



                }else {
                    AuthUI.IdpConfig facebook = new AuthUI.IdpConfig.FacebookBuilder().setPermissions(Arrays.asList("email")).build();
                    AuthUI.IdpConfig google = new AuthUI.IdpConfig.GoogleBuilder().build();
                    AuthMethodPickerLayout personalizarLogin = new AuthMethodPickerLayout.Builder(R.layout.personalizar_acceso).setEmailButtonId(R.id.btnCorreo).setFacebookButtonId(R.id.btnFacebook).setGoogleButtonId(R.id.btnGoogle).build();

                    startActivityForResult(AuthUI.getInstance().createSignInIntentBuilder().setIsSmartLockEnabled(false).setTosAndPrivacyPolicyUrls("http://cuchita.inforad.net.pe/", "http://inforad.net.pe/").setAvailableProviders(Arrays.asList(new AuthUI.IdpConfig.EmailBuilder().build(), facebook, google)).setTheme(R.style.Tema).setLogo(R.drawable.logo).setAuthMethodPickerLayout(personalizarLogin).build(), RC_SIGN_IN);
                }
            }
        };
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN){
            if (resultCode == RESULT_OK){
                Toast.makeText(this, "Bienvenido a Cuchita", Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(this, "Error al registrar", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAuthListener != null){
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }
}