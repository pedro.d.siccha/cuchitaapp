package com.inforad.cuchitaapp.services;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.channel.NotificacionHelper;
import com.inforad.cuchitaapp.receivers.AceptarReceiver;
import com.inforad.cuchitaapp.receivers.CancelarReceiver;

import java.util.Map;

public class MyfirebaseMessagingClient extends FirebaseMessagingService {

    private static final int NOTIFICACION_CODE = 100;

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
    }

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        RemoteMessage.Notification notification = remoteMessage.getNotification();
        Map<String, String> data = remoteMessage.getData();
        String titulo = data.get("titulo");
        String contenido = data.get("contenido");

        if (titulo != null){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                if (titulo.contains("Pedido Pendiente")){
                    String idUsuario = data.get("idUsuario");
                    String idProducto = data.get("idProducto");
                    showNotificationActions(titulo, contenido, idUsuario, idProducto);
                }else {
                    showNotificationApiOreo(titulo, contenido);
                }
            }else {
                if (titulo.contains("Pedido Pendiente")) {
                    String idUsuario = data.get("idUsuario");
                    String idProducto = data.get("idProducto");
                    showNotificationActions(titulo, contenido, idUsuario, idProducto);
                }else {
                    showNotification(titulo, contenido);
                }
            }
        }
    }

    private void showNotification(String titulo, String cuerpo) {
        PendingIntent intent = PendingIntent.getActivity(getBaseContext(), 0, new Intent(), PendingIntent.FLAG_ONE_SHOT);
        Uri sonido = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificacionHelper notificacionHelper = new NotificacionHelper(getBaseContext());
        NotificationCompat.Builder builder = notificacionHelper.getNotificationOlApi(titulo, cuerpo, intent, sonido);
        notificacionHelper.getManager().notify(1, builder.build());
    }

    private void showNotificationActions(String titulo, String cuerpo, String idUsuario, String idProducto) {

        //Aceptar
        Intent aceptarIntent = new Intent(this, AceptarReceiver.class);
        aceptarIntent.putExtra("idUsuario", idUsuario);
        aceptarIntent.putExtra("idProducto", idProducto);
        PendingIntent aceptarPendingIntent = PendingIntent.getBroadcast(this, NOTIFICACION_CODE, aceptarIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Action aceptarAction = new NotificationCompat.Action.Builder(R.mipmap.ic_launcher, "Aceptar", aceptarPendingIntent).build();

        //Cancelar

        Intent cancelarIntent = new Intent(this, CancelarReceiver.class);
        cancelarIntent.putExtra("idUsuario", idUsuario);
        cancelarIntent.putExtra("idProducto", idProducto);
        PendingIntent cancelarPendingIntent = PendingIntent.getBroadcast(this, NOTIFICACION_CODE, cancelarIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Action cancelarAction = new NotificationCompat.Action.Builder(R.mipmap.ic_launcher, "Cancelar", cancelarPendingIntent).build();



        //PendingIntent intent = PendingIntent.getActivity(getBaseContext(), 0, new Intent(), PendingIntent.FLAG_ONE_SHOT);
        Uri sonido = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificacionHelper notificacionHelper = new NotificacionHelper(getBaseContext());
        NotificationCompat.Builder builder = notificacionHelper.getNotificationOlApiActions(titulo, cuerpo, sonido, aceptarAction, cancelarAction);
        notificacionHelper.getManager().notify(2, builder.build());
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void showNotificationApiOreo(String titulo, String cuerpo) {
        PendingIntent intent = PendingIntent.getActivity(getBaseContext(), 0, new Intent(), PendingIntent.FLAG_ONE_SHOT);
        Uri sonido = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificacionHelper notificacionHelper = new NotificacionHelper(getBaseContext());
        Notification.Builder builder = notificacionHelper.getNotification(titulo, cuerpo, intent, sonido);
        notificacionHelper.getManager().notify(1, builder.build());
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void showNotificationApiOreoActions(String titulo, String cuerpo, String idUsuario, String idProducto) {
        Intent aceptarIntent = new Intent(this, AceptarReceiver.class);
        aceptarIntent.putExtra("idUsuario", idUsuario);
        aceptarIntent.putExtra("idProducto", idProducto);
        PendingIntent aceptarPendingIntent = PendingIntent.getBroadcast(this, NOTIFICACION_CODE, aceptarIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification.Action aceptarAction = new Notification.Action.Builder(R.mipmap.ic_launcher, "Aceptar", aceptarPendingIntent).build();

        Intent cancelarIntent = new Intent(this, CancelarReceiver.class);
        cancelarIntent.putExtra("idUsuario", idUsuario);
        cancelarIntent.putExtra("idProducto", idProducto);
        PendingIntent cancelarPendingIntent = PendingIntent.getBroadcast(this, NOTIFICACION_CODE, cancelarIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification.Action cancelarAction = new Notification.Action.Builder(R.mipmap.ic_launcher, "Cancelar", cancelarPendingIntent).build();

        //PendingIntent intent = PendingIntent.getActivity(getBaseContext(), 0, new Intent(), PendingIntent.FLAG_ONE_SHOT);
        Uri sonido = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificacionHelper notificacionHelper = new NotificacionHelper(getBaseContext());
        Notification.Builder builder = notificacionHelper.getNotificationActions(titulo, cuerpo, sonido, aceptarAction, cancelarAction);
        notificacionHelper.getManager().notify(2, builder.build());
    }
}
