package com.inforad.cuchitaapp.models;

public class Reclamo {
    String reclamo;
    String idUsuario;
    String idTienda;
    String estado;
    String respuesta;

    public Reclamo() {
    }

    public Reclamo(String reclamo, String idUsuario, String idTienda, String estado, String respuesta) {
        this.reclamo = reclamo;
        this.idUsuario = idUsuario;
        this.idTienda = idTienda;
        this.estado = estado;
        this.respuesta = respuesta;
    }

    public String getReclamo() {
        return reclamo;
    }

    public void setReclamo(String reclamo) {
        this.reclamo = reclamo;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getIdTienda() {
        return idTienda;
    }

    public void setIdTienda(String idTienda) {
        this.idTienda = idTienda;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }
}
