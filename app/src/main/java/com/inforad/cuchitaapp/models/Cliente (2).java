package com.inforad.cuchitaapp.models;

public class Cliente {
    String idCliente;
    String imagen;
    String alias;
    String dni;
    String telefono;

    public Cliente() {
    }

    public Cliente(String idCliente, String imagen, String alias, String dni, String telefono) {
        this.idCliente = idCliente;
        this.imagen = imagen;
        this.alias = alias;
        this.dni = dni;
        this.telefono = telefono;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
}
