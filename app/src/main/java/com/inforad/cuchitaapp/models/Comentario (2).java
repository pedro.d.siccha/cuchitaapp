package com.inforad.cuchitaapp.models;

public class Comentario {
    String id;
    String idNegocio;
    String idPedido;
    String idUsuario;
    String comentario;
    String valoracion;
    String estado;

    public Comentario() {
    }

    public Comentario(String id, String idNegocio, String idPedido, String idUsuario, String comentario, String valoracion, String estado) {
        this.id = id;
        this.idNegocio = idNegocio;
        this.idPedido = idPedido;
        this.idUsuario = idUsuario;
        this.comentario = comentario;
        this.valoracion = valoracion;
        this.estado = estado;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(String idNegocio) {
        this.idNegocio = idNegocio;
    }

    public String getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(String idPedido) {
        this.idPedido = idPedido;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getValoracion() {
        return valoracion;
    }

    public void setValoracion(String valoracion) {
        this.valoracion = valoracion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
