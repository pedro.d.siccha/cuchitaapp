package com.inforad.cuchitaapp.models;

public class Negocio {

    String id;
    String nombre;
    String descripcion;
    String imagen;
    String categoria;
    String plan;
    String idAdministrador;
    String canLike;
    String cantComentario;

    public Negocio() {
    }

    public Negocio(String id, String nombre, String descripcion, String imagen, String categoria, String plan, String idAdministrador, String canLike, String cantComentario) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.imagen = imagen;
        this.categoria = categoria;
        this.plan = plan;
        this.idAdministrador = idAdministrador;
        this.canLike = canLike;
        this.cantComentario = cantComentario;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public String getIdAdministrador() {
        return idAdministrador;
    }

    public void setIdAdministrador(String idAdministrador) {
        this.idAdministrador = idAdministrador;
    }

    public String getCanLike() {
        return canLike;
    }

    public void setCanLike(String canLike) {
        this.canLike = canLike;
    }

    public String getCantComentario() {
        return cantComentario;
    }

    public void setCantComentario(String cantComentario) {
        this.cantComentario = cantComentario;
    }
}
