package com.inforad.cuchitaapp.models;

public class PedidoPendiente {
    String id;
    String usuario_id;
    String tienda_id;
    String producto_id;
    String producto;
    String cantidad;
    String precio_total;
    String fecha_actual;
    String stock_actual;

    public PedidoPendiente() {
    }

    public PedidoPendiente(String id, String usuario_id, String tienda_id, String producto_id, String producto, String cantidad, String precio_total, String fecha_actual, String stock_actual) {
        this.id = id;
        this.usuario_id = usuario_id;
        this.tienda_id = tienda_id;
        this.producto_id = producto_id;
        this.producto = producto;
        this.cantidad = cantidad;
        this.precio_total = precio_total;
        this.fecha_actual = fecha_actual;
        this.stock_actual = stock_actual;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(String usuario_id) {
        this.usuario_id = usuario_id;
    }

    public String getTienda_id() {
        return tienda_id;
    }

    public void setTienda_id(String tienda_id) {
        this.tienda_id = tienda_id;
    }

    public String getProducto_id() {
        return producto_id;
    }

    public void setProducto_id(String producto_id) {
        this.producto_id = producto_id;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getPrecio_total() {
        return precio_total;
    }

    public void setPrecio_total(String precio_total) {
        this.precio_total = precio_total;
    }

    public String getFecha_actual() {
        return fecha_actual;
    }

    public void setFecha_actual(String fecha_actual) {
        this.fecha_actual = fecha_actual;
    }

    public String getStock_actual() {
        return stock_actual;
    }

    public void setStock_actual(String stock_actual) {
        this.stock_actual = stock_actual;
    }
}
