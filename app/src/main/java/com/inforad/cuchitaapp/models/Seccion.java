package com.inforad.cuchitaapp.models;

public class Seccion {
    String id;
    String idNegocio;
    String seccion;
    String imagen;
    String estado;

    public Seccion() {
    }

    public Seccion(String id, String idNegocio, String seccion, String imagen, String estado) {
        this.id = id;
        this.idNegocio = idNegocio;
        this.seccion = seccion;
        this.imagen = imagen;
        this.estado = estado;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(String idNegocio) {
        this.idNegocio = idNegocio;
    }

    public String getSeccion() {
        return seccion;
    }

    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return seccion;
    }
}
