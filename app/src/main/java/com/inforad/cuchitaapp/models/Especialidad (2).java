package com.inforad.cuchitaapp.models;

public class Especialidad {

    String id;
    String nombre;
    String caracteristica;
    String palabraClave;

    public Especialidad() {
    }

    public Especialidad(String id, String nombre, String caracteristica, String palabraClave) {
        this.id = id;
        this.nombre = nombre;
        this.caracteristica = caracteristica;
        this.palabraClave = palabraClave;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCaracteristica() {
        return caracteristica;
    }

    public void setCaracteristica(String caracteristica) {
        this.caracteristica = caracteristica;
    }

    public String getPalabraClave() {
        return palabraClave;
    }

    public void setPalabraClave(String palabraClave) {
        this.palabraClave = palabraClave;
    }
}
