package com.inforad.cuchitaapp.models;

public class ClientePedido {
    String id;
    String usuario_id;
    String tienda_id;
    String producto_id;
    String producto;
    String cantidad;
    String precio_total;
    String fecha_actual;
    String stock_actual;
    String time;
    String km;
    double direccionLat;
    double direccionLong;
    String estado;

    public ClientePedido() {
    }

    public ClientePedido(String id, String usuario_id, String tienda_id, String producto_id, String producto, String cantidad, String precio_total, String fecha_actual, String stock_actual, String time, String km, double direccionLat, double direccionLong, String estado) {
        this.id = id;
        this.usuario_id = usuario_id;
        this.tienda_id = tienda_id;
        this.producto_id = producto_id;
        this.producto = producto;
        this.cantidad = cantidad;
        this.precio_total = precio_total;
        this.fecha_actual = fecha_actual;
        this.stock_actual = stock_actual;
        this.time = time;
        this.km = km;
        this.direccionLat = direccionLat;
        this.direccionLong = direccionLong;
        this.estado = estado;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(String usuario_id) {
        this.usuario_id = usuario_id;
    }

    public String getTienda_id() {
        return tienda_id;
    }

    public void setTienda_id(String tienda_id) {
        this.tienda_id = tienda_id;
    }

    public String getProducto_id() {
        return producto_id;
    }

    public void setProducto_id(String producto_id) {
        this.producto_id = producto_id;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getPrecio_total() {
        return precio_total;
    }

    public void setPrecio_total(String precio_total) {
        this.precio_total = precio_total;
    }

    public String getFecha_actual() {
        return fecha_actual;
    }

    public void setFecha_actual(String fecha_actual) {
        this.fecha_actual = fecha_actual;
    }

    public String getStock_actual() {
        return stock_actual;
    }

    public void setStock_actual(String stock_actual) {
        this.stock_actual = stock_actual;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getKm() {
        return km;
    }

    public void setKm(String km) {
        this.km = km;
    }

    public double getDireccionLat() {
        return direccionLat;
    }

    public void setDireccionLat(double direccionLat) {
        this.direccionLat = direccionLat;
    }

    public double getDireccionLong() {
        return direccionLong;
    }

    public void setDireccionLong(double direccionLong) {
        this.direccionLong = direccionLong;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
