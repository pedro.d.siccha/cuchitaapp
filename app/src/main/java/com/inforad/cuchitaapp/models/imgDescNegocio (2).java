package com.inforad.cuchitaapp.models;

public class imgDescNegocio {

    String id;
    String nombreImagen;
    String url;
    String ordenImg;

    public imgDescNegocio() {
    }

    public imgDescNegocio(String id, String nombreImagen, String url, String ordenImg) {
        this.id = id;
        this.nombreImagen = nombreImagen;
        this.url = url;
        this.ordenImg = ordenImg;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombreImagen() {
        return nombreImagen;
    }

    public void setNombreImagen(String nombreImagen) {
        this.nombreImagen = nombreImagen;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getOrdenImg() {
        return ordenImg;
    }

    public void setOrdenImg(String ordenImg) {
        this.ordenImg = ordenImg;
    }
}
