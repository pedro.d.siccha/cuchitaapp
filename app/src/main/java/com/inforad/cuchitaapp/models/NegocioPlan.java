package com.inforad.cuchitaapp.models;

public class NegocioPlan {
    String id;
    String fecInicio;
    String fecPago;
    String estado;

    public NegocioPlan() {
    }

    public NegocioPlan(String id, String fecInicio, String fecPago, String estado) {
        this.id = id;
        this.fecInicio = fecInicio;
        this.fecPago = fecPago;
        this.estado = estado;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFecInicio() {
        return fecInicio;
    }

    public void setFecInicio(String fecInicio) {
        this.fecInicio = fecInicio;
    }

    public String getFecPago() {
        return fecPago;
    }

    public void setFecPago(String fecPago) {
        this.fecPago = fecPago;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
