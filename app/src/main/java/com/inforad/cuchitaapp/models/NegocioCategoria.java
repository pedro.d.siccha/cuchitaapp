package com.inforad.cuchitaapp.models;

public class NegocioCategoria {
    String id;
    String ico;

    public NegocioCategoria() {
    }

    public NegocioCategoria(String id, String negocia_id, String ico) {
        this.id = id;
        this.ico = ico;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIco() {
        return ico;
    }

    public void setIco(String ico) {
        this.ico = ico;
    }
}
