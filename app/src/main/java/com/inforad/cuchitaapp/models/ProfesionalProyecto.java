package com.inforad.cuchitaapp.models;

public class ProfesionalProyecto {

    String id;
    String Profesional_id;
    String nombreProyecto;
    String tiempoProyecto;
    String fechaInicio;
    String fechaFin;
    String Hora;
    String descripcion;
    String empresa;
    String numRegEmpresa;
    String fotoCertificado;

    public ProfesionalProyecto() {
    }

    public ProfesionalProyecto(String id, String profesional_id, String nombreProyecto, String tiempoProyecto, String fechaInicio, String fechaFin, String hora, String descripcion, String empresa, String numRegEmpresa, String fotoCertificado) {
        this.id = id;
        Profesional_id = profesional_id;
        this.nombreProyecto = nombreProyecto;
        this.tiempoProyecto = tiempoProyecto;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        Hora = hora;
        this.descripcion = descripcion;
        this.empresa = empresa;
        this.numRegEmpresa = numRegEmpresa;
        this.fotoCertificado = fotoCertificado;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProfesional_id() {
        return Profesional_id;
    }

    public void setProfesional_id(String profesional_id) {
        Profesional_id = profesional_id;
    }

    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }

    public String getTiempoProyecto() {
        return tiempoProyecto;
    }

    public void setTiempoProyecto(String tiempoProyecto) {
        this.tiempoProyecto = tiempoProyecto;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getHora() {
        return Hora;
    }

    public void setHora(String hora) {
        Hora = hora;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getNumRegEmpresa() {
        return numRegEmpresa;
    }

    public void setNumRegEmpresa(String numRegEmpresa) {
        this.numRegEmpresa = numRegEmpresa;
    }

    public String getFotoCertificado() {
        return fotoCertificado;
    }

    public void setFotoCertificado(String fotoCertificado) {
        this.fotoCertificado = fotoCertificado;
    }
}
