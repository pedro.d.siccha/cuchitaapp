package com.inforad.cuchitaapp.models;

public class ListaCompra {

    String id;
    String usuario_id;
    String fecha_actual;
    String time;
    String km;
    String descpedido;
    String idTienda;
    double precio;
    double direccionLat;
    double direccionLong;
    String estado;

    public ListaCompra() {
    }

    public ListaCompra(String id, String usuario_id, String fecha_actual, String time, String km, String descpedido, String idTienda, double precio, double direccionLat, double direccionLong, String estado) {
        this.id = id;
        this.usuario_id = usuario_id;
        this.fecha_actual = fecha_actual;
        this.time = time;
        this.km = km;
        this.descpedido = descpedido;
        this.idTienda = idTienda;
        this.precio = precio;
        this.direccionLat = direccionLat;
        this.direccionLong = direccionLong;
        this.estado = estado;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(String usuario_id) {
        this.usuario_id = usuario_id;
    }

    public String getFecha_actual() {
        return fecha_actual;
    }

    public void setFecha_actual(String fecha_actual) {
        this.fecha_actual = fecha_actual;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getKm() {
        return km;
    }

    public void setKm(String km) {
        this.km = km;
    }

    public String getDescpedido() {
        return descpedido;
    }

    public void setDescpedido(String descpedido) {
        this.descpedido = descpedido;
    }

    public String getIdTienda() {
        return idTienda;
    }

    public void setIdTienda(String idTienda) {
        this.idTienda = idTienda;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public double getDireccionLat() {
        return direccionLat;
    }

    public void setDireccionLat(double direccionLat) {
        this.direccionLat = direccionLat;
    }

    public double getDireccionLong() {
        return direccionLong;
    }

    public void setDireccionLong(double direccionLong) {
        this.direccionLong = direccionLong;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

}
