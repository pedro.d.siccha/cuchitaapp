package com.inforad.cuchitaapp.models;

public class FotoProyecto {

    String id;
    String Proyecto_id;
    String url;
    String descricpion;

    public FotoProyecto() {
    }

    public FotoProyecto(String id, String proyecto_id, String url, String descricpion) {
        this.id = id;
        Proyecto_id = proyecto_id;
        this.url = url;
        this.descricpion = descricpion;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProyecto_id() {
        return Proyecto_id;
    }

    public void setProyecto_id(String proyecto_id) {
        Proyecto_id = proyecto_id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescricpion() {
        return descricpion;
    }

    public void setDescricpion(String descricpion) {
        this.descricpion = descricpion;
    }
}
