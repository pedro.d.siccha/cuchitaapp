package com.inforad.cuchitaapp.models;

public class Cupones {
    String codigo;
    String estado;
    String porcentaje;
    String idProducto;
    String idNegocio;
    double precioAnterior;
    double nuevoPrecio;

    public Cupones() {
    }

    public Cupones(String codigo, String estado, String porcentaje, String idProducto, String idNegocio, double precioAnterior, double nuevoPrecio) {
        this.codigo = codigo;
        this.estado = estado;
        this.porcentaje = porcentaje;
        this.idProducto = idProducto;
        this.idNegocio = idNegocio;
        this.precioAnterior = precioAnterior;
        this.nuevoPrecio = nuevoPrecio;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(String porcentaje) {
        this.porcentaje = porcentaje;
    }

    public String getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(String idProducto) {
        this.idProducto = idProducto;
    }

    public String getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(String idNegocio) {
        this.idNegocio = idNegocio;
    }

    public double getPrecioAnterior() {
        return precioAnterior;
    }

    public void setPrecioAnterior(double precioAnterior) {
        this.precioAnterior = precioAnterior;
    }

    public double getNuevoPrecio() {
        return nuevoPrecio;
    }

    public void setNuevoPrecio(double nuevoPrecio) {
        this.nuevoPrecio = nuevoPrecio;
    }
}
