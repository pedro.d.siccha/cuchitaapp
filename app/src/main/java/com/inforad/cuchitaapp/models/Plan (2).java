package com.inforad.cuchitaapp.models;

public class Plan {
    String plan;
    String nombreNegocio;
    String idNegocio;
    String idUsuario;
    String fecInicio;
    String fecFin;
    double costo;

    public Plan() {
    }

    public Plan(String plan, String nombreNegocio, String idNegocio, String idUsuario, String fecInicio, String fecFin, double costo) {
        this.plan = plan;
        this.nombreNegocio = nombreNegocio;
        this.idNegocio = idNegocio;
        this.idUsuario = idUsuario;
        this.fecInicio = fecInicio;
        this.fecFin = fecFin;
        this.costo = costo;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public String getNombreNegocio() {
        return nombreNegocio;
    }

    public void setNombreNegocio(String nombreNegocio) {
        this.nombreNegocio = nombreNegocio;
    }

    public String getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(String idNegocio) {
        this.idNegocio = idNegocio;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getFecInicio() {
        return fecInicio;
    }

    public void setFecInicio(String fecInicio) {
        this.fecInicio = fecInicio;
    }

    public String getFecFin() {
        return fecFin;
    }

    public void setFecFin(String fecFin) {
        this.fecFin = fecFin;
    }

    public double getCosto() {
        return costo;
    }

    public void setCosto(double costo) {
        this.costo = costo;
    }
}
