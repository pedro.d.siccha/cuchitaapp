package com.inforad.cuchitaapp.models;

public class NegocioUsuario {
    String idTienda;
    String tipoUsuario;
    String idUsuario;

    public NegocioUsuario() {
    }

    public NegocioUsuario(String idTienda, String tipoUsuario, String idUsuario) {
        this.idTienda = idTienda;
        this.tipoUsuario = tipoUsuario;
        this.idUsuario = idUsuario;
    }

    public String getIdTienda() {
        return idTienda;
    }

    public void setIdTienda(String idTienda) {
        this.idTienda = idTienda;
    }

    public String getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(String tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }
}
