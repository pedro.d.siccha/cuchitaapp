package com.inforad.cuchitaapp.models;

public class Direccion {

    String id;
    String direccion;
    String ciudad;
    String pais;
    String estado;
    String nivel;
    double dirLat;
    double dirLong;

    public Direccion() {
    }

    public Direccion(String id, String direccion, String ciudad, String pais, String estado, String nivel, double dirLat, double dirLong) {
        this.id = id;
        this.direccion = direccion;
        this.ciudad = ciudad;
        this.pais = pais;
        this.estado = estado;
        this.nivel = nivel;
        this.dirLat = dirLat;
        this.dirLong = dirLong;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public double getDirLat() {
        return dirLat;
    }

    public void setDirLat(double dirLat) {
        this.dirLat = dirLat;
    }

    public double getDirLong() {
        return dirLong;
    }

    public void setDirLong(double dirLong) {
        this.dirLong = dirLong;
    }
}
