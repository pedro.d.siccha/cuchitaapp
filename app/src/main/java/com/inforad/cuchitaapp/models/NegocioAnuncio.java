package com.inforad.cuchitaapp.models;

public class NegocioAnuncio {
    String id;
    String titulo;
    String detalle;
    String fecInicio;
    String fecFin;
    String costo;
    String cartel;

    public NegocioAnuncio() {
    }

    public NegocioAnuncio(String id, String titulo, String detalle, String fecInicio, String fecFin, String costo, String cartel) {
        this.id = id;
        this.titulo = titulo;
        this.detalle = detalle;
        this.fecInicio = fecInicio;
        this.fecFin = fecFin;
        this.costo = costo;
        this.cartel = cartel;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getFecInicio() {
        return fecInicio;
    }

    public void setFecInicio(String fecInicio) {
        this.fecInicio = fecInicio;
    }

    public String getFecFin() {
        return fecFin;
    }

    public void setFecFin(String fecFin) {
        this.fecFin = fecFin;
    }

    public String getCosto() {
        return costo;
    }

    public void setCosto(String costo) {
        this.costo = costo;
    }

    public String getCartel() {
        return cartel;
    }

    public void setCartel(String cartel) {
        this.cartel = cartel;
    }
}
