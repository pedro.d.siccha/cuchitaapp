package com.inforad.cuchitaapp.models;

public class Producto {
    String id;
    String idTienda;
    String idSeccion;
    String idUsuario;
    String nombre;
    String detalle;
    String stock;
    String stockMinimo;
    String stockMaximo;
    String unidadMedida;
    String precioUnidad;
    String imgProducto;
    String tiempoEntrega;
    String estado;

    public Producto() {
    }

    public Producto(String id, String idTienda, String idSeccion, String idUsuario, String nombre, String detalle, String stock, String stockMinimo, String stockMaximo, String unidadMedida, String precioUnidad, String imgProducto, String tiempoEntrega, String estado) {
        this.id = id;
        this.idTienda = idTienda;
        this.idSeccion = idSeccion;
        this.idUsuario = idUsuario;
        this.nombre = nombre;
        this.detalle = detalle;
        this.stock = stock;
        this.stockMinimo = stockMinimo;
        this.stockMaximo = stockMaximo;
        this.unidadMedida = unidadMedida;
        this.precioUnidad = precioUnidad;
        this.imgProducto = imgProducto;
        this.tiempoEntrega = tiempoEntrega;
        this.estado = estado;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdTienda() {
        return idTienda;
    }

    public void setIdTienda(String idTienda) {
        this.idTienda = idTienda;
    }

    public String getIdSeccion() {
        return idSeccion;
    }

    public void setIdSeccion(String idSeccion) {
        this.idSeccion = idSeccion;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getStockMinimo() {
        return stockMinimo;
    }

    public void setStockMinimo(String stockMinimo) {
        this.stockMinimo = stockMinimo;
    }

    public String getStockMaximo() {
        return stockMaximo;
    }

    public void setStockMaximo(String stockMaximo) {
        this.stockMaximo = stockMaximo;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public String getPrecioUnidad() {
        return precioUnidad;
    }

    public void setPrecioUnidad(String precioUnidad) {
        this.precioUnidad = precioUnidad;
    }

    public String getImgProducto() {
        return imgProducto;
    }

    public void setImgProducto(String imgProducto) {
        this.imgProducto = imgProducto;
    }

    public String getTiempoEntrega() {
        return tiempoEntrega;
    }

    public void setTiempoEntrega(String tiempoEntrega) {
        this.tiempoEntrega = tiempoEntrega;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
