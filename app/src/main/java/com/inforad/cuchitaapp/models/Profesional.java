package com.inforad.cuchitaapp.models;

public class Profesional {

    String id;
    String usuario_id;
    String foto;
    String Nombre;
    String Apellido;
    String Dni;
    int Edad;
    String ruc;
    String regProfecional;
    double direccionLat;
    double direccionLong;
    String especialidad_id;
    String especialidad;
    String foto_id;

    public Profesional() {
    }

    public Profesional(String id, String usuario_id, String foto, String nombre, String apellido, String dni, int edad, String ruc, String regProfecional, double direccionLat, double direccionLong, String especialidad_id, String especialidad, String foto_id) {
        this.id = id;
        this.usuario_id = usuario_id;
        this.foto = foto;
        Nombre = nombre;
        Apellido = apellido;
        Dni = dni;
        Edad = edad;
        this.ruc = ruc;
        this.regProfecional = regProfecional;
        this.direccionLat = direccionLat;
        this.direccionLong = direccionLong;
        this.especialidad_id = especialidad_id;
        this.especialidad = especialidad;
        this.foto_id = foto_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(String usuario_id) {
        this.usuario_id = usuario_id;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String apellido) {
        Apellido = apellido;
    }

    public String getDni() {
        return Dni;
    }

    public void setDni(String dni) {
        Dni = dni;
    }

    public int getEdad() {
        return Edad;
    }

    public void setEdad(int edad) {
        Edad = edad;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getRegProfecional() {
        return regProfecional;
    }

    public void setRegProfecional(String regProfecional) {
        this.regProfecional = regProfecional;
    }

    public double getDireccionLat() {
        return direccionLat;
    }

    public void setDireccionLat(double direccionLat) {
        this.direccionLat = direccionLat;
    }

    public double getDireccionLong() {
        return direccionLong;
    }

    public void setDireccionLong(double direccionLong) {
        this.direccionLong = direccionLong;
    }

    public String getEspecialidad_id() {
        return especialidad_id;
    }

    public void setEspecialidad_id(String especialidad_id) {
        this.especialidad_id = especialidad_id;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public String getFoto_id() {
        return foto_id;
    }

    public void setFoto_id(String foto_id) {
        this.foto_id = foto_id;
    }
}
