package com.inforad.cuchitaapp.models;

public class tipoUsuarioModel {

    String id;
    String idUsuario;

    public tipoUsuarioModel() {
    }

    public tipoUsuarioModel(String id, String idUsuario) {
        this.id = id;
        this.idUsuario = idUsuario;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }
}
