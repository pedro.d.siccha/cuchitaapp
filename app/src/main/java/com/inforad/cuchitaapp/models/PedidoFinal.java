package com.inforad.cuchitaapp.models;

public class PedidoFinal {
    String cliente;
    String idMoso;
    String idCliente;
    String idTienda;
    String idRepartidor;
    double precio;
    String tiempoEntrega;
    String estado;

    public PedidoFinal() {
    }

    public PedidoFinal(String cliente, String idMoso, String idCliente, String idTienda, String idRepartidor, double precio, String tiempoEntrega, String estado) {
        this.cliente = cliente;
        this.idMoso = idMoso;
        this.idCliente = idCliente;
        this.idTienda = idTienda;
        this.idRepartidor = idRepartidor;
        this.precio = precio;
        this.tiempoEntrega = tiempoEntrega;
        this.estado = estado;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getIdMoso() {
        return idMoso;
    }

    public void setIdMoso(String idMoso) {
        this.idMoso = idMoso;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getIdTienda() {
        return idTienda;
    }

    public void setIdTienda(String idTienda) {
        this.idTienda = idTienda;
    }

    public String getIdRepartidor() {
        return idRepartidor;
    }

    public void setIdRepartidor(String idRepartidor) {
        this.idRepartidor = idRepartidor;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getTiempoEntrega() {
        return tiempoEntrega;
    }

    public void setTiempoEntrega(String tiempoEntrega) {
        this.tiempoEntrega = tiempoEntrega;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
