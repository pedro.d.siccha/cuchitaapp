package com.inforad.cuchitaapp.models;

public class Pedido {
    String idTienda;
    String tienda;
    String imgTienda;
    String estado;
    String idUsuario;
    String fecPedido;
    String fecEntrega;
    String repartidor;
    String dniRepartidor;
    double precioTotal;
    double precioDelivery;

    public Pedido() {
    }

    public Pedido(String idTienda, String tienda, String imgTienda, String estado, String idUsuario, String fecPedido, String fecEntrega, String repartidor, String dniRepartidor, double precioTotal, double precioDelivery) {
        this.idTienda = idTienda;
        this.tienda = tienda;
        this.imgTienda = imgTienda;
        this.estado = estado;
        this.idUsuario = idUsuario;
        this.fecPedido = fecPedido;
        this.fecEntrega = fecEntrega;
        this.repartidor = repartidor;
        this.dniRepartidor = dniRepartidor;
        this.precioTotal = precioTotal;
        this.precioDelivery = precioDelivery;
    }

    public String getIdTienda() {
        return idTienda;
    }

    public void setIdTienda(String idTienda) {
        this.idTienda = idTienda;
    }

    public String getTienda() {
        return tienda;
    }

    public void setTienda(String tienda) {
        this.tienda = tienda;
    }

    public String getImgTienda() {
        return imgTienda;
    }

    public void setImgTienda(String imgTienda) {
        this.imgTienda = imgTienda;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getFecPedido() {
        return fecPedido;
    }

    public void setFecPedido(String fecPedido) {
        this.fecPedido = fecPedido;
    }

    public String getFecEntrega() {
        return fecEntrega;
    }

    public void setFecEntrega(String fecEntrega) {
        this.fecEntrega = fecEntrega;
    }

    public String getRepartidor() {
        return repartidor;
    }

    public void setRepartidor(String repartidor) {
        this.repartidor = repartidor;
    }

    public String getDniRepartidor() {
        return dniRepartidor;
    }

    public void setDniRepartidor(String dniRepartidor) {
        this.dniRepartidor = dniRepartidor;
    }

    public double getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(double precioTotal) {
        this.precioTotal = precioTotal;
    }

    public double getPrecioDelivery() {
        return precioDelivery;
    }

    public void setPrecioDelivery(double precioDelivery) {
        this.precioDelivery = precioDelivery;
    }
}
