package com.inforad.cuchitaapp.models;

public class Contacto {

    String idPricipal;
    String idContacto;

    public Contacto() {
    }

    public Contacto(String idPricipal, String idContacto) {
        this.idPricipal = idPricipal;
        this.idContacto = idContacto;
    }

    public String getIdPricipal() {
        return idPricipal;
    }

    public void setIdPricipal(String idPricipal) {
        this.idPricipal = idPricipal;
    }

    public String getIdContacto() {
        return idContacto;
    }

    public void setIdContacto(String idContacto) {
        this.idContacto = idContacto;
    }
}
