package com.inforad.cuchitaapp.models;

public class SeccionProducto {
    String idSeccion;
    String idNegocio;
    String idUsuario;
    String idProducto;

    public SeccionProducto() {
    }

    public SeccionProducto(String idSeccion, String idNegocio, String idUsuario, String idProducto) {
        this.idSeccion = idSeccion;
        this.idNegocio = idNegocio;
        this.idUsuario = idUsuario;
        this.idProducto = idProducto;
    }

    public String getIdSeccion() {
        return idSeccion;
    }

    public void setIdSeccion(String idSeccion) {
        this.idSeccion = idSeccion;
    }

    public String getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(String idNegocio) {
        this.idNegocio = idNegocio;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(String idProducto) {
        this.idProducto = idProducto;
    }
}
