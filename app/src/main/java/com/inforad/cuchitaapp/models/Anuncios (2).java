package com.inforad.cuchitaapp.models;

public class Anuncios {
    String id;
    String titulo;
    String descripcion;
    String FecInicio;
    String FecFin;
    String TiempoPublicacion;
    String estado;
    String imagen;
    String idTienda;
    String idUsuario;

    public Anuncios() {
    }

    public Anuncios(String id, String titulo, String descripcion, String fecInicio, String fecFin, String tiempoPublicacion, String estado, String imagen, String idTienda, String idUsuario) {
        this.id = id;
        this.titulo = titulo;
        this.descripcion = descripcion;
        FecInicio = fecInicio;
        FecFin = fecFin;
        TiempoPublicacion = tiempoPublicacion;
        this.estado = estado;
        this.imagen = imagen;
        this.idTienda = idTienda;
        this.idUsuario = idUsuario;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFecInicio() {
        return FecInicio;
    }

    public void setFecInicio(String fecInicio) {
        FecInicio = fecInicio;
    }

    public String getFecFin() {
        return FecFin;
    }

    public void setFecFin(String fecFin) {
        FecFin = fecFin;
    }

    public String getTiempoPublicacion() {
        return TiempoPublicacion;
    }

    public void setTiempoPublicacion(String tiempoPublicacion) {
        TiempoPublicacion = tiempoPublicacion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getIdTienda() {
        return idTienda;
    }

    public void setIdTienda(String idTienda) {
        this.idTienda = idTienda;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }
}
