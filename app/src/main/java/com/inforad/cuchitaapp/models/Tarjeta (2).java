package com.inforad.cuchitaapp.models;

public class Tarjeta {

    String id;
    String numero;
    String codigo;
    String fecVencimiento;
    String banco;
    String fecRegistro;
    String nivel;
    String estado;

    public Tarjeta() {
    }

    public Tarjeta(String id, String numero, String codigo, String fecVencimiento, String banco, String fecRegistro, String nivel, String estado) {
        this.id = id;
        this.numero = numero;
        this.codigo = codigo;
        this.fecVencimiento = fecVencimiento;
        this.banco = banco;
        this.fecRegistro = fecRegistro;
        this.nivel = nivel;
        this.estado = estado;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getFecVencimiento() {
        return fecVencimiento;
    }

    public void setFecVencimiento(String fecVencimiento) {
        this.fecVencimiento = fecVencimiento;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getFecRegistro() {
        return fecRegistro;
    }

    public void setFecRegistro(String fecRegistro) {
        this.fecRegistro = fecRegistro;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
