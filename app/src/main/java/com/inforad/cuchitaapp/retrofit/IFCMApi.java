package com.inforad.cuchitaapp.retrofit;

import com.inforad.cuchitaapp.models.FCMBody;
import com.inforad.cuchitaapp.models.FCMResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface IFCMApi {

    @Headers({
            "Content-Type:application/json",
            "Authorization:key=AAAALECs3mE:APA91bH_hlNFDr0EHD1HvQLghg7ZKP2EMjJ6hbCktcpzleOzU6lbDlhSftueLrB9Shz5lVuinS4sWMtjiJcOQHtOOPnbDABmPMcmDtH87O_6txcLj3jNNAU4lvmjx-nYWNua-yESjmBY"
    })
    @POST("fcm/send")
    Call<FCMResponse> send(@Body FCMBody body);

}
