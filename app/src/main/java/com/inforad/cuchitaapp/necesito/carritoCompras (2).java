package com.inforad.cuchitaapp.necesito;

import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.adapters.pedidoCarritoPagoAdaptador;
import com.inforad.cuchitaapp.models.PedidoFinal;
import com.inforad.cuchitaapp.provider.PedidoProvider;

public class carritoCompras extends AppCompatActivity {

    private String idUsuario, idTienda, metodoPago, precioBoton;
    private TextView txtMonto, txtObservacion, txtNombre, txtFecPedido, txtFecEntrega;
    private LinearLayout verDescripcion, verMonto;
    private PedidoProvider mPedidoProvider;
    private ImageView imgTienda;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private pedidoCarritoPagoAdaptador mPedidoPago;
    private RecyclerView recyclerListaPedidoPago;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_carrito_compras);
        Iniciar();
    }

    private void Iniciar(){
        txtMonto = findViewById(R.id.txtNomNegocio);
        mPedidoProvider = new PedidoProvider();
        imgTienda = findViewById(R.id.imgTienda);
        txtNombre = findViewById(R.id.tvTienda);
        txtFecPedido = findViewById(R.id.tvFecPedido);
        txtFecEntrega = findViewById(R.id.tvFecEntregas);
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        recyclerListaPedidoPago = findViewById(R.id.rerecyclerListaPedidoPago);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerListaPedidoPago.setLayoutManager(linearLayoutManager);

    }

    @Override
    protected void onStart() {
        super.onStart();
        Query query = mDatabase.child("Pedido").orderByChild("idCliente").equalTo(idUsuario);
        FirebaseRecyclerOptions<PedidoFinal> options = new FirebaseRecyclerOptions.Builder<PedidoFinal>().setQuery(query, PedidoFinal.class).build();
        mPedidoPago = new pedidoCarritoPagoAdaptador(options, carritoCompras.this);
        recyclerListaPedidoPago.setAdapter(mPedidoPago);
        mPedidoPago.startListening();
    }
}