package com.inforad.cuchitaapp.necesito;

import android.os.Bundle;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.adapters.busquedaTiendaAdaptador;
import com.inforad.cuchitaapp.models.Producto;

import java.util.ArrayList;

public class buscarTienda extends AppCompatActivity {

    private SearchView buscador;
    private RecyclerView listaTiendas;
    private DatabaseReference mDatabase;
    private busquedaTiendaAdaptador adaptadorBusqueda;
    private ArrayList<Producto> list;
    private LinearLayoutManager lm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_buscar_tienda);
        Iniciar();

        buscador.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                filtrar(s);
                return true;
            }
        });
    }

    private void filtrar(String palabra) {

        ArrayList<Producto> productos = new ArrayList<>();
        for (Producto obj: list){
            if (obj.getNombre().toLowerCase().contains(palabra.toLowerCase())){
                productos.add(obj);
            }
        }
        busquedaTiendaAdaptador adaptadorBusqueda = new busquedaTiendaAdaptador(productos, buscarTienda.this);
        listaTiendas.setAdapter(adaptadorBusqueda);

    }

    private void Iniciar() {
        buscador = findViewById(R.id.svBuscador);
        listaTiendas = findViewById(R.id.rvListaTienda);
        lm = new LinearLayoutManager(this);
        listaTiendas.setLayoutManager(lm);
        list = new ArrayList<>();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Producto");
        adaptadorBusqueda = new busquedaTiendaAdaptador(list, buscarTienda.this);
        listaTiendas.setAdapter(adaptadorBusqueda);

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    for (DataSnapshot snapshot1 : snapshot.getChildren()){
                        Producto pr = snapshot1.getValue(Producto.class);
                        list.add(pr);
                    }
                    adaptadorBusqueda.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        /*
        Query query = mDatabase.child("Producto");
        FirebaseRecyclerOptions<Producto> options = new FirebaseRecyclerOptions.Builder<Producto>().setQuery(query, Producto.class).build();
        adaptadorBusqueda = new busquedaTiendaAdaptador(options, buscarTienda.this);
        listaTiendas.setAdapter(adaptadorBusqueda);
        adaptadorBusqueda.startListening();

         */
    }
}