package com.inforad.cuchitaapp.necesito;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.FCMBody;
import com.inforad.cuchitaapp.models.FCMResponse;
import com.inforad.cuchitaapp.models.ListaCompra;
import com.inforad.cuchitaapp.provider.ListCompraProvider;
import com.inforad.cuchitaapp.provider.NotificationProvider;
import com.inforad.cuchitaapp.provider.TokenProvider;
import com.inforad.cuchitaapp.provider.UserProvider;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class enviarcompras extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private String user_id;
    private FirebaseUser user;
    private DatabaseReference mDatabase;
    private ListaCompra listaCompra;
    private ListCompraProvider mListCompraProvider;
    private UserProvider mUserProvider;
    private TextView txtNickName, txtDni;
    private EditText txtDetallePedido;
    private Button btnEnviarPedido;
    private TokenProvider mTokenProvider;
    private NotificationProvider mNotificationProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_enviarcompras);
        Iniciar();

        btnEnviarPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendNotification(user.getUid(), user.getUid(), txtDetallePedido.getText().toString(), "1");
                agregarLista(user.getUid(), user.getUid(), "03/08/2020", "0:00", "0", txtDetallePedido.getText().toString(), 0.00, 0.00, "ENVIADO");
            }
        });

    }

    private void Iniciar(){
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        mUserProvider = new UserProvider();
        txtNickName = findViewById(R.id.tvNickName);
        txtDni = findViewById(R.id.tvDNI);
        txtDetallePedido = findViewById(R.id.etAreaPedido);
        btnEnviarPedido = findViewById(R.id.btnEnviar);
        mTokenProvider = new TokenProvider();
        mNotificationProvider = new NotificationProvider();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        getNick();
    }

    private void getNick(){
        /*
        mUserProvider.obtenerDatos().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String nickname = snapshot.child("nickname").getValue().toString();
                    String dni = snapshot.child("dni").getValue().toString();
                    txtNickName.setText(nickname);
                    txtDni.setText(dni);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
         */
    }

    private void agregarLista(String id, String usuario_id, String fecha_actual, String time, String km, String descpedido, double direccionLat, double direccionLong, String estado){
        ListaCompra lista = new ListaCompra();
        lista.setId(id);
        lista.setUsuario_id(usuario_id);
        lista.setFecha_actual(fecha_actual);
        lista.setTime(time);
        lista.setKm(km);
        lista.setDescpedido(descpedido);
        lista.setDireccionLat(direccionLat);
        lista.setDireccionLong(direccionLong);
        lista.setEstado(estado);

        mDatabase.child("Cliente_ListaCompra").child(user.getUid()).setValue(lista).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(enviarcompras.this, "Pedido Realizado Correctamente", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(enviarcompras.this, categotia.class);
                startActivity(intent);

            }
        });

    }

    private void sendNotification(String tienda_id, String producto_id, String producto, String cantidad) {
        mTokenProvider.getToken(tienda_id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String token = snapshot.child("token").getValue().toString();
                    Map<String, String> map = new HashMap<>();
                    map.put("titulo", "Pedido Pendientes");
                    map.put("contenido", "Tiene un pedido de " + cantidad + " " + producto);
                    map.put("idUsuario", user.getUid());
                    map.put("idProducto", producto_id);
                    FCMBody fcmBody = new FCMBody(token, "high", map);
                    mNotificationProvider.sendNotification(fcmBody).enqueue(new Callback<FCMResponse>() {
                        @Override
                        public void onResponse(Call<FCMResponse> call, Response<FCMResponse> response) {
                            if (response.body() != null){
                                if (response.body().getSuccess() == 1){
                                   // verificarEstadoClientePedido(producto_id);
                                }else {
                                    Toast.makeText(enviarcompras.this, "Error no se pudo enviar la notificación", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<FCMResponse> call, Throwable t) {
                            Log.d("Error", "Error 0001: " + t.getMessage());
                        }
                    });
                }else {
                    Toast.makeText(enviarcompras.this, "Error Existe TOken Asociado", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

}