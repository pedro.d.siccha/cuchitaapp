package com.inforad.cuchitaapp.necesito;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.adapters.detallePedidoAdaptador;
import com.inforad.cuchitaapp.adapters.pedidoAceptadoAdaptador;
import com.inforad.cuchitaapp.models.FCMBody;
import com.inforad.cuchitaapp.models.FCMResponse;
import com.inforad.cuchitaapp.models.PedidoProducto;
import com.inforad.cuchitaapp.provider.NotificationProvider;
import com.inforad.cuchitaapp.provider.PedidoProvider;
import com.inforad.cuchitaapp.provider.TokenProvider;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class pedido extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private String usuario_id, tienda_id;
    private DatabaseReference mDatabase;
    private RecyclerView recyclerListaPedidoAceptado;
    private pedidoAceptadoAdaptador mPedidoAceptadoAdaptador;
    private String idProducto;
    private Button btnConfirmar;
    private detallePedidoAdaptador mdetalleAdaptador;
    private Dialog mDialog;
    private PedidoProvider mPedidoProvider;
    private TokenProvider mTokenProvider;
    private NotificationProvider mNotificationProvider;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_pedido);
        Iniciar();

        btnConfirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendNotification(usuario_id, tienda_id, tienda_id, "1");
                mPedidoProvider.actualizarEstado(usuario_id, tienda_id, "PENDIENTE").addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Intent intent = new Intent(pedido.this, categotia.class);
                        startActivity(intent);
                    }
                });
            }
        });
    }

    private void Iniciar(){
        mAuth = FirebaseAuth.getInstance();
        usuario_id = mAuth.getCurrentUser().getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mPedidoProvider = new PedidoProvider();

        idProducto = getIntent().getStringExtra("idProducto");
        btnConfirmar = findViewById(R.id.btnConfirmarPedidos);
        obtenerDatosAnteriores();
        recyclerListaPedidoAceptado = findViewById(R.id.recyclerListaPedidoAceptado);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerListaPedidoAceptado.setLayoutManager(linearLayoutManager);

        mTokenProvider = new TokenProvider();
        mNotificationProvider = new NotificationProvider();

        mDialog = new Dialog(this);

    }

    public void mostrarTipoPago(View view){
        TextView txtCerrar;
        LinearLayout btnTarjeta, btnEfectivo, btnCuchita;

        txtCerrar = (TextView) mDialog.findViewById(R.id.tvCerrar);
        txtCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        btnTarjeta = (LinearLayout) mDialog.findViewById(R.id.llPagoTarjeta);
        btnTarjeta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(pedido.this, carritoCompras.class);
                intent.putExtra("idUsuario", usuario_id);
                intent.putExtra("idTienda", tienda_id);
                intent.putExtra("metodoPago", "TARJETA");
                startActivity(intent);
            }
        });
        btnEfectivo = (LinearLayout) mDialog.findViewById(R.id.llPagoEfectivo);
        btnEfectivo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(pedido.this, carritoCompras.class);
                intent.putExtra("idUsuario", usuario_id);
                intent.putExtra("idTienda", tienda_id);
                intent.putExtra("metodoPago", "EFECTIVO");
                startActivity(intent);
            }
        });
        btnCuchita = (LinearLayout) mDialog.findViewById(R.id.llPagoCuchita);
        btnCuchita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast toast = Toast.makeText(pedido.this, "Proximamente", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 50, 50);
                toast.getView().setBackgroundColor(Color.GREEN);
                toast.show();
            }
        });
        mDialog.show();
    }

    private void obtenerDatosAnteriores(){
        tienda_id = getIntent().getStringExtra("tienda_id");
    }


    private void sendNotification(String tienda_id, String producto_id, String producto, String cantidad) {
        mTokenProvider.getToken(tienda_id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String token = snapshot.child("token").getValue().toString();
                    Map<String, String> map = new HashMap<>();
                    map.put("titulo", "Pedido Pendientes");
                    map.put("contenido", "Tiene un pedido de " + cantidad + " " + producto);
                    map.put("idUsuario", usuario_id);
                    map.put("idProducto", producto_id);
                    FCMBody fcmBody = new FCMBody(token, "high", map);
                    mNotificationProvider.sendNotification(fcmBody).enqueue(new Callback<FCMResponse>() {
                        @Override
                        public void onResponse(Call<FCMResponse> call, Response<FCMResponse> response) {
                            if (response.body() != null){
                                if (response.body().getSuccess() == 1){

                                }else {
                                    Toast.makeText(pedido.this, "Error no se pudo enviar la notificación", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<FCMResponse> call, Throwable t) {
                            Log.d("Error", "Error 0001: " + t.getMessage());
                        }
                    });
                }else {
                    Toast.makeText(pedido.this, "Error Existe TOken Asociado", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }



    @Override
    protected void onStart() {
        super.onStart();
        Query queryAceptar = mDatabase.child("Pedido").child(usuario_id).child(tienda_id).child("PRODUCTO");
        FirebaseRecyclerOptions<PedidoProducto> options = new FirebaseRecyclerOptions.Builder<PedidoProducto>().setQuery(queryAceptar, PedidoProducto.class).build();
        mdetalleAdaptador = new detallePedidoAdaptador(options, pedido.this);
        recyclerListaPedidoAceptado.setAdapter(mdetalleAdaptador);
        mdetalleAdaptador.startListening();

    }
}