package com.inforad.cuchitaapp.necesito;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.adapters.tarjetaAdaptador;
import com.inforad.cuchitaapp.models.Tarjeta;
import com.inforad.cuchitaapp.provider.UserProvider;
import com.inforad.cuchitaapp.provider.tarjetaProvider;

public class billetera extends AppCompatActivity {

    private LinearLayout btnInicio, btnPedido, btnBilletera, btnAjustes, btnCarrito;
    private FirebaseAuth mAuth;
    private String idUsuario;
    private UserProvider mUserProvider;
    private tarjetaProvider mTarjetaProvider;
    private ImageView btnAgregar;
    private DatabaseReference mDatabase;
    private RecyclerView recyclerListaTarjetas;
    private tarjetaAdaptador adaptadorTarjeta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_billetera);
        Iniciar();

        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(billetera.this, agregarTarjeta.class);
                startActivity(intent);
            }
        });

        btnInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(billetera.this, categotia.class);
                startActivity(intent);
            }
        });

        btnPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(billetera.this, listapedido.class);
                startActivity(intent);
            }
        });

        btnBilletera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(billetera.this, billetera.class);
                startActivity(intent);
            }
        });

        btnAjustes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(billetera.this, ajustes.class);
                startActivity(intent);
            }
        });

        btnCarrito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    private void Iniciar() {
        btnAgregar = findViewById(R.id.ivAgregar);
        recyclerListaTarjetas = findViewById(R.id.rvListaTarjeta);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerListaTarjetas.setLayoutManager(linearLayoutManager);

        mAuth = FirebaseAuth.getInstance();
        mUserProvider = new UserProvider();
        mTarjetaProvider = new tarjetaProvider();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        idUsuario = mAuth.getCurrentUser().getUid();

        BarraHerramientas();
        obtenerDatos();
    }

    private void obtenerDatos() {

    }

    private void BarraHerramientas(){
        btnInicio = findViewById(R.id.llInicio);
        btnPedido = findViewById(R.id.llPedido);
        btnBilletera = findViewById(R.id.llBilletera);
        btnAjustes = findViewById(R.id.llAjustes);
        btnCarrito = findViewById(R.id.llCarritoCompras);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Query query = mDatabase.child("Tarjeta").child(idUsuario);
        FirebaseRecyclerOptions<Tarjeta> options = new FirebaseRecyclerOptions.Builder<Tarjeta>().setQuery(query, Tarjeta.class).build();
        adaptadorTarjeta = new tarjetaAdaptador(options, billetera.this);
        recyclerListaTarjetas.setAdapter(adaptadorTarjeta);
        adaptadorTarjeta.startListening();

    }
}