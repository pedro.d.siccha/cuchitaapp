package com.inforad.cuchitaapp.necesito;

import android.os.Bundle;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.adapters.anuncioTiendaAdaptador;
import com.inforad.cuchitaapp.models.Anuncios;
import com.inforad.cuchitaapp.provider.AnuncioProvider;
import com.inforad.cuchitaapp.provider.UserProvider;
import com.inforad.cuchitaapp.provider.negocioProvider;

public class misAnuncios extends AppCompatActivity {

    private RecyclerView lisaAnuncios;
    private FirebaseAuth mAuth;
    private String usuario_id, tienda_id, plan;
    private DatabaseReference mDatabase;
    private anuncioTiendaAdaptador adaptadorAnuncio;
    private AnuncioProvider mAnuncioProvider;
    private negocioProvider mNegocioProvider;
    private UserProvider mUserProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_mis_anuncios);
        Iniciar();
    }

    private void Iniciar() {
        lisaAnuncios = findViewById(R.id.recyclerAnuncios);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        lisaAnuncios.setLayoutManager(linearLayoutManager);
        mAuth = FirebaseAuth.getInstance();
        usuario_id = mAuth.getCurrentUser().getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAnuncioProvider = new AnuncioProvider();
        mNegocioProvider = new negocioProvider();
        mUserProvider = new UserProvider();

        verificar();
    }

    private void verificar() {
    }

    @Override
    protected void onStart() {
        super.onStart();
        Query query = mDatabase.child("Anuncios").orderByChild("idUsuario").equalTo(usuario_id);
        FirebaseRecyclerOptions<Anuncios> options = new FirebaseRecyclerOptions.Builder<Anuncios>().setQuery(query, Anuncios.class).build();
        adaptadorAnuncio = new anuncioTiendaAdaptador(options, misAnuncios.this);
        lisaAnuncios.setAdapter(adaptadorAnuncio);
        adaptadorAnuncio.startListening();
    }
}