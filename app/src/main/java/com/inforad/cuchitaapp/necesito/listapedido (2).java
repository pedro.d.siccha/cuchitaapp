package com.inforad.cuchitaapp.necesito;

import android.os.Bundle;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.adapters.pedidoAdaptador;
import com.inforad.cuchitaapp.adapters.pedidoFinalAdaptador;
import com.inforad.cuchitaapp.adapters.pedidoPendienteAdaptador;
import com.inforad.cuchitaapp.models.PedidoFinal;

public class listapedido extends AppCompatActivity {

    private RecyclerView recyclerListaPedidoPendiente;
    private RecyclerView recyclerListaPedidoFinal;
    private DatabaseReference mDatabase;
    private pedidoPendienteAdaptador adaptadorPedidoPendiente;
    private pedidoFinalAdaptador adaptadorPedidoFinal;
    private FirebaseAuth mAuth;
    private String usuario_id;
    private pedidoAdaptador adaptadorPedido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_listapedido);
        Iniciar();

    }

    private void Iniciar(){
        mAuth = FirebaseAuth.getInstance();
        usuario_id = mAuth.getCurrentUser().getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        recyclerListaPedidoPendiente = findViewById(R.id.recyclerListaPedidoPendiente);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerListaPedidoPendiente.setLayoutManager(linearLayoutManager);

        recyclerListaPedidoFinal = findViewById(R.id.recyclerListaPedidoFinal);
        LinearLayoutManager linearLayoutManagerP = new LinearLayoutManager(this);
        recyclerListaPedidoFinal.setLayoutManager(linearLayoutManagerP);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Query queryPendiente = mDatabase.child("Pedido").orderByChild("idCliente").equalTo(usuario_id);
        FirebaseRecyclerOptions<PedidoFinal> options = new FirebaseRecyclerOptions.Builder<PedidoFinal>().setQuery(queryPendiente, PedidoFinal.class).build();
        adaptadorPedido = new pedidoAdaptador(options, listapedido.this);
        recyclerListaPedidoPendiente.setAdapter(adaptadorPedido);
        adaptadorPedido.startListening();

        /*
        Query queryFinal = mDatabase.child("Historial").orderByChild("idCliente").equalTo(usuario_id);
        FirebaseRecyclerOptions<Pedido> options1 = new FirebaseRecyclerOptions.Builder<Pedido>().setQuery(queryFinal, Pedido.class).build();
        adaptadorPedido = new pedidoAdaptador(options1, listapedido.this);
        recyclerListaPedidoFinal.setAdapter(adaptadorPedido);
        adaptadorPedido.startListening();

         */
    }
}