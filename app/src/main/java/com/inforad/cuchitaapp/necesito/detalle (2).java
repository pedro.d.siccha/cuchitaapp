package com.inforad.cuchitaapp.necesito;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.adapters.imagenDescripcionAdaptador;
import com.inforad.cuchitaapp.models.imgDescNegocio;
import com.inforad.cuchitaapp.ofrezco.descripcionNegocio;
import com.inforad.cuchitaapp.provider.negocioProvider;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class detalle extends AppCompatActivity {

    private ImageView btnAtras;
    private TextView txtTitulo, txtDetalles;
    private DatabaseReference mDatabase;
    private RecyclerView recyclerViewImagenes;
    private imagenDescripcionAdaptador adaptadorImagen;
    private CircleImageView imgPerfil;
    private String idNegocio, tienda, img;
    private negocioProvider mNegocioProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);
        Iniciar();

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(detalle.this, detalleTienda.class);
                intent.putExtra("tienda_id", idNegocio);
                intent.putExtra("tienda", tienda);
                intent.putExtra("imagen", img);
                startActivity(intent);
            }
        });
    }

    private void Iniciar() {
        btnAtras = findViewById(R.id.ivAtras);
        txtTitulo = findViewById(R.id.tvTitulo);
        imgPerfil = findViewById(R.id.civPerfil);
        txtDetalles = findViewById(R.id.tvDetalles);
        recyclerViewImagenes = findViewById(R.id.rvFotos);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerViewImagenes.setLayoutManager(linearLayoutManager);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mNegocioProvider = new negocioProvider();

        obtenerDatos();
    }

    private void obtenerDatos() {
        idNegocio = getIntent().getStringExtra("idTienda");

        mNegocioProvider.getNegocio(idNegocio).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    tienda = snapshot.child("nombre").getValue().toString();
                    txtTitulo.setText(snapshot.child("nombre").getValue().toString());
                    txtDetalles.setText(snapshot.child("descripcion").getValue().toString());
                    img = snapshot.child("imagen").getValue().toString();
                    Picasso.with(detalle.this).load(img).into(imgPerfil);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Query query = mDatabase.child("Negocio_Imagen_Descripcion").child(idNegocio);
        FirebaseRecyclerOptions<imgDescNegocio> options = new FirebaseRecyclerOptions.Builder<imgDescNegocio>().setQuery(query, imgDescNegocio.class).build();
        adaptadorImagen = new imagenDescripcionAdaptador(options, detalle.this);
        recyclerViewImagenes.setAdapter(adaptadorImagen);
        adaptadorImagen.startListening();
    }
}