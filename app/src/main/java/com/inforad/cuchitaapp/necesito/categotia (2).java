package com.inforad.cuchitaapp.necesito;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.provider.direccionProvider;
import com.inforad.cuchitaapp.tipoUsuario;

public class categotia extends AppCompatActivity {

    private LinearLayout btnAnuncios, btnComida, btnMercado, btnListaCompras, btnMascota, btnPc, btnProfesionales, btnTecnico, btnFarnacia, btnFerreteria, btnModa, btnJuguete, btnMensaje;
    private LinearLayout btnInicio, btnPedido, btnBilletera, btnAjustes, btnCarrito;
    private TextView txtDireccion;
    private FirebaseAuth mAuth;
    private String idUsuario;
    private direccionProvider mDireccionProvider;
    private ImageView btnBuscar, btnDireccion;
    private Dialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_categotia);
        Iniciar();

        btnAnuncios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(categotia.this, anuncios.class);
                intent.putExtra("categoria","Anuncios");
                startActivity(intent);

            }
        });

        btnComida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(categotia.this, resultCategoria.class);
                intent.putExtra("categoria","Comida");
                startActivity(intent);
            }
        });

        btnMercado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(categotia.this, resultCategoria.class);
                intent.putExtra("categoria","Mercado");
                startActivity(intent);
            }
        });

        btnListaCompras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(categotia.this, enviarcompras.class);
                //intent.putExtra("categoria","ListaCompra");
                startActivity(intent);
            }
        });

        btnMascota.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(categotia.this, resultCategoria.class);
                intent.putExtra("categoria","Mascota");
                startActivity(intent);
            }
        });

        btnPc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(categotia.this, resultCategoria.class);
                intent.putExtra("categoria","Pc");
                startActivity(intent);
            }
        });
/*
        btnProfesionales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(categotia.this, listaprofesionales.class);
                intent.putExtra("categoria","Profesionales");
                startActivity(intent);
            }
        });

        btnTecnico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(categotia.this, listaprofesionales.class);
                intent.putExtra("categoria","Tecnico");
                startActivity(intent);
            }
        });

 */

        btnFarnacia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(categotia.this, resultCategoria.class);
                intent.putExtra("categoria","Farmacia");
                startActivity(intent);
            }
        });

        btnFerreteria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(categotia.this, resultCategoria.class);
                intent.putExtra("categoria","Ferreteria");
                startActivity(intent);
            }
        });

        btnModa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(categotia.this, resultCategoria.class);
                intent.putExtra("categoria","Moda");
                startActivity(intent);
            }
        });

        btnJuguete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(categotia.this, resultCategoria.class);
                intent.putExtra("categoria","Juguete");
                startActivity(intent);
            }
        });

        btnPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(categotia.this, listapedido.class);
                startActivity(intent);
            }
        });

        btnBilletera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(categotia.this, billetera.class);
                startActivity(intent);
            }
        });

        btnAjustes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(categotia.this, ajustes.class);
                startActivity(intent);
            }
        });

        btnCarrito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(categotia.this, carritoCompras.class);
                startActivity(intent);
            }
        });

        btnMensaje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(categotia.this, contacto.class);
                startActivity(intent);
            }
        });

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(categotia.this, buscarTienda.class);
                startActivity(intent);
            }
        });

        txtDireccion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mostrarDireccion(view);
            }
        });

        btnDireccion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mostrarDireccion(view);
            }
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == event.KEYCODE_BACK){
            Intent intent = new Intent(categotia.this, tipoUsuario.class);
            startActivity(intent);
        }
        return super.onKeyUp(keyCode, event);
    }

    private void Iniciar(){
        btnAnuncios = findViewById(R.id.llCatAnuncios);
        btnComida = findViewById(R.id.llCatComida);
        btnMercado = findViewById(R.id.llCatMercado);
        btnListaCompras = findViewById(R.id.llCatListaCompras);
        btnMascota = findViewById(R.id.llCatMascota);
        btnPc = findViewById(R.id.llCatPc);
        btnMensaje = findViewById(R.id.llMensaje);
        btnBuscar = findViewById(R.id.ivBuscar);
        btnDireccion = findViewById(R.id.ivDireccion);
/*
        btnProfesionales = findViewById(R.id.llCatProfesional);
        btnTecnico = findViewById(R.id.llCatTecnico);
 */
        btnFarnacia = findViewById(R.id.llCatFarmacia);
        btnFerreteria = findViewById(R.id.llCatFerreteria);
        btnModa = findViewById(R.id.llCatModa);
        btnJuguete = findViewById(R.id.llCatJuguetes);
        txtDireccion = findViewById(R.id.tvDireccion);

        mAuth = FirebaseAuth.getInstance();
        mDireccionProvider = new direccionProvider();

        idUsuario = mAuth.getCurrentUser().getUid();

        mDialog = new Dialog(categotia.this);
        mDialog.setContentView(R.layout.modal_direccion);

        cargarDatos();
        BarraHerramientas();
    }

    private void BarraHerramientas(){
        btnInicio = findViewById(R.id.llInicio);
        btnPedido = findViewById(R.id.llPedido);
        btnBilletera = findViewById(R.id.llBilletera);
        btnAjustes = findViewById(R.id.llAjustes);
        btnCarrito = findViewById(R.id.llCarritoCompras);
    }

    private void cargarDatos(){
        //Toast.makeText(this, idUsuario, Toast.LENGTH_SHORT).show();

        mDireccionProvider.obtenerDireccion(idUsuario).child("CASA").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    txtDireccion.setText(snapshot.child("ciudad").getValue().toString());
                }else {
                    Toast.makeText(categotia.this, "No hay direccion", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void mostrarDireccion(View view){
        TextView txtCerrar;
        LinearLayout btnCasa, btnOficina, btnTrabajo, btnMiUbicacion;

        txtCerrar = (TextView) mDialog.findViewById(R.id.tvCerrar);
        txtCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        btnCasa = (LinearLayout) mDialog.findViewById(R.id.llCasa);
        btnCasa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDireccionProvider.obtenerDireccionLista(idUsuario, "CASA").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (snapshot.exists()){
                            txtDireccion.setText(snapshot.child("ciudad").getValue().toString());
                            mDialog.dismiss();
                        }else {
                            Intent intent = new Intent(categotia.this, registrarDireccion.class);
                            intent.putExtra("idUsuario", idUsuario);
                            intent.putExtra("categoria", "CASA");
                            startActivity(intent);
                            mDialog.dismiss();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });

                mDialog.dismiss();
            }

        });
        btnOficina = (LinearLayout) mDialog.findViewById(R.id.llOficina);
        btnOficina.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDireccionProvider.obtenerDireccionLista(idUsuario, "OFICINA").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (snapshot.exists()){
                            txtDireccion.setText(snapshot.child("ciudad").getValue().toString());
                            mDialog.dismiss();
                        }else {
                            Intent intent = new Intent(categotia.this, registrarDireccion.class);
                            intent.putExtra("idUsuario", idUsuario);
                            intent.putExtra("categoria", "OFICINA");
                            startActivity(intent);
                            mDialog.dismiss();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
            }
        });
        btnTrabajo = (LinearLayout) mDialog.findViewById(R.id.llTrabajo);
        btnTrabajo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDireccionProvider.obtenerDireccionLista(idUsuario, "TRABAJO").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (snapshot.exists()){
                            txtDireccion.setText(snapshot.child("ciudad").getValue().toString());
                            mDialog.dismiss();
                        }else {
                            Intent intent = new Intent(categotia.this, registrarDireccion.class);
                            intent.putExtra("idUsuario", idUsuario);
                            intent.putExtra("categoria", "TRABAJO");
                            startActivity(intent);
                            mDialog.dismiss();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
            }
        });
        btnMiUbicacion = (LinearLayout) mDialog.findViewById(R.id.llMiUbicacion);
        btnMiUbicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDireccionProvider.obtenerDireccionLista(idUsuario, "MI UBICACION").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (snapshot.exists()){
                            txtDireccion.setText(snapshot.child("ciudad").getValue().toString());
                            mDialog.dismiss();
                        }else {
                            Intent intent = new Intent(categotia.this, registrarDireccion.class);
                            intent.putExtra("idUsuario", idUsuario);
                            intent.putExtra("categoria", "MI UBICACION");
                            startActivity(intent);
                            mDialog.dismiss();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
            }
        });
        mDialog.show();
    }
}