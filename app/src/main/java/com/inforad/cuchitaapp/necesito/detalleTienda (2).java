package com.inforad.cuchitaapp.necesito;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.adapters.productoTiendaAdaptador;
import com.inforad.cuchitaapp.chat.ViewChat;
import com.inforad.cuchitaapp.models.Contacto;
import com.inforad.cuchitaapp.models.Producto;
import com.inforad.cuchitaapp.provider.UserProvider;
import com.inforad.cuchitaapp.provider.contactoProvider;
import com.inforad.cuchitaapp.provider.negocioProvider;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class detalleTienda extends AppCompatActivity {

    private TextView txtTienda;
    private String tienda_id, nombreTienda, imagen, categoria, idUsuario, idAdministrador, nomUsuario, fotoUsuario, telfUsuario, nomAdmin, fotoAdmin, telfAdmin;
    private RecyclerView recyclerListaProducto;
    private DatabaseReference mDatabase;
    private productoTiendaAdaptador adaptadorProducto;
    private CircleImageView imgTienda;
    private LinearLayout btnInicio, btnPedido, btnBilletera, btnAjustes, btnCarrito;
    private negocioProvider mNegocioProdiver;
    private ImageView btnAtras;
    private Button btnEnviarMensaje;
    private FirebaseAuth mAuth;
    private contactoProvider mContactoProvider;
    private UserProvider mUserProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_detalle_tienda);
        Iniciar();

        btnInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(detalleTienda.this, categotia.class);
                startActivity(intent);
            }
        });

        btnPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(detalleTienda.this, listapedido.class);
                startActivity(intent);
            }
        });

        btnBilletera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(detalleTienda.this, billetera.class);
                startActivity(intent);
            }
        });

        btnAjustes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(detalleTienda.this, ajustes.class);
                startActivity(intent);
            }
        });

        btnCarrito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        txtTienda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(detalleTienda.this, detalle.class);
                intent.putExtra("idTienda", tienda_id);
                startActivity(intent);
            }
        });

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(detalleTienda.this, resultCategoria.class);
                intent.putExtra("categoria", categoria);
                startActivity(intent);
            }
        });

        btnEnviarMensaje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Contacto enviar = new Contacto(idUsuario, idAdministrador);
                mContactoProvider.crearContactoUsuario(enviar, idUsuario, idAdministrador).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Contacto recibir = new Contacto(idAdministrador, idUsuario);
                        mContactoProvider.crearContactoUsuario(recibir, idAdministrador, idUsuario).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Intent intent = new Intent(detalleTienda.this, ViewChat.class);
                                intent.putExtra("idEnvia", idUsuario);
                                intent.putExtra("idRecibir", idAdministrador);
                                startActivity(intent);
                            }
                        });
                    }
                });
            }
        });


    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == event.KEYCODE_BACK){
            Intent intent = new Intent(detalleTienda.this, resultCategoria.class);
            intent.putExtra("categoria", categoria);
            startActivity(intent);
        }
        return super.onKeyUp(keyCode, event);
    }

    private void Iniciar(){
        btnAtras = findViewById(R.id.ivAtras);
        mNegocioProdiver = new negocioProvider();
        txtTienda = findViewById(R.id.tvNombreNegocioDetalle);
        btnEnviarMensaje = findViewById(R.id.bAgregarContacto);

        txtTienda.setText(nombreTienda);
        imgTienda = findViewById(R.id.circleImgNegocio);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        recyclerListaProducto = findViewById(R.id.recyclerListaProductoTienda);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerListaProducto.setLayoutManager(linearLayoutManager);
        mContactoProvider = new contactoProvider();
        mUserProvider = new UserProvider();
        obtenerDatosAnteriores();

        //cargarDatos();
        BarraHerramientas();
    }

    private void BarraHerramientas(){
        btnInicio = findViewById(R.id.llInicio);
        btnPedido = findViewById(R.id.llPedido);
        btnBilletera = findViewById(R.id.llBilletera);
        btnAjustes = findViewById(R.id.llAjustes);
        btnCarrito = findViewById(R.id.llCarritoCompras);
    }

    private void obtenerDatosAnteriores(){
        tienda_id = getIntent().getStringExtra("tienda_id");
        nombreTienda = getIntent().getStringExtra("tienda");
        imagen = getIntent().getStringExtra("imagen");

        mNegocioProdiver.getNegocio(tienda_id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String img = snapshot.child("imagen").getValue().toString();
                    Picasso.with(detalleTienda.this).load(img).into(imgTienda);
                    categoria = snapshot.child("categoria").getValue().toString();
                    idAdministrador = snapshot.child("idAdministrador").getValue().toString();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        mUserProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    nomUsuario = snapshot.child("alias").getValue().toString();
                    fotoUsuario = snapshot.child("imagen").getValue().toString();
                    telfUsuario = snapshot.child("telefono").getValue().toString();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });



    }

    private void cargarDatos(){

        Query query = mDatabase.child("Producto").orderByChild("idTienda").equalTo(tienda_id);
        FirebaseRecyclerOptions<Producto> options = new FirebaseRecyclerOptions.Builder<Producto>().setQuery(query, Producto.class).build();
        adaptadorProducto = new productoTiendaAdaptador(options, detalleTienda.this, tienda_id);
        recyclerListaProducto.setAdapter(adaptadorProducto);
        adaptadorProducto.startListening();
    }

    private void mostrarTopVentas(){
        Query query = mDatabase.child("Producto").orderByChild("idTienda").equalTo(tienda_id);
        FirebaseRecyclerOptions<Producto> options = new FirebaseRecyclerOptions.Builder<Producto>().setQuery(query, Producto.class).build();
        adaptadorProducto = new productoTiendaAdaptador(options, detalleTienda.this, tienda_id);
        recyclerListaProducto.setAdapter(adaptadorProducto);
        adaptadorProducto.startListening();
    }

    @Override
    protected void onStart() {
        super.onStart();
        cargarDatos();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adaptadorProducto.stopListening();
    }
}