package com.inforad.cuchitaapp.necesito;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.MainActivity;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.provider.UserProvider;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class ajustes extends AppCompatActivity {

    private LinearLayout btnInicio, btnPedido, btnBilletera, btnAjustes, btnCarrito;
    private CircleImageView imgPerfil;
    private TextView txtNombre, txtApellido, txtAlias, txtNumero, txtCorreo, txtTiempo, txtCosto, txtDescripcion;
    private FirebaseAuth mAuth;
    private String idUsuario;
    private UserProvider mUserProvider;
    private ImageView btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_ajustes);
        Iniciar();
        BarraHerramientas();


        btnInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ajustes.this, categotia.class);
                startActivity(intent);
            }
        });

        btnPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ajustes.this, listapedido.class);
                startActivity(intent);
            }
        });

        btnBilletera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ajustes.this, billetera.class);
                startActivity(intent);
            }
        });

        btnAjustes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ajustes.this, ajustes.class);
                startActivity(intent);
            }
        });

        btnCarrito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
                Intent inte = new Intent(ajustes.this, MainActivity.class);
                startActivity(inte);
                finish();
            }
        });


    }

    private void Iniciar() {
        txtNombre = findViewById(R.id.tvNombre);
        txtApellido = findViewById(R.id.tvApellido);
        txtAlias = findViewById(R.id.tvNickName);
        txtCorreo = findViewById(R.id.tvCorreo);
        txtNumero = findViewById(R.id.tvNumero);
        txtTiempo = findViewById(R.id.tvTiempo);
        txtCosto = findViewById(R.id.tvCosto);
        txtDescripcion = findViewById(R.id.tvDescripcion);
        imgPerfil = findViewById(R.id.civPerfil);
        btnSalir = findViewById(R.id.ivSalir);

        mAuth = FirebaseAuth.getInstance();
        mUserProvider = new UserProvider();

        idUsuario = mAuth.getCurrentUser().getUid();

        cargarDatos();
    }

    private void BarraHerramientas(){
        btnInicio = findViewById(R.id.llInicio);
        btnPedido = findViewById(R.id.llPedido);
        btnBilletera = findViewById(R.id.llBilletera);
        btnAjustes = findViewById(R.id.llAjustes);
        btnCarrito = findViewById(R.id.llCarritoCompras);
    }

    private void cargarDatos(){
        mUserProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    txtNombre.setText(snapshot.child("nombre").getValue().toString());
                    txtApellido.setText(snapshot.child("apellido").getValue().toString());
                    txtAlias.setText(snapshot.child("alias").getValue().toString());
                    txtCorreo.setText(snapshot.child("dni").getValue().toString());
                    txtNumero.setText(snapshot.child("telefono").getValue().toString());
                    String img = snapshot.child("imagen").getValue().toString();
                    Picasso.with(ajustes.this).load(img).into(imgPerfil);
                    txtDescripcion.setText(snapshot.child("plan").getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

}