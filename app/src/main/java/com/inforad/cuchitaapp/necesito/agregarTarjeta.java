package com.inforad.cuchitaapp.necesito;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.Tarjeta;
import com.inforad.cuchitaapp.provider.tarjetaProvider;

public class agregarTarjeta extends AppCompatActivity {

    private Button btnAgregar;
    private EditText inputNumTarjeta, inputFecha, inputCodigo;
    private tarjetaProvider mTarjetaProvider;
    private FirebaseAuth mAuth;
    private String idUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_agregar_tarjeta);
        Iniciar();

        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Tarjeta tarjeta = new Tarjeta(inputNumTarjeta.getText().toString(), inputNumTarjeta.getText().toString(), inputCodigo.getText().toString(), inputFecha.getText().toString(), "", "", "1", "ACTIVO");
                mTarjetaProvider.crear(tarjeta, idUsuario, inputNumTarjeta.getText().toString()).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Intent intent = new Intent(agregarTarjeta.this, billetera.class);
                        startActivity(intent);
                    }
                });
            }
        });
    }

    private void Iniciar() {
        btnAgregar = findViewById(R.id.bGuardar);
        inputNumTarjeta = findViewById(R.id.etNumTarjeta);
        inputCodigo = findViewById(R.id.etCod);
        inputFecha = findViewById(R.id.etFecVencimiento);

        mTarjetaProvider = new tarjetaProvider();
        mAuth = FirebaseAuth.getInstance();

        idUsuario = mAuth.getCurrentUser().getUid();
    }
}