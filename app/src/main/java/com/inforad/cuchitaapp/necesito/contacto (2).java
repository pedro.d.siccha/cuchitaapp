package com.inforad.cuchitaapp.necesito;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.adapters.contactosAdaptador;
import com.inforad.cuchitaapp.models.Contacto;
import com.inforad.cuchitaapp.provider.UserProvider;

public class contacto extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private UserProvider mUserProvider;
    private RecyclerView recyclerListaContactos;
    private String idUsuario;
    private contactosAdaptador mContactoAdaptador;
    private DrawerLayout backGroundPrincipal;
    private Bitmap bitmap;
    private BitmapDrawable background;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_contacto);
        Inicio();
    }

    private void Inicio() {
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mUserProvider = new UserProvider();

        recyclerListaContactos = findViewById(R.id.rvListaContactos);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerListaContactos.setLayoutManager(linearLayoutManager);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Query queryContacto = mDatabase.child("Users").child(idUsuario).child("Contacto");
        FirebaseRecyclerOptions<Contacto> options = new FirebaseRecyclerOptions.Builder<Contacto>().setQuery(queryContacto, Contacto.class).build();
        mContactoAdaptador = new contactosAdaptador(options, contacto.this);
        recyclerListaContactos.setAdapter(mContactoAdaptador);
        mContactoAdaptador.startListening();
    }
}