package com.inforad.cuchitaapp.necesito;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.adapters.tiendaAdaptador;
import com.inforad.cuchitaapp.models.Negocio;

import java.util.ArrayList;
import java.util.List;

public class resultCategoria extends AppCompatActivity {

    private String Categoria, idUsuario;
    private RecyclerView recyclerViewListProducto;
    private DatabaseReference mDatabase;
    private List<Negocio> tienda = new ArrayList<>();
    private tiendaAdaptador adaptadorTienda;
    private LinearLayout btnInicio, btnPedido, btnBilletera, btnAjustes, btnCarrito;
    private FirebaseAuth mAuth;
    private ImageView btnAtras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_result_categoria);
        Iniciar();

        btnInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(resultCategoria.this, categotia.class);
                startActivity(intent);
            }
        });

        btnPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(resultCategoria.this, listapedido.class);
                startActivity(intent);
            }
        });

        btnBilletera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(resultCategoria.this, billetera.class);
                startActivity(intent);
            }
        });

        btnAjustes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(resultCategoria.this, ajustes.class);
                startActivity(intent);
            }
        });

        btnCarrito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(resultCategoria.this, categotia.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == event.KEYCODE_BACK){
            Intent intent = new Intent(resultCategoria.this, categotia.class);
            startActivity(intent);
        }
        return super.onKeyUp(keyCode, event);
    }

    private void Iniciar(){
        obtenerDatosAnteriores();
        btnAtras = findViewById(R.id.ivAtras);
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        recyclerViewListProducto = findViewById(R.id.recyclerListaTienda);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerViewListProducto.setLayoutManager(linearLayoutManager);
        BarraHerramientas();

    }

    private void BarraHerramientas(){
        btnInicio = findViewById(R.id.llInicio);
        btnPedido = findViewById(R.id.llPedido);
        btnBilletera = findViewById(R.id.llBilletera);
        btnAjustes = findViewById(R.id.llAjustes);
        btnCarrito = findViewById(R.id.llCarritoCompras);
    }

    private void obtenerDatosAnteriores(){
        Categoria = getIntent().getStringExtra("categoria");
    }

    @Override
    protected void onStart() {
        super.onStart();

        Query query = mDatabase.child("Negocio_Activo").orderByChild("categoria").equalTo(Categoria);

        FirebaseRecyclerOptions<Negocio> options = new FirebaseRecyclerOptions.Builder<Negocio>().setQuery(query, Negocio.class).build();
        adaptadorTienda = new tiendaAdaptador(options, resultCategoria.this, idUsuario);

        recyclerViewListProducto.setAdapter(adaptadorTienda);
        adaptadorTienda.startListening();

    }

    @Override
    protected void onStop() {
        super.onStop();
        adaptadorTienda.stopListening();
    }
}