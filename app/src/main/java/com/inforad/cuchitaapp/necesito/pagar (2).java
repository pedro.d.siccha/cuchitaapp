package com.inforad.cuchitaapp.necesito;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.provider.negocioProvider;
import com.inforad.cuchitaapp.provider.pedidoProductoProvider;

public class pagar extends AppCompatActivity {

    private TextView txtTienda;
    private String idPedido, precio, fecPedido, fecEntrega, idUsuario, idTienda;
    private Dialog mDialog;
    private Button btnPagar;
    private pedidoProductoProvider mPedidoProvider;
    private negocioProvider mNegocioProvider;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_pagar);
        Iniciar();

        btnPagar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mostrarTipoPago(view);
                Toast.makeText(pagar.this, "Prueba", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void Iniciar(){

        txtTienda = findViewById(R.id.tvTienda);
        btnPagar = findViewById(R.id.btnPagarPedido);


        mDialog = new Dialog(pagar.this);
        mDialog.setContentView(R.layout.modal_tipo_pago);
        mPedidoProvider = new pedidoProductoProvider();
        mNegocioProvider = new negocioProvider();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();

        obtenerDatosAnteriores();

    }

    private void obtenerDatosAnteriores(){
        idPedido = getIntent().getStringExtra("idPedido");
        idTienda = getIntent().getStringExtra("idTienda");

        mNegocioProvider.getNegocio(idTienda).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    txtTienda.setText(snapshot.child("nombre").getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        mPedidoProvider.obtenerPedido(idTienda, idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    btnPagar.setText("S/. " + snapshot.child("precio").getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    public void mostrarTipoPago(View view){
        TextView txtCerrar;
        LinearLayout btnTarjeta, btnEfectivo, btnCuchita;

        txtCerrar = (TextView) mDialog.findViewById(R.id.tvCerrar);
        txtCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        btnTarjeta = (LinearLayout) mDialog.findViewById(R.id.llPagoTarjeta);
        btnTarjeta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(pagar.this, "Pago realizado correctamente", Toast.LENGTH_SHORT).show();
                mPedidoProvider.actualizarEstado(idUsuario, idTienda, "PAGADO").addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Intent intent = new Intent(pagar.this, calificacion.class);
                        intent.putExtra("idTienda", idTienda);
                        startActivity(intent);
                    }
                });

                /*
                intent.putExtra("idUsuario", usuario_id);
                intent.putExtra("idTienda", tienda_id);
                intent.putExtra("metodoPago", "TARJETA");
                startActivity(intent);
                 */
            }
        });
        btnEfectivo = (LinearLayout) mDialog.findViewById(R.id.llPagoEfectivo);
        btnEfectivo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(pagar.this, "Pago realizado correctamente", Toast.LENGTH_SHORT).show();
                mPedidoProvider.actualizarEstado(idUsuario, idTienda, "PAGADO").addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Intent intent = new Intent(pagar.this, calificacion.class);
                        intent.putExtra("idTienda", idTienda);
                        startActivity(intent);
                    }
                });

                /*
                intent.putExtra("idUsuario", usuario_id);
                intent.putExtra("idTienda", tienda_id);
                intent.putExtra("metodoPago", "EFECTIVO");
                 */

            }
        });
        btnCuchita = (LinearLayout) mDialog.findViewById(R.id.llPagoCuchita);
        btnCuchita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast toast = Toast.makeText(pagar.this, "Proximamente", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 50, 50);
                toast.getView().setBackgroundColor(Color.GREEN);
                toast.show();
            }
        });
        mDialog.show();
    }
}