package com.inforad.cuchitaapp.necesito;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.adapters.anuncioTiendaAdaptador;
import com.inforad.cuchitaapp.models.Anuncios;
import com.inforad.cuchitaapp.provider.AnuncioProvider;
import com.inforad.cuchitaapp.provider.UserProvider;
import com.inforad.cuchitaapp.provider.negocioProvider;

public class anuncios extends AppCompatActivity {

    private String idNegocio, idUsuario;
    private Button btnNuevo, btnAnuncios;
    private FirebaseAuth mAuth;
    private FirebaseAuth mFirebaseAuth;
    private DatabaseReference mDatabase;
    private anuncioTiendaAdaptador adaptadorAnuncio;
    private AnuncioProvider mAnuncioProvider;
    private negocioProvider mNegocioProvider;
    private UserProvider mUserProvider;
    private RecyclerView lisaAnuncios;
    private LinearLayout btnInicio, btnPedido, btnBilletera, btnAjustes, btnCarrito;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_anuncios);
        Iniciar();

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(anuncios.this, nuevoAnuncio.class);
                intent.putExtra("idTienda", idNegocio);
                startActivity(intent);
            }
        });

        btnInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(anuncios.this, categotia.class);
                startActivity(intent);
            }
        });

        btnPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(anuncios.this, listapedido.class);
                startActivity(intent);
            }
        });

        btnBilletera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(anuncios.this, billetera.class);
                startActivity(intent);
            }
        });

        btnAjustes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(anuncios.this, ajustes.class);
                startActivity(intent);
            }
        });

        btnCarrito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(anuncios.this, carritoCompras.class);
                startActivity(intent);
            }
        });

        btnAnuncios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(anuncios.this, misAnuncios.class);
                startActivity(intent);
            }
        });
    }


    private void Iniciar(){
        btnNuevo = findViewById(R.id.btnSubirAnuncios);
        btnAnuncios = findViewById(R.id.btnMisAnuncios);
        lisaAnuncios = findViewById(R.id.verAnuncios);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        lisaAnuncios.setLayoutManager(linearLayoutManager);
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAnuncioProvider = new AnuncioProvider();
        mNegocioProvider = new negocioProvider();
        mUserProvider = new UserProvider();

        obtenerDatosAnteriores();
        BarraHerramientas();
    }

    private void BarraHerramientas() {
        btnInicio = findViewById(R.id.llInicio);
        btnPedido = findViewById(R.id.llPedido);
        btnBilletera = findViewById(R.id.llBilletera);
        btnAjustes = findViewById(R.id.llAjustes);
        btnCarrito = findViewById(R.id.llCarritoCompras);

    }

    private void obtenerDatosAnteriores(){
        idNegocio = getIntent().getStringExtra("negocio_id");
    }


    @Override
    protected void onStart() {
        super.onStart();
        Query query = mDatabase.child("Anuncios").orderByChild("estado").equalTo("PUBLICADO");
        FirebaseRecyclerOptions<Anuncios> options = new FirebaseRecyclerOptions.Builder<Anuncios>().setQuery(query, Anuncios.class).build();
        adaptadorAnuncio = new anuncioTiendaAdaptador(options, anuncios.this);
        lisaAnuncios.setAdapter(adaptadorAnuncio);
        adaptadorAnuncio.startListening();
    }
}