package com.inforad.cuchitaapp.necesito;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.provider.negocioProvider;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class calificacion extends AppCompatActivity {

    private TextView txtTienda, txtTipoTienda;
    private ImageView btnLike, btnDisLike;
    private Button btnFinalizar;
    private EditText imputComentario;
    private CircleImageView imgTienda;
    private String idTienda;
    private negocioProvider mNegocioProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_calificacion);
        Iniciar();

        btnLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(calificacion.this, "LIKE", Toast.LENGTH_SHORT).show();
                btnDisLike.setVisibility(View.GONE);
            }
        });

        btnDisLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(calificacion.this, "DISLIKE", Toast.LENGTH_SHORT).show();
                btnLike.setVisibility(View.VISIBLE);
            }
        });

        btnFinalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(calificacion.this, "Gracias por su preferencia", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(calificacion.this, carritoCompras.class);
                startActivity(intent);
            }
        });

    }

    private void Iniciar() {
        txtTienda = findViewById(R.id.tvNomTienda);
        txtTipoTienda = findViewById(R.id.tvTipoNegocio);
        imgTienda = findViewById(R.id.ivNegocio);
        btnLike = findViewById(R.id.ivLike);
        btnDisLike = findViewById(R.id.ivDisLike);
        btnFinalizar = findViewById(R.id.btnFinalizar);
        imputComentario = findViewById(R.id.etComentario);

        mNegocioProvider = new negocioProvider();

        cargarDatos();
    }

    private void cargarDatos(){
        idTienda = getIntent().getStringExtra("idTienda");

        mNegocioProvider.getNegocio(idTienda).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    txtTienda.setText(snapshot.child("nombre").getValue().toString());
                    txtTipoTienda.setText(snapshot.child("categoria").getValue().toString());
                    String img = snapshot.child("imagen").getValue().toString();
                    Picasso.with(calificacion.this).load(img).into(imgTienda);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}