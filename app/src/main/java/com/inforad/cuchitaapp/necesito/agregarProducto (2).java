package com.inforad.cuchitaapp.necesito;

import android.app.ProgressDialog;
import android.content.Intent;
import android.icu.text.DateFormat;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.inforad.cuchitaapp.R;
import com.inforad.cuchitaapp.models.PedidoProducto;
import com.inforad.cuchitaapp.provider.UserProvider;
import com.inforad.cuchitaapp.provider.contactoProvider;
import com.inforad.cuchitaapp.provider.negocioInventarioProvider;
import com.inforad.cuchitaapp.provider.negocioProvider;
import com.inforad.cuchitaapp.provider.pedidoProductoProvider;
import com.inforad.cuchitaapp.provider.productoProvider;
import com.squareup.picasso.Picasso;

import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

public class agregarProducto extends AppCompatActivity {

    private CircleImageView imgFotoProducto;
    private TextView txtNombre, txtStock, txtPrecio, txtDescripcion;
    private EditText etCantidad;
    private Button btnCancelar, btnAgregar;
    private String producto_id, idUsuario, fhoy, stock, precio, idNegocio, nomProducto;
    private ProgressDialog mProgressDialog;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private negocioInventarioProvider mInventarioProvider;
    private Date fechaActual;
    private DateFormat dateFormat;
    private pedidoProductoProvider mPedidoProvider;
    private pedidoProductoProvider mProductoProvider;
    private negocioProvider mNegocioProvider;
    private contactoProvider mContactoProvider;
    private UserProvider mUserProvider;
    private productoProvider mProdProvider;
    private ImageView btnAtras;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_agregar_producto);
        Iniciar();

        etCantidad.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!charSequence.toString().equals(null)){
                    double nuevoStock;
                    double nuevoPrecio;
                    nuevoStock = Double.parseDouble(stock);

                    if (Double.parseDouble(charSequence.toString()) > nuevoStock){
                        Toast.makeText(agregarProducto.this, "Por favor ingrese una cantidad correcta", Toast.LENGTH_SHORT).show();
                        etCantidad.setFocusable(true);
                    }else {
                        nuevoPrecio = Double.parseDouble(precio);
                        btnAgregar.setText("S/. " + nuevoPrecio*Double.parseDouble(charSequence.toString()));
                    }

                }else {
                    btnAgregar.setText("AGREGAR");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double nuevaCantidad = Double.parseDouble(etCantidad.getText().toString())*Double.parseDouble(precio);
                PedidoProducto producto = new PedidoProducto(idNegocio, idUsuario, producto_id, nomProducto, etCantidad.getText().toString(), nuevaCantidad);
                mPedidoProvider.create(producto, idNegocio, idUsuario, producto_id).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        actualizarStock(producto_id, Double.parseDouble(etCantidad.getText().toString()), nuevaCantidad);
                    }
                });
            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(agregarProducto.this, detalleTienda.class);
                intent.putExtra("tienda_id", idNegocio);
                intent.putExtra("tienda", "");
                intent.putExtra("imagen", "");
                startActivity(intent);
            }
        });

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(agregarProducto.this, detalleTienda.class);
                intent.putExtra("tienda_id", idNegocio);
                intent.putExtra("tienda", "");
                intent.putExtra("imagen", "");
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == event.KEYCODE_BACK){
            Intent intent = new Intent(agregarProducto.this, detalleTienda.class);
            intent.putExtra("tienda_id", idNegocio);
            intent.putExtra("tienda", "");
            intent.putExtra("imagen", "");
            startActivity(intent);
        }
        return super.onKeyUp(keyCode, event);
    }

    private void actualizarStock(String idProducto, double cantidad, double addPrecio) {
        mProdProvider.obtenerProducto(idProducto).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String stock = snapshot.child("stock").getValue().toString();
                    double stockActual = Double.parseDouble(stock);
                    double nuevoStock = stockActual - cantidad;
                    String actualizado = String.valueOf(nuevoStock);

                    mProdProvider.actualizarStock(idProducto, actualizado).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            actualizarPrecioPedido(addPrecio);
                        }
                    });

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void actualizarPrecioPedido(double precioProducto) {
        mPedidoProvider.obtenerPedido(idNegocio, idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String prec = snapshot.child("precio").getValue().toString();
                    double precio = Double.parseDouble(prec);
                    double nuevoPrecio = precio + precioProducto;
                    mPedidoProvider.actualizarPrecio(idNegocio, idUsuario, nuevoPrecio).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Intent intent = new Intent(agregarProducto.this, detalleTienda.class);
                            intent.putExtra("tienda_id", idNegocio);
                            intent.putExtra("tienda", "");
                            intent.putExtra("imagen", "");
                            startActivity(intent);
                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void Iniciar(){

        mProgressDialog = new ProgressDialog(this);
        imgFotoProducto = findViewById(R.id.imgProductoAgregar);
        txtNombre = findViewById(R.id.nombreProducto);
        txtStock = findViewById(R.id.stockProducto);
        txtPrecio = findViewById(R.id.precioProducto);
        etCantidad = findViewById(R.id.inputCantidadProducto);
        btnCancelar = findViewById(R.id.btnVolver);
        btnAgregar = findViewById(R.id.btnAgregar);
        txtDescripcion = findViewById(R.id.tvDescripcion);
        btnAtras = findViewById(R.id.ivAtras);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        obtenerDatosAnteriores();
        mInventarioProvider = new negocioInventarioProvider();
        fechaActual = new Date();
        dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        fhoy = dateFormat.format(fechaActual);
        mPedidoProvider = new pedidoProductoProvider();
        mProductoProvider = new pedidoProductoProvider();
        mNegocioProvider = new negocioProvider();
        mContactoProvider = new contactoProvider();
        mUserProvider = new UserProvider();
        mProdProvider = new productoProvider();
        obtenerDatosNegocio();
    }

    private void obtenerDatosNegocio(){

        mProdProvider.obtenerProducto(producto_id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    stock = snapshot.child("stock").getValue().toString();
                    precio = snapshot.child("precioUnidad").getValue().toString();
                    idNegocio = snapshot.child("idTienda").getValue().toString();
                    nomProducto = snapshot.child("nombre").getValue().toString();
                    txtNombre.setText(snapshot.child("nombre").getValue().toString());
                    txtPrecio.setText("S/. " + snapshot.child("precioUnidad").getValue().toString());
                    txtStock.setText(snapshot.child("unidadMedida").getValue().toString());
                    txtDescripcion.setText(snapshot.child("detalle").getValue().toString());
                    String img = snapshot.child("imgProducto").getValue().toString();
                    Picasso.with(agregarProducto.this).load(img).into(imgFotoProducto);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void obtenerDatosAnteriores(){

        producto_id = getIntent().getStringExtra("idProducto");

    }
}