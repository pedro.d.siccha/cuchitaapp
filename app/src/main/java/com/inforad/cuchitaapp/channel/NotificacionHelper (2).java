package com.inforad.cuchitaapp.channel;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.inforad.cuchitaapp.R;

public class NotificacionHelper extends ContextWrapper {

    private static final String CHANNEL_ID = "com.inforad.cuchitaapp";
    private static final String CHANNEL_NAME = "Cuchita SAC";

    private NotificationManager manager;

    public NotificacionHelper(Context base) {
        super(base);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            createChannels();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void createChannels(){
        NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
        notificationChannel.enableLights(true);
        notificationChannel.enableVibration(true);
        notificationChannel.setLightColor(Color.GRAY);
        notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        getManager().createNotificationChannel(notificationChannel);
    }

    public NotificationManager getManager(){
        if (manager == null){
            manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return manager;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public Notification.Builder getNotification(String titulo, String contenido, PendingIntent intent, Uri sounUri){
        return new Notification.Builder(getApplicationContext(), CHANNEL_ID).setContentTitle(titulo).setContentText(contenido).setAutoCancel(true).setSound(sounUri).setContentIntent(intent).setSmallIcon(R.drawable.ic_notificaion).setStyle(new Notification.BigTextStyle().bigText(contenido).setBigContentTitle(titulo));
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public Notification.Builder getNotificationActions(String titulo, String contenido, Uri sounUri, Notification.Action aceptarAccion, Notification.Action cancelarAccion){
        return new Notification.Builder(getApplicationContext(), CHANNEL_ID).setContentTitle(titulo).setContentText(contenido).setAutoCancel(true).setSound(sounUri).setSmallIcon(R.drawable.ic_notificaion).addAction(aceptarAccion).addAction(cancelarAccion).setStyle(new Notification.BigTextStyle().bigText(contenido).setBigContentTitle(titulo));
    }

    public NotificationCompat.Builder getNotificationOlApi(String titulo, String contenido, PendingIntent intent, Uri sounUri){
        return new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID).setContentTitle(titulo).setContentText(contenido).setAutoCancel(true).setSound(sounUri).setContentIntent(intent).setSmallIcon(R.drawable.ic_notificaion).setStyle(new NotificationCompat.BigTextStyle().bigText(contenido).setBigContentTitle(titulo));
    }

    public NotificationCompat.Builder getNotificationOlApiActions(String titulo, String contenido, Uri sounUri, NotificationCompat.Action aceptarAccion, NotificationCompat.Action cancelarAccion){
        return new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID).setContentTitle(titulo).setContentText(contenido).setAutoCancel(true).setSound(sounUri).setSmallIcon(R.drawable.ic_notificaion).addAction(aceptarAccion).addAction(cancelarAccion).setStyle(new NotificationCompat.BigTextStyle().bigText(contenido).setBigContentTitle(titulo));
    }

}
